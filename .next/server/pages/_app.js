module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/antd/dist/antd.min.css":
/*!*********************************************!*\
  !*** ./node_modules/antd/dist/antd.min.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./node_modules/quill/dist/quill.snow.css":
/*!************************************************!*\
  !*** ./node_modules/quill/dist/quill.snow.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./node_modules/slick-carousel/slick/slick.css":
/*!*****************************************************!*\
  !*** ./node_modules/slick-carousel/slick/slick.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd_dist_antd_min_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/dist/antd.min.css */ "./node_modules/antd/dist/antd.min.css");
/* harmony import */ var antd_dist_antd_min_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_dist_antd_min_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styles_global_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styles/global.scss */ "./styles/global.scss");
/* harmony import */ var styles_global_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styles_global_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! slick-carousel/slick/slick.css */ "./node_modules/slick-carousel/slick/slick.css");
/* harmony import */ var slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(slick_carousel_slick_slick_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! quill/dist/quill.snow.css */ "./node_modules/quill/dist/quill.snow.css");
/* harmony import */ var quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(quill_dist_quill_snow_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var plugins_utils_ConfigLive__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! plugins/utils/ConfigLive */ "./plugins/utils/ConfigLive.js");
/* harmony import */ var styles_header_scss__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! styles/header.scss */ "./styles/header.scss");
/* harmony import */ var styles_header_scss__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(styles_header_scss__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var styles_hero_scss__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! styles/hero.scss */ "./styles/hero.scss");
/* harmony import */ var styles_hero_scss__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(styles_hero_scss__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var styles_articles_scss__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! styles/articles.scss */ "./styles/articles.scss");
/* harmony import */ var styles_articles_scss__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(styles_articles_scss__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var styles_footer_scss__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! styles/footer.scss */ "./styles/footer.scss");
/* harmony import */ var styles_footer_scss__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(styles_footer_scss__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var styles_hero_text_scss__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! styles/hero-text.scss */ "./styles/hero-text.scss");
/* harmony import */ var styles_hero_text_scss__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(styles_hero_text_scss__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var styles_article_detail_scss__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! styles/article-detail.scss */ "./styles/article-detail.scss");
/* harmony import */ var styles_article_detail_scss__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(styles_article_detail_scss__WEBPACK_IMPORTED_MODULE_11__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;













function MyApp({
  Component,
  pageProps
}) {
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    plugins_utils_ConfigLive__WEBPACK_IMPORTED_MODULE_5__["ConfigLive"].consoleHandle();
    return () => {};
  }, []);
  return __jsx(Component, pageProps);
}

/* harmony default export */ __webpack_exports__["default"] = (MyApp);

/***/ }),

/***/ "./plugins/teexii/TBrowser.js":
/*!************************************!*\
  !*** ./plugins/teexii/TBrowser.js ***!
  \************************************/
/*! exports provided: TBrowser, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TBrowser", function() { return TBrowser; });
const TBrowser = {
  deleteAllCookies: function () {
    var cookies = document.cookie.split(";");

    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i];
      var eqPos = cookie.indexOf("=");
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
  },
  setCookie: function (cname, cvalue, exdays) {
    if (exdays == undefined) exdays = 999;
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  },
  getCookie: function (cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];

      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }

      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }

    return "";
  },
  hasCookie: function (_name) {
    var _str = tBrowser.getCookie(_name);

    if (_str != "") {
      return true;
      ç;
    } else {
      return false;
    }
  },
  isMobile: function () {
    if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)) {
      return true;
    } else {
      return false;
    }
  },
  isSupportWebGL: function () {
    // return Detector.webgl();
    var Detector = {
      canvas: !!window.CanvasRenderingContext2D,
      webgl: function () {
        // 
        try {
          var canvas = document.createElement('canvas');
          return !!(window.WebGLRenderingContext && (canvas.getContext('webgl') || canvas.getContext('experimental-webgl')));
        } catch (e) {
          return false;
        }
      }(),
      workers: !!window.Worker,
      fileapi: window.File && window.FileReader && window.FileList && window.Blob,
      getWebGLErrorMessage: function () {
        var element = document.createElement('div');
        element.id = 'webgl-error-message';
        element.style.fontFamily = 'monospace';
        element.style.fontSize = '13px';
        element.style.fontWeight = 'normal';
        element.style.textAlign = 'center';
        element.style.background = '#fff';
        element.style.color = '#000';
        element.style.padding = '1.5em';
        element.style.width = '400px';
        element.style.margin = '5em auto 0';

        if (!this.webgl) {
          element.innerHTML = window.WebGLRenderingContext ? ['Your graphics card does not seem to support <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation" style="color:#000">WebGL</a>.<br />', 'Find out how to get it <a href="http://get.webgl.org/" style="color:#000">here</a>.'].join('\n') : ['Your browser does not seem to support <a href="http://khronos.org/webgl/wiki/Getting_a_WebGL_Implementation" style="color:#000">WebGL</a>.<br/>', 'Find out how to get it <a href="http://get.webgl.org/" style="color:#000">here</a>.'].join('\n');
        }

        return element;
      },
      addGetWebGLMessage: function (parameters) {
        var parent, id, element;
        parameters = parameters || {};
        parent = parameters.parent !== undefined ? parameters.parent : document.body;
        id = parameters.id !== undefined ? parameters.id : 'oldie';
        element = Detector.getWebGLErrorMessage();
        element.id = id;
        parent.appendChild(element);
      }
    };
    return Detector.webgl;
  },
  getUrlParameters: function (parameter, staticURL, decode) {
    staticURL = staticURL == undefined ? window.location : staticURL;
    var currLocation = staticURL.length ? staticURL : window.location.search;
    if (currLocation.split("?").length < 2) return "";
    var parArr = currLocation.split("?")[1].split("&"),
        returnBool = true;

    for (var i = 0; i < parArr.length; i++) {
      var parr = parArr[i].split("=");

      if (parr[0] == parameter) {
        return decode ? decodeURIComponent(parr[1]) : parr[1];
        returnBool = true;
      } else {
        returnBool = false;
      }
    }

    if (!returnBool) return false;
  },
  checkOS: function (option) {
    option = option != undefined ? option : {};
    var callback = option.hasOwnProperty("callback") ? option.callback : null;

    if (typeof window.deviceInfo == "undefined") {
      var unknown = '-'; // screen

      var screenSize = '';

      if (screen.width) {
        var width = screen.width ? screen.width : '';
        var height = screen.height ? screen.height : '';
        screenSize += '' + width + " x " + height;
      } // browser


      var nVer = navigator.appVersion;
      var nAgt = navigator.userAgent;
      var browser = navigator.appName;
      var version = '' + parseFloat(navigator.appVersion);
      var majorVersion = parseInt(navigator.appVersion, 10);
      var nameOffset, verOffset, ix; // Opera

      if ((verOffset = nAgt.indexOf('Opera')) != -1) {
        browser = 'Opera';
        version = nAgt.substring(verOffset + 6);

        if ((verOffset = nAgt.indexOf('Version')) != -1) {
          version = nAgt.substring(verOffset + 8);
        }
      } // Opera Next


      if ((verOffset = nAgt.indexOf('OPR')) != -1) {
        browser = 'Opera';
        version = nAgt.substring(verOffset + 4);
      } // Edge
      else if ((verOffset = nAgt.indexOf('Edge')) != -1) {
          browser = 'Microsoft Edge';
          version = nAgt.substring(verOffset + 5);
        } // MSIE
        else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(verOffset + 5);
          } // Chrome
          else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
              browser = 'Chrome';
              version = nAgt.substring(verOffset + 7);
            } // Safari
            else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
                browser = 'Safari';
                version = nAgt.substring(verOffset + 7);

                if ((verOffset = nAgt.indexOf('Version')) != -1) {
                  version = nAgt.substring(verOffset + 8);
                }
              } // Firefox
              else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
                  browser = 'Firefox';
                  version = nAgt.substring(verOffset + 8);
                } // MSIE 11+
                else if (nAgt.indexOf('Trident/') != -1) {
                    browser = 'Microsoft Internet Explorer';
                    version = nAgt.substring(nAgt.indexOf('rv:') + 3);
                  } // Other browsers
                  else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
                      browser = nAgt.substring(nameOffset, verOffset);
                      version = nAgt.substring(verOffset + 1);

                      if (browser.toLowerCase() == browser.toUpperCase()) {
                        browser = navigator.appName;
                      }
                    } // trim the version string


      if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
      if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
      if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);
      majorVersion = parseInt('' + version, 10);

      if (isNaN(majorVersion)) {
        version = '' + parseFloat(navigator.appVersion);
        majorVersion = parseInt(navigator.appVersion, 10);
      } // mobile version


      var mobile = /Mobile|mini|Fennec|Android|iP(ad|od|hone)/.test(nVer); // cookie

      var cookieEnabled = navigator.cookieEnabled ? true : false;

      if (typeof navigator.cookieEnabled == 'undefined' && !cookieEnabled) {
        document.cookie = 'testcookie';
        cookieEnabled = document.cookie.indexOf('testcookie') != -1 ? true : false;
      } // system


      var os = unknown;
      var clientStrings = [{
        s: 'Windows 10',
        r: /(Windows 10.0|Windows NT 10.0)/
      }, {
        s: 'Windows 8.1',
        r: /(Windows 8.1|Windows NT 6.3)/
      }, {
        s: 'Windows 8',
        r: /(Windows 8|Windows NT 6.2)/
      }, {
        s: 'Windows 7',
        r: /(Windows 7|Windows NT 6.1)/
      }, {
        s: 'Windows Vista',
        r: /Windows NT 6.0/
      }, {
        s: 'Windows Server 2003',
        r: /Windows NT 5.2/
      }, {
        s: 'Windows XP',
        r: /(Windows NT 5.1|Windows XP)/
      }, {
        s: 'Windows 2000',
        r: /(Windows NT 5.0|Windows 2000)/
      }, {
        s: 'Windows ME',
        r: /(Win 9x 4.90|Windows ME)/
      }, {
        s: 'Windows 98',
        r: /(Windows 98|Win98)/
      }, {
        s: 'Windows 95',
        r: /(Windows 95|Win95|Windows_95)/
      }, {
        s: 'Windows NT 4.0',
        r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/
      }, {
        s: 'Windows CE',
        r: /Windows CE/
      }, {
        s: 'Windows 3.11',
        r: /Win16/
      }, {
        s: 'Android',
        r: /Android/
      }, {
        s: 'Open BSD',
        r: /OpenBSD/
      }, {
        s: 'Sun OS',
        r: /SunOS/
      }, {
        s: 'Linux',
        r: /(Linux|X11)/
      }, {
        s: 'iOS',
        r: /(iPhone|iPad|iPod)/
      }, {
        s: 'Mac OS X',
        r: /Mac OS X/
      }, {
        s: 'Mac OS',
        r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/
      }, {
        s: 'QNX',
        r: /QNX/
      }, {
        s: 'UNIX',
        r: /UNIX/
      }, {
        s: 'BeOS',
        r: /BeOS/
      }, {
        s: 'OS/2',
        r: /OS\/2/
      }, {
        s: 'Search Bot',
        r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/
      }];

      for (var id in clientStrings) {
        var cs = clientStrings[id];

        if (cs.r.test(nAgt)) {
          os = cs.s;
          break;
        }
      }

      var osVersion = unknown;

      if (/Windows/.test(os)) {
        osVersion = /Windows (.*)/.exec(os)[1];
        os = 'Windows';
      }

      switch (os) {
        case 'Mac OS X':
          osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
          break;

        case 'Android':
          osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
          break;

        case 'iOS':
          osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
          osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
          break;
      } // flash (you'll need to include swfobject)

      /* script src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js" */


      var flashVersion = 'no check';

      if (typeof swfobject != 'undefined') {
        var fv = swfobject.getFlashPlayerVersion();

        if (fv.major > 0) {
          flashVersion = fv.major + '.' + fv.minor + ' r' + fv.release;
        } else {
          flashVersion = unknown;
        }
      }

      window.deviceInfo = {
        screen: screenSize,
        browser: browser,
        browserVersion: version,
        browserMajorVersion: majorVersion,
        mobile: mobile,
        os: os,
        osVersion: osVersion,
        cookies: cookieEnabled,
        flashVersion: flashVersion
      };
    }

    if (callback != null) callback(window.deviceInfo);
  },
  SAFARI: "safari",
  CHROME: "chrome",
  FIREFOX: "firefox",
  OPERA: "opera",
  WEBVIEW: "webview",
  isPortain: function () {
    console.log('isPortain');
  },
  restoreConsole: function (params) {
    //<iframe> element
    var iframe = document.createElement("iframe"); //Hide it somewhere

    iframe.style.position = "fixed";
    iframe.style.height = iframe.style.width = "1px";
    iframe.style.top = iframe.style.left = "-5px";
    iframe.style.display = "none"; //No src to prevent loading some data

    iframe.src = "about: blank"; //Needs append to work

    document.body.appendChild(iframe); //Get the inner console

    window.console = {};
    window.console = iframe.contentWindow.console; // delete iframe;
  },
  removeConsole: function () {
    // console.clear()
    for (var i in console) {
      console[i] = function () {};
    }
  },
  showCredit: function () {
    console.log("Developed by Digitop | https://wearetopgroup.com/");
  }
};
/* harmony default export */ __webpack_exports__["default"] = (TBrowser);

/***/ }),

/***/ "./plugins/utils/ConfigLive.js":
/*!*************************************!*\
  !*** ./plugins/utils/ConfigLive.js ***!
  \*************************************/
/*! exports provided: ConfigLive, IsDev, IsStag, IsProd, IsCanary */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfigLive", function() { return ConfigLive; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IsDev", function() { return IsDev; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IsStag", function() { return IsStag; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IsProd", function() { return IsProd; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IsCanary", function() { return IsCanary; });
/* harmony import */ var plugins_teexii_TBrowser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! plugins/teexii/TBrowser */ "./plugins/teexii/TBrowser.js");
/* harmony import */ var web_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! web.config */ "./web.config.js");


const ConfigLive = {
  consoleHandle: (showCredit = true) => {
    if (IsProd()) {
      console.clear();
      if (showCredit) plugins_teexii_TBrowser__WEBPACK_IMPORTED_MODULE_0__["default"].showCredit();
      plugins_teexii_TBrowser__WEBPACK_IMPORTED_MODULE_0__["default"].removeConsole();
    }
  }
};
const IsDev = function () {
  return web_config__WEBPACK_IMPORTED_MODULE_1__["default"].environment == web_config__WEBPACK_IMPORTED_MODULE_1__["ENVIRONMENT_DATA"].DEVELOPMENT;
};
const IsStag = function () {
  return web_config__WEBPACK_IMPORTED_MODULE_1__["default"].environment == web_config__WEBPACK_IMPORTED_MODULE_1__["ENVIRONMENT_DATA"].STAGING;
};
const IsProd = function () {
  return web_config__WEBPACK_IMPORTED_MODULE_1__["default"].environment == web_config__WEBPACK_IMPORTED_MODULE_1__["ENVIRONMENT_DATA"].PRODUCTION;
};
const IsCanary = function () {
  return web_config__WEBPACK_IMPORTED_MODULE_1__["default"].environment == web_config__WEBPACK_IMPORTED_MODULE_1__["ENVIRONMENT_DATA"].CANARY;
};

/***/ }),

/***/ "./styles/article-detail.scss":
/*!************************************!*\
  !*** ./styles/article-detail.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/articles.scss":
/*!******************************!*\
  !*** ./styles/articles.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/footer.scss":
/*!****************************!*\
  !*** ./styles/footer.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/global.scss":
/*!****************************!*\
  !*** ./styles/global.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/header.scss":
/*!****************************!*\
  !*** ./styles/header.scss ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/hero-text.scss":
/*!*******************************!*\
  !*** ./styles/hero-text.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./styles/hero.scss":
/*!**************************!*\
  !*** ./styles/hero.scss ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./web.config.js":
/*!***********************!*\
  !*** ./web.config.js ***!
  \***********************/
/*! exports provided: ENVIRONMENT_DATA, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENVIRONMENT_DATA", function() { return ENVIRONMENT_DATA; });
const ENVIRONMENT_DATA = {
  PRODUCTION: "production",
  STAGING: "staging",
  DEVELOPMENT: "development"
};
const CONFIG = {
  environment: process.env.NEXT_PUBLIC_ENV || "development",
  site: {
    title: "Diginext Website",
    description: "Description goes here",
    type: "article"
  },
  links: {
    facebookPage: ""
  },
  dateFormat: "yyyy-MM-dd HH:mm:ss",
  // these variables can be exposed to front-end:
  NEXT_PUBLIC_FB_APP_ID: process.env.NEXT_PUBLIC_FB_APP_ID || "",
  // currently using XXX
  NEXT_PUBLIC_FB_PAGE_ID: process.env.NEXT_PUBLIC_FB_PAGE_ID || "1162214040532902",
  // currently using Digitop developers
  NEXT_PUBLIC_BASE_PATH: process.env.NEXT_PUBLIC_BASE_PATH || "",
  NEXT_PUBLIC_API_BASE_PATH: process.env.NEXT_PUBLIC_API_BASE_PATH || "",
  NEXT_PUBLIC_CDN_BASE_PATH: process.env.NEXT_PUBLIC_CDN_BASE_PATH || "",
  NEXT_PUBLIC_APP_DOMAIN: process.env.NEXT_PUBLIC_APP_DOMAIN || "",
  NEXT_PUBLIC_BASE_URL: process.env.NEXT_PUBLIC_BASE_URL || "",
  // some secret keys which won't be exposed to front-end:
  SOME_SECRET_KEY: process.env.SOME_SECRET_KEY || "",
  IRON_SESSION_NAME: "DIGINEXTADMINCOOKIE",
  IRON_SESSION_SECRET: process.env.IRON_SESSION_SECRET || "",

  get SESSION_NAME() {
    return `DIGINEXTAPPCOOKIE`;
  },

  getBasePath: () => {
    return CONFIG.NEXT_PUBLIC_BASE_PATH ? "/" + CONFIG.NEXT_PUBLIC_BASE_PATH : "";
  },
  getBaseUrl: () => {
    return CONFIG.NEXT_PUBLIC_BASE_URL ? CONFIG.NEXT_PUBLIC_BASE_URL : "";
  },
  path: path => {
    return CONFIG.getBasePath() + path;
  }
};

if (false) {} else {// console.log(CONFIG);
  }

/* harmony default export */ __webpack_exports__["default"] = (CONFIG);

/***/ }),

/***/ 0:
/*!****************************************!*\
  !*** multi private-next-pages/_app.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! private-next-pages/_app.js */"./pages/_app.js");


/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvX2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9wbHVnaW5zL3RlZXhpaS9UQnJvd3Nlci5qcyIsIndlYnBhY2s6Ly8vLi9wbHVnaW5zL3V0aWxzL0NvbmZpZ0xpdmUuanMiLCJ3ZWJwYWNrOi8vLy4vd2ViLmNvbmZpZy5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIl0sIm5hbWVzIjpbIk15QXBwIiwiQ29tcG9uZW50IiwicGFnZVByb3BzIiwidXNlRWZmZWN0IiwiQ29uZmlnTGl2ZSIsImNvbnNvbGVIYW5kbGUiLCJUQnJvd3NlciIsImRlbGV0ZUFsbENvb2tpZXMiLCJjb29raWVzIiwiZG9jdW1lbnQiLCJjb29raWUiLCJzcGxpdCIsImkiLCJsZW5ndGgiLCJlcVBvcyIsImluZGV4T2YiLCJuYW1lIiwic3Vic3RyIiwic2V0Q29va2llIiwiY25hbWUiLCJjdmFsdWUiLCJleGRheXMiLCJ1bmRlZmluZWQiLCJkIiwiRGF0ZSIsInNldFRpbWUiLCJnZXRUaW1lIiwiZXhwaXJlcyIsInRvVVRDU3RyaW5nIiwiZ2V0Q29va2llIiwiY2EiLCJjIiwiY2hhckF0Iiwic3Vic3RyaW5nIiwiaGFzQ29va2llIiwiX25hbWUiLCJfc3RyIiwidEJyb3dzZXIiLCLDpyIsImlzTW9iaWxlIiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwibWF0Y2giLCJpc1N1cHBvcnRXZWJHTCIsIkRldGVjdG9yIiwiY2FudmFzIiwid2luZG93IiwiQ2FudmFzUmVuZGVyaW5nQ29udGV4dDJEIiwid2ViZ2wiLCJjcmVhdGVFbGVtZW50IiwiV2ViR0xSZW5kZXJpbmdDb250ZXh0IiwiZ2V0Q29udGV4dCIsImUiLCJ3b3JrZXJzIiwiV29ya2VyIiwiZmlsZWFwaSIsIkZpbGUiLCJGaWxlUmVhZGVyIiwiRmlsZUxpc3QiLCJCbG9iIiwiZ2V0V2ViR0xFcnJvck1lc3NhZ2UiLCJlbGVtZW50IiwiaWQiLCJzdHlsZSIsImZvbnRGYW1pbHkiLCJmb250U2l6ZSIsImZvbnRXZWlnaHQiLCJ0ZXh0QWxpZ24iLCJiYWNrZ3JvdW5kIiwiY29sb3IiLCJwYWRkaW5nIiwid2lkdGgiLCJtYXJnaW4iLCJpbm5lckhUTUwiLCJqb2luIiwiYWRkR2V0V2ViR0xNZXNzYWdlIiwicGFyYW1ldGVycyIsInBhcmVudCIsImJvZHkiLCJhcHBlbmRDaGlsZCIsImdldFVybFBhcmFtZXRlcnMiLCJwYXJhbWV0ZXIiLCJzdGF0aWNVUkwiLCJkZWNvZGUiLCJsb2NhdGlvbiIsImN1cnJMb2NhdGlvbiIsInNlYXJjaCIsInBhckFyciIsInJldHVybkJvb2wiLCJwYXJyIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwiY2hlY2tPUyIsIm9wdGlvbiIsImNhbGxiYWNrIiwiaGFzT3duUHJvcGVydHkiLCJkZXZpY2VJbmZvIiwidW5rbm93biIsInNjcmVlblNpemUiLCJzY3JlZW4iLCJoZWlnaHQiLCJuVmVyIiwiYXBwVmVyc2lvbiIsIm5BZ3QiLCJicm93c2VyIiwiYXBwTmFtZSIsInZlcnNpb24iLCJwYXJzZUZsb2F0IiwibWFqb3JWZXJzaW9uIiwicGFyc2VJbnQiLCJuYW1lT2Zmc2V0IiwidmVyT2Zmc2V0IiwiaXgiLCJsYXN0SW5kZXhPZiIsInRvTG93ZXJDYXNlIiwidG9VcHBlckNhc2UiLCJpc05hTiIsIm1vYmlsZSIsInRlc3QiLCJjb29raWVFbmFibGVkIiwib3MiLCJjbGllbnRTdHJpbmdzIiwicyIsInIiLCJjcyIsIm9zVmVyc2lvbiIsImV4ZWMiLCJmbGFzaFZlcnNpb24iLCJzd2ZvYmplY3QiLCJmdiIsImdldEZsYXNoUGxheWVyVmVyc2lvbiIsIm1ham9yIiwibWlub3IiLCJyZWxlYXNlIiwiYnJvd3NlclZlcnNpb24iLCJicm93c2VyTWFqb3JWZXJzaW9uIiwiU0FGQVJJIiwiQ0hST01FIiwiRklSRUZPWCIsIk9QRVJBIiwiV0VCVklFVyIsImlzUG9ydGFpbiIsImNvbnNvbGUiLCJsb2ciLCJyZXN0b3JlQ29uc29sZSIsInBhcmFtcyIsImlmcmFtZSIsInBvc2l0aW9uIiwidG9wIiwibGVmdCIsImRpc3BsYXkiLCJzcmMiLCJjb250ZW50V2luZG93IiwicmVtb3ZlQ29uc29sZSIsInNob3dDcmVkaXQiLCJJc1Byb2QiLCJjbGVhciIsIklzRGV2IiwiQ09ORklHIiwiZW52aXJvbm1lbnQiLCJFTlZJUk9OTUVOVF9EQVRBIiwiREVWRUxPUE1FTlQiLCJJc1N0YWciLCJTVEFHSU5HIiwiUFJPRFVDVElPTiIsIklzQ2FuYXJ5IiwiQ0FOQVJZIiwicHJvY2VzcyIsImVudiIsIk5FWFRfUFVCTElDX0VOViIsInNpdGUiLCJ0aXRsZSIsImRlc2NyaXB0aW9uIiwidHlwZSIsImxpbmtzIiwiZmFjZWJvb2tQYWdlIiwiZGF0ZUZvcm1hdCIsIk5FWFRfUFVCTElDX0ZCX0FQUF9JRCIsIk5FWFRfUFVCTElDX0ZCX1BBR0VfSUQiLCJORVhUX1BVQkxJQ19CQVNFX1BBVEgiLCJORVhUX1BVQkxJQ19BUElfQkFTRV9QQVRIIiwiTkVYVF9QVUJMSUNfQ0ROX0JBU0VfUEFUSCIsIk5FWFRfUFVCTElDX0FQUF9ET01BSU4iLCJORVhUX1BVQkxJQ19CQVNFX1VSTCIsIlNPTUVfU0VDUkVUX0tFWSIsIklST05fU0VTU0lPTl9OQU1FIiwiSVJPTl9TRVNTSU9OX1NFQ1JFVCIsIlNFU1NJT05fTkFNRSIsImdldEJhc2VQYXRoIiwiZ2V0QmFzZVVybCIsInBhdGgiXSwibWFwcGluZ3MiOiI7O1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxJQUFJO1FBQ0o7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0EsS0FBVCxDQUFlO0FBQUVDLFdBQUY7QUFBYUM7QUFBYixDQUFmLEVBQXlDO0FBQ3ZDQyx5REFBUyxDQUFDLE1BQU07QUFDZEMsdUVBQVUsQ0FBQ0MsYUFBWDtBQUNBLFdBQU8sTUFBTSxDQUFFLENBQWY7QUFDRCxHQUhRLEVBR04sRUFITSxDQUFUO0FBS0EsU0FBTyxNQUFDLFNBQUQsRUFBZUgsU0FBZixDQUFQO0FBQ0Q7O0FBRWNGLG9FQUFmLEU7Ozs7Ozs7Ozs7OztBQ3ZCQTtBQUFBO0FBQU8sTUFBTU0sUUFBUSxHQUFHO0FBQ3BCQyxrQkFBZ0IsRUFBRSxZQUFZO0FBQzFCLFFBQUlDLE9BQU8sR0FBR0MsUUFBUSxDQUFDQyxNQUFULENBQWdCQyxLQUFoQixDQUFzQixHQUF0QixDQUFkOztBQUNBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0osT0FBTyxDQUFDSyxNQUE1QixFQUFvQ0QsQ0FBQyxFQUFyQyxFQUF5QztBQUNyQyxVQUFJRixNQUFNLEdBQUdGLE9BQU8sQ0FBQ0ksQ0FBRCxDQUFwQjtBQUNBLFVBQUlFLEtBQUssR0FBR0osTUFBTSxDQUFDSyxPQUFQLENBQWUsR0FBZixDQUFaO0FBQ0EsVUFBSUMsSUFBSSxHQUFHRixLQUFLLEdBQUcsQ0FBQyxDQUFULEdBQWFKLE1BQU0sQ0FBQ08sTUFBUCxDQUFjLENBQWQsRUFBaUJILEtBQWpCLENBQWIsR0FBdUNKLE1BQWxEO0FBQ0FELGNBQVEsQ0FBQ0MsTUFBVCxHQUFrQk0sSUFBSSxHQUFHLHlDQUF6QjtBQUNIO0FBQ0osR0FUbUI7QUFXcEJFLFdBQVMsRUFBRSxVQUFVQyxLQUFWLEVBQWlCQyxNQUFqQixFQUF5QkMsTUFBekIsRUFBaUM7QUFDeEMsUUFBSUEsTUFBTSxJQUFJQyxTQUFkLEVBQXlCRCxNQUFNLEdBQUcsR0FBVDtBQUN6QixRQUFJRSxDQUFDLEdBQUcsSUFBSUMsSUFBSixFQUFSO0FBQ0FELEtBQUMsQ0FBQ0UsT0FBRixDQUFVRixDQUFDLENBQUNHLE9BQUYsS0FBZUwsTUFBTSxHQUFHLEVBQVQsR0FBYyxFQUFkLEdBQW1CLEVBQW5CLEdBQXdCLElBQWpEO0FBQ0EsUUFBSU0sT0FBTyxHQUFHLGFBQWFKLENBQUMsQ0FBQ0ssV0FBRixFQUEzQjtBQUNBbkIsWUFBUSxDQUFDQyxNQUFULEdBQWtCUyxLQUFLLEdBQUcsR0FBUixHQUFjQyxNQUFkLEdBQXVCLEdBQXZCLEdBQTZCTyxPQUE3QixHQUF1QyxTQUF6RDtBQUNILEdBakJtQjtBQW1CcEJFLFdBQVMsRUFBRSxVQUFVVixLQUFWLEVBQWlCO0FBQ3hCLFFBQUlILElBQUksR0FBR0csS0FBSyxHQUFHLEdBQW5CO0FBQ0EsUUFBSVcsRUFBRSxHQUFHckIsUUFBUSxDQUFDQyxNQUFULENBQWdCQyxLQUFoQixDQUFzQixHQUF0QixDQUFUOztBQUNBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR2tCLEVBQUUsQ0FBQ2pCLE1BQXZCLEVBQStCRCxDQUFDLEVBQWhDLEVBQW9DO0FBQ2hDLFVBQUltQixDQUFDLEdBQUdELEVBQUUsQ0FBQ2xCLENBQUQsQ0FBVjs7QUFDQSxhQUFPbUIsQ0FBQyxDQUFDQyxNQUFGLENBQVMsQ0FBVCxLQUFlLEdBQXRCLEVBQTJCO0FBQ3ZCRCxTQUFDLEdBQUdBLENBQUMsQ0FBQ0UsU0FBRixDQUFZLENBQVosQ0FBSjtBQUNIOztBQUNELFVBQUlGLENBQUMsQ0FBQ2hCLE9BQUYsQ0FBVUMsSUFBVixLQUFtQixDQUF2QixFQUEwQjtBQUN0QixlQUFPZSxDQUFDLENBQUNFLFNBQUYsQ0FBWWpCLElBQUksQ0FBQ0gsTUFBakIsRUFBeUJrQixDQUFDLENBQUNsQixNQUEzQixDQUFQO0FBQ0g7QUFDSjs7QUFDRCxXQUFPLEVBQVA7QUFDSCxHQWhDbUI7QUFrQ3BCcUIsV0FBUyxFQUFFLFVBQVVDLEtBQVYsRUFBaUI7QUFDeEIsUUFBSUMsSUFBSSxHQUFHQyxRQUFRLENBQUNSLFNBQVQsQ0FBbUJNLEtBQW5CLENBQVg7O0FBQ0EsUUFBSUMsSUFBSSxJQUFJLEVBQVosRUFBZ0I7QUFDWixhQUFPLElBQVA7QUFBYUUsT0FBQztBQUNqQixLQUZELE1BRU87QUFDSCxhQUFPLEtBQVA7QUFDSDtBQUNKLEdBekNtQjtBQTJDcEJDLFVBQVEsRUFBRSxZQUFZO0FBQ2xCLFFBQUlDLFNBQVMsQ0FBQ0MsU0FBVixDQUFvQkMsS0FBcEIsQ0FBMEIsVUFBMUIsS0FDR0YsU0FBUyxDQUFDQyxTQUFWLENBQW9CQyxLQUFwQixDQUEwQixRQUExQixDQURILElBRUdGLFNBQVMsQ0FBQ0MsU0FBVixDQUFvQkMsS0FBcEIsQ0FBMEIsU0FBMUIsQ0FGSCxJQUdHRixTQUFTLENBQUNDLFNBQVYsQ0FBb0JDLEtBQXBCLENBQTBCLE9BQTFCLENBSEgsSUFJR0YsU0FBUyxDQUFDQyxTQUFWLENBQW9CQyxLQUFwQixDQUEwQixPQUExQixDQUpILElBS0dGLFNBQVMsQ0FBQ0MsU0FBVixDQUFvQkMsS0FBcEIsQ0FBMEIsYUFBMUIsQ0FMSCxJQU1HRixTQUFTLENBQUNDLFNBQVYsQ0FBb0JDLEtBQXBCLENBQTBCLGdCQUExQixDQU5QLEVBT0U7QUFDRSxhQUFPLElBQVA7QUFDSCxLQVRELE1BVUs7QUFDRCxhQUFPLEtBQVA7QUFDSDtBQUNKLEdBekRtQjtBQTJEcEJDLGdCQUFjLEVBQUUsWUFBWTtBQUV4QjtBQUNBLFFBQUlDLFFBQVEsR0FBRztBQUNYQyxZQUFNLEVBQUUsQ0FBQyxDQUFDQyxNQUFNLENBQUNDLHdCQUROO0FBRVhDLFdBQUssRUFBRyxZQUFZO0FBQ2hCO0FBQ0EsWUFBSTtBQUVBLGNBQUlILE1BQU0sR0FBR3BDLFFBQVEsQ0FBQ3dDLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBYjtBQUErQyxpQkFBTyxDQUFDLEVBQUVILE1BQU0sQ0FBQ0kscUJBQVAsS0FBaUNMLE1BQU0sQ0FBQ00sVUFBUCxDQUFrQixPQUFsQixLQUE4Qk4sTUFBTSxDQUFDTSxVQUFQLENBQWtCLG9CQUFsQixDQUEvRCxDQUFGLENBQVI7QUFFbEQsU0FKRCxDQUlFLE9BQU9DLENBQVAsRUFBVTtBQUVSLGlCQUFPLEtBQVA7QUFFSDtBQUVKLE9BWk0sRUFGSTtBQWVYQyxhQUFPLEVBQUUsQ0FBQyxDQUFDUCxNQUFNLENBQUNRLE1BZlA7QUFnQlhDLGFBQU8sRUFBRVQsTUFBTSxDQUFDVSxJQUFQLElBQWVWLE1BQU0sQ0FBQ1csVUFBdEIsSUFBb0NYLE1BQU0sQ0FBQ1ksUUFBM0MsSUFBdURaLE1BQU0sQ0FBQ2EsSUFoQjVEO0FBa0JYQywwQkFBb0IsRUFBRSxZQUFZO0FBRTlCLFlBQUlDLE9BQU8sR0FBR3BELFFBQVEsQ0FBQ3dDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBZDtBQUNBWSxlQUFPLENBQUNDLEVBQVIsR0FBYSxxQkFBYjtBQUNBRCxlQUFPLENBQUNFLEtBQVIsQ0FBY0MsVUFBZCxHQUEyQixXQUEzQjtBQUNBSCxlQUFPLENBQUNFLEtBQVIsQ0FBY0UsUUFBZCxHQUF5QixNQUF6QjtBQUNBSixlQUFPLENBQUNFLEtBQVIsQ0FBY0csVUFBZCxHQUEyQixRQUEzQjtBQUNBTCxlQUFPLENBQUNFLEtBQVIsQ0FBY0ksU0FBZCxHQUEwQixRQUExQjtBQUNBTixlQUFPLENBQUNFLEtBQVIsQ0FBY0ssVUFBZCxHQUEyQixNQUEzQjtBQUNBUCxlQUFPLENBQUNFLEtBQVIsQ0FBY00sS0FBZCxHQUFzQixNQUF0QjtBQUNBUixlQUFPLENBQUNFLEtBQVIsQ0FBY08sT0FBZCxHQUF3QixPQUF4QjtBQUNBVCxlQUFPLENBQUNFLEtBQVIsQ0FBY1EsS0FBZCxHQUFzQixPQUF0QjtBQUNBVixlQUFPLENBQUNFLEtBQVIsQ0FBY1MsTUFBZCxHQUF1QixZQUF2Qjs7QUFFQSxZQUFJLENBQUMsS0FBS3hCLEtBQVYsRUFBaUI7QUFFYmEsaUJBQU8sQ0FBQ1ksU0FBUixHQUFvQjNCLE1BQU0sQ0FBQ0kscUJBQVAsR0FBK0IsQ0FDL0Msd0pBRCtDLEVBRS9DLHFGQUYrQyxFQUdqRHdCLElBSGlELENBRzVDLElBSDRDLENBQS9CLEdBR0wsQ0FDWCxpSkFEVyxFQUVYLHFGQUZXLEVBR2JBLElBSGEsQ0FHUixJQUhRLENBSGY7QUFRSDs7QUFFRCxlQUFPYixPQUFQO0FBRUgsT0E5Q1U7QUFnRFhjLHdCQUFrQixFQUFFLFVBQVVDLFVBQVYsRUFBc0I7QUFFdEMsWUFBSUMsTUFBSixFQUFZZixFQUFaLEVBQWdCRCxPQUFoQjtBQUVBZSxrQkFBVSxHQUFHQSxVQUFVLElBQUksRUFBM0I7QUFFQUMsY0FBTSxHQUFHRCxVQUFVLENBQUNDLE1BQVgsS0FBc0J2RCxTQUF0QixHQUFrQ3NELFVBQVUsQ0FBQ0MsTUFBN0MsR0FBc0RwRSxRQUFRLENBQUNxRSxJQUF4RTtBQUNBaEIsVUFBRSxHQUFHYyxVQUFVLENBQUNkLEVBQVgsS0FBa0J4QyxTQUFsQixHQUE4QnNELFVBQVUsQ0FBQ2QsRUFBekMsR0FBOEMsT0FBbkQ7QUFFQUQsZUFBTyxHQUFHakIsUUFBUSxDQUFDZ0Isb0JBQVQsRUFBVjtBQUNBQyxlQUFPLENBQUNDLEVBQVIsR0FBYUEsRUFBYjtBQUVBZSxjQUFNLENBQUNFLFdBQVAsQ0FBbUJsQixPQUFuQjtBQUVIO0FBOURVLEtBQWY7QUFrRUEsV0FBT2pCLFFBQVEsQ0FBQ0ksS0FBaEI7QUFDSCxHQWpJbUI7QUFvSXBCZ0Msa0JBQWdCLEVBQUUsVUFBVUMsU0FBVixFQUFxQkMsU0FBckIsRUFBZ0NDLE1BQWhDLEVBQXdDO0FBRXRERCxhQUFTLEdBQUdBLFNBQVMsSUFBSTVELFNBQWIsR0FBeUJ3QixNQUFNLENBQUNzQyxRQUFoQyxHQUEyQ0YsU0FBdkQ7QUFDQSxRQUFJRyxZQUFZLEdBQUlILFNBQVMsQ0FBQ3JFLE1BQVgsR0FBcUJxRSxTQUFyQixHQUFpQ3BDLE1BQU0sQ0FBQ3NDLFFBQVAsQ0FBZ0JFLE1BQXBFO0FBRUEsUUFBSUQsWUFBWSxDQUFDMUUsS0FBYixDQUFtQixHQUFuQixFQUF3QkUsTUFBeEIsR0FBaUMsQ0FBckMsRUFDSSxPQUFPLEVBQVA7QUFFSixRQUFJMEUsTUFBTSxHQUFHRixZQUFZLENBQUMxRSxLQUFiLENBQW1CLEdBQW5CLEVBQXdCLENBQXhCLEVBQTJCQSxLQUEzQixDQUFpQyxHQUFqQyxDQUFiO0FBQUEsUUFDSTZFLFVBQVUsR0FBRyxJQURqQjs7QUFHQSxTQUFLLElBQUk1RSxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHMkUsTUFBTSxDQUFDMUUsTUFBM0IsRUFBbUNELENBQUMsRUFBcEMsRUFBd0M7QUFDcEMsVUFBSTZFLElBQUksR0FBR0YsTUFBTSxDQUFDM0UsQ0FBRCxDQUFOLENBQVVELEtBQVYsQ0FBZ0IsR0FBaEIsQ0FBWDs7QUFDQSxVQUFJOEUsSUFBSSxDQUFDLENBQUQsQ0FBSixJQUFXUixTQUFmLEVBQTBCO0FBQ3RCLGVBQVFFLE1BQUQsR0FBV08sa0JBQWtCLENBQUNELElBQUksQ0FBQyxDQUFELENBQUwsQ0FBN0IsR0FBeUNBLElBQUksQ0FBQyxDQUFELENBQXBEO0FBQ0FELGtCQUFVLEdBQUcsSUFBYjtBQUNILE9BSEQsTUFHTztBQUNIQSxrQkFBVSxHQUFHLEtBQWI7QUFDSDtBQUNKOztBQUVELFFBQUksQ0FBQ0EsVUFBTCxFQUFpQixPQUFPLEtBQVA7QUFDcEIsR0ExSm1CO0FBNkpwQkcsU0FBTyxFQUFFLFVBQVVDLE1BQVYsRUFBa0I7QUFDdkJBLFVBQU0sR0FBR0EsTUFBTSxJQUFJdEUsU0FBVixHQUFzQnNFLE1BQXRCLEdBQStCLEVBQXhDO0FBRUEsUUFBSUMsUUFBUSxHQUFHRCxNQUFNLENBQUNFLGNBQVAsQ0FBc0IsVUFBdEIsSUFBb0NGLE1BQU0sQ0FBQ0MsUUFBM0MsR0FBc0QsSUFBckU7O0FBRUEsUUFBSSxPQUFPL0MsTUFBTSxDQUFDaUQsVUFBZCxJQUE0QixXQUFoQyxFQUE2QztBQUN6QyxVQUFJQyxPQUFPLEdBQUcsR0FBZCxDQUR5QyxDQUd6Qzs7QUFDQSxVQUFJQyxVQUFVLEdBQUcsRUFBakI7O0FBQ0EsVUFBSUMsTUFBTSxDQUFDM0IsS0FBWCxFQUFrQjtBQUNkLFlBQUlBLEtBQUssR0FBSTJCLE1BQU0sQ0FBQzNCLEtBQVIsR0FBaUIyQixNQUFNLENBQUMzQixLQUF4QixHQUFnQyxFQUE1QztBQUNBLFlBQUk0QixNQUFNLEdBQUlELE1BQU0sQ0FBQ0MsTUFBUixHQUFrQkQsTUFBTSxDQUFDQyxNQUF6QixHQUFrQyxFQUEvQztBQUNBRixrQkFBVSxJQUFJLEtBQUsxQixLQUFMLEdBQWEsS0FBYixHQUFxQjRCLE1BQW5DO0FBQ0gsT0FUd0MsQ0FXekM7OztBQUNBLFVBQUlDLElBQUksR0FBRzVELFNBQVMsQ0FBQzZELFVBQXJCO0FBQ0EsVUFBSUMsSUFBSSxHQUFHOUQsU0FBUyxDQUFDQyxTQUFyQjtBQUNBLFVBQUk4RCxPQUFPLEdBQUcvRCxTQUFTLENBQUNnRSxPQUF4QjtBQUNBLFVBQUlDLE9BQU8sR0FBRyxLQUFLQyxVQUFVLENBQUNsRSxTQUFTLENBQUM2RCxVQUFYLENBQTdCO0FBQ0EsVUFBSU0sWUFBWSxHQUFHQyxRQUFRLENBQUNwRSxTQUFTLENBQUM2RCxVQUFYLEVBQXVCLEVBQXZCLENBQTNCO0FBQ0EsVUFBSVEsVUFBSixFQUFnQkMsU0FBaEIsRUFBMkJDLEVBQTNCLENBakJ5QyxDQW1CekM7O0FBQ0EsVUFBSSxDQUFDRCxTQUFTLEdBQUdSLElBQUksQ0FBQ3ZGLE9BQUwsQ0FBYSxPQUFiLENBQWIsS0FBdUMsQ0FBQyxDQUE1QyxFQUErQztBQUMzQ3dGLGVBQU8sR0FBRyxPQUFWO0FBQ0FFLGVBQU8sR0FBR0gsSUFBSSxDQUFDckUsU0FBTCxDQUFlNkUsU0FBUyxHQUFHLENBQTNCLENBQVY7O0FBQ0EsWUFBSSxDQUFDQSxTQUFTLEdBQUdSLElBQUksQ0FBQ3ZGLE9BQUwsQ0FBYSxTQUFiLENBQWIsS0FBeUMsQ0FBQyxDQUE5QyxFQUFpRDtBQUM3QzBGLGlCQUFPLEdBQUdILElBQUksQ0FBQ3JFLFNBQUwsQ0FBZTZFLFNBQVMsR0FBRyxDQUEzQixDQUFWO0FBQ0g7QUFDSixPQTFCd0MsQ0EyQnpDOzs7QUFDQSxVQUFJLENBQUNBLFNBQVMsR0FBR1IsSUFBSSxDQUFDdkYsT0FBTCxDQUFhLEtBQWIsQ0FBYixLQUFxQyxDQUFDLENBQTFDLEVBQTZDO0FBQ3pDd0YsZUFBTyxHQUFHLE9BQVY7QUFDQUUsZUFBTyxHQUFHSCxJQUFJLENBQUNyRSxTQUFMLENBQWU2RSxTQUFTLEdBQUcsQ0FBM0IsQ0FBVjtBQUNILE9BSEQsQ0FJQTtBQUpBLFdBS0ssSUFBSSxDQUFDQSxTQUFTLEdBQUdSLElBQUksQ0FBQ3ZGLE9BQUwsQ0FBYSxNQUFiLENBQWIsS0FBc0MsQ0FBQyxDQUEzQyxFQUE4QztBQUMvQ3dGLGlCQUFPLEdBQUcsZ0JBQVY7QUFDQUUsaUJBQU8sR0FBR0gsSUFBSSxDQUFDckUsU0FBTCxDQUFlNkUsU0FBUyxHQUFHLENBQTNCLENBQVY7QUFDSCxTQUhJLENBSUw7QUFKSyxhQUtBLElBQUksQ0FBQ0EsU0FBUyxHQUFHUixJQUFJLENBQUN2RixPQUFMLENBQWEsTUFBYixDQUFiLEtBQXNDLENBQUMsQ0FBM0MsRUFBOEM7QUFDL0N3RixtQkFBTyxHQUFHLDZCQUFWO0FBQ0FFLG1CQUFPLEdBQUdILElBQUksQ0FBQ3JFLFNBQUwsQ0FBZTZFLFNBQVMsR0FBRyxDQUEzQixDQUFWO0FBQ0gsV0FISSxDQUlMO0FBSkssZUFLQSxJQUFJLENBQUNBLFNBQVMsR0FBR1IsSUFBSSxDQUFDdkYsT0FBTCxDQUFhLFFBQWIsQ0FBYixLQUF3QyxDQUFDLENBQTdDLEVBQWdEO0FBQ2pEd0YscUJBQU8sR0FBRyxRQUFWO0FBQ0FFLHFCQUFPLEdBQUdILElBQUksQ0FBQ3JFLFNBQUwsQ0FBZTZFLFNBQVMsR0FBRyxDQUEzQixDQUFWO0FBQ0gsYUFISSxDQUlMO0FBSkssaUJBS0EsSUFBSSxDQUFDQSxTQUFTLEdBQUdSLElBQUksQ0FBQ3ZGLE9BQUwsQ0FBYSxRQUFiLENBQWIsS0FBd0MsQ0FBQyxDQUE3QyxFQUFnRDtBQUNqRHdGLHVCQUFPLEdBQUcsUUFBVjtBQUNBRSx1QkFBTyxHQUFHSCxJQUFJLENBQUNyRSxTQUFMLENBQWU2RSxTQUFTLEdBQUcsQ0FBM0IsQ0FBVjs7QUFDQSxvQkFBSSxDQUFDQSxTQUFTLEdBQUdSLElBQUksQ0FBQ3ZGLE9BQUwsQ0FBYSxTQUFiLENBQWIsS0FBeUMsQ0FBQyxDQUE5QyxFQUFpRDtBQUM3QzBGLHlCQUFPLEdBQUdILElBQUksQ0FBQ3JFLFNBQUwsQ0FBZTZFLFNBQVMsR0FBRyxDQUEzQixDQUFWO0FBQ0g7QUFDSixlQU5JLENBT0w7QUFQSyxtQkFRQSxJQUFJLENBQUNBLFNBQVMsR0FBR1IsSUFBSSxDQUFDdkYsT0FBTCxDQUFhLFNBQWIsQ0FBYixLQUF5QyxDQUFDLENBQTlDLEVBQWlEO0FBQ2xEd0YseUJBQU8sR0FBRyxTQUFWO0FBQ0FFLHlCQUFPLEdBQUdILElBQUksQ0FBQ3JFLFNBQUwsQ0FBZTZFLFNBQVMsR0FBRyxDQUEzQixDQUFWO0FBQ0gsaUJBSEksQ0FJTDtBQUpLLHFCQUtBLElBQUlSLElBQUksQ0FBQ3ZGLE9BQUwsQ0FBYSxVQUFiLEtBQTRCLENBQUMsQ0FBakMsRUFBb0M7QUFDckN3RiwyQkFBTyxHQUFHLDZCQUFWO0FBQ0FFLDJCQUFPLEdBQUdILElBQUksQ0FBQ3JFLFNBQUwsQ0FBZXFFLElBQUksQ0FBQ3ZGLE9BQUwsQ0FBYSxLQUFiLElBQXNCLENBQXJDLENBQVY7QUFDSCxtQkFISSxDQUlMO0FBSkssdUJBS0EsSUFBSSxDQUFDOEYsVUFBVSxHQUFHUCxJQUFJLENBQUNVLFdBQUwsQ0FBaUIsR0FBakIsSUFBd0IsQ0FBdEMsS0FBNENGLFNBQVMsR0FBR1IsSUFBSSxDQUFDVSxXQUFMLENBQWlCLEdBQWpCLENBQXhELENBQUosRUFBb0Y7QUFDckZULDZCQUFPLEdBQUdELElBQUksQ0FBQ3JFLFNBQUwsQ0FBZTRFLFVBQWYsRUFBMkJDLFNBQTNCLENBQVY7QUFDQUwsNkJBQU8sR0FBR0gsSUFBSSxDQUFDckUsU0FBTCxDQUFlNkUsU0FBUyxHQUFHLENBQTNCLENBQVY7O0FBQ0EsMEJBQUlQLE9BQU8sQ0FBQ1UsV0FBUixNQUF5QlYsT0FBTyxDQUFDVyxXQUFSLEVBQTdCLEVBQW9EO0FBQ2hEWCwrQkFBTyxHQUFHL0QsU0FBUyxDQUFDZ0UsT0FBcEI7QUFDSDtBQUNKLHFCQXhFd0MsQ0F5RXpDOzs7QUFDQSxVQUFJLENBQUNPLEVBQUUsR0FBR04sT0FBTyxDQUFDMUYsT0FBUixDQUFnQixHQUFoQixDQUFOLEtBQStCLENBQUMsQ0FBcEMsRUFBdUMwRixPQUFPLEdBQUdBLE9BQU8sQ0FBQ3hFLFNBQVIsQ0FBa0IsQ0FBbEIsRUFBcUI4RSxFQUFyQixDQUFWO0FBQ3ZDLFVBQUksQ0FBQ0EsRUFBRSxHQUFHTixPQUFPLENBQUMxRixPQUFSLENBQWdCLEdBQWhCLENBQU4sS0FBK0IsQ0FBQyxDQUFwQyxFQUF1QzBGLE9BQU8sR0FBR0EsT0FBTyxDQUFDeEUsU0FBUixDQUFrQixDQUFsQixFQUFxQjhFLEVBQXJCLENBQVY7QUFDdkMsVUFBSSxDQUFDQSxFQUFFLEdBQUdOLE9BQU8sQ0FBQzFGLE9BQVIsQ0FBZ0IsR0FBaEIsQ0FBTixLQUErQixDQUFDLENBQXBDLEVBQXVDMEYsT0FBTyxHQUFHQSxPQUFPLENBQUN4RSxTQUFSLENBQWtCLENBQWxCLEVBQXFCOEUsRUFBckIsQ0FBVjtBQUV2Q0osa0JBQVksR0FBR0MsUUFBUSxDQUFDLEtBQUtILE9BQU4sRUFBZSxFQUFmLENBQXZCOztBQUNBLFVBQUlVLEtBQUssQ0FBQ1IsWUFBRCxDQUFULEVBQXlCO0FBQ3JCRixlQUFPLEdBQUcsS0FBS0MsVUFBVSxDQUFDbEUsU0FBUyxDQUFDNkQsVUFBWCxDQUF6QjtBQUNBTSxvQkFBWSxHQUFHQyxRQUFRLENBQUNwRSxTQUFTLENBQUM2RCxVQUFYLEVBQXVCLEVBQXZCLENBQXZCO0FBQ0gsT0FsRndDLENBb0Z6Qzs7O0FBQ0EsVUFBSWUsTUFBTSxHQUFHLDRDQUE0Q0MsSUFBNUMsQ0FBaURqQixJQUFqRCxDQUFiLENBckZ5QyxDQXVGekM7O0FBQ0EsVUFBSWtCLGFBQWEsR0FBSTlFLFNBQVMsQ0FBQzhFLGFBQVgsR0FBNEIsSUFBNUIsR0FBbUMsS0FBdkQ7O0FBRUEsVUFBSSxPQUFPOUUsU0FBUyxDQUFDOEUsYUFBakIsSUFBa0MsV0FBbEMsSUFBaUQsQ0FBQ0EsYUFBdEQsRUFBcUU7QUFDakU3RyxnQkFBUSxDQUFDQyxNQUFULEdBQWtCLFlBQWxCO0FBQ0E0RyxxQkFBYSxHQUFJN0csUUFBUSxDQUFDQyxNQUFULENBQWdCSyxPQUFoQixDQUF3QixZQUF4QixLQUF5QyxDQUFDLENBQTNDLEdBQWdELElBQWhELEdBQXVELEtBQXZFO0FBQ0gsT0E3RndDLENBK0Z6Qzs7O0FBQ0EsVUFBSXdHLEVBQUUsR0FBR3ZCLE9BQVQ7QUFDQSxVQUFJd0IsYUFBYSxHQUFHLENBQ2hCO0FBQUVDLFNBQUMsRUFBRSxZQUFMO0FBQW1CQyxTQUFDLEVBQUU7QUFBdEIsT0FEZ0IsRUFFaEI7QUFBRUQsU0FBQyxFQUFFLGFBQUw7QUFBb0JDLFNBQUMsRUFBRTtBQUF2QixPQUZnQixFQUdoQjtBQUFFRCxTQUFDLEVBQUUsV0FBTDtBQUFrQkMsU0FBQyxFQUFFO0FBQXJCLE9BSGdCLEVBSWhCO0FBQUVELFNBQUMsRUFBRSxXQUFMO0FBQWtCQyxTQUFDLEVBQUU7QUFBckIsT0FKZ0IsRUFLaEI7QUFBRUQsU0FBQyxFQUFFLGVBQUw7QUFBc0JDLFNBQUMsRUFBRTtBQUF6QixPQUxnQixFQU1oQjtBQUFFRCxTQUFDLEVBQUUscUJBQUw7QUFBNEJDLFNBQUMsRUFBRTtBQUEvQixPQU5nQixFQU9oQjtBQUFFRCxTQUFDLEVBQUUsWUFBTDtBQUFtQkMsU0FBQyxFQUFFO0FBQXRCLE9BUGdCLEVBUWhCO0FBQUVELFNBQUMsRUFBRSxjQUFMO0FBQXFCQyxTQUFDLEVBQUU7QUFBeEIsT0FSZ0IsRUFTaEI7QUFBRUQsU0FBQyxFQUFFLFlBQUw7QUFBbUJDLFNBQUMsRUFBRTtBQUF0QixPQVRnQixFQVVoQjtBQUFFRCxTQUFDLEVBQUUsWUFBTDtBQUFtQkMsU0FBQyxFQUFFO0FBQXRCLE9BVmdCLEVBV2hCO0FBQUVELFNBQUMsRUFBRSxZQUFMO0FBQW1CQyxTQUFDLEVBQUU7QUFBdEIsT0FYZ0IsRUFZaEI7QUFBRUQsU0FBQyxFQUFFLGdCQUFMO0FBQXVCQyxTQUFDLEVBQUU7QUFBMUIsT0FaZ0IsRUFhaEI7QUFBRUQsU0FBQyxFQUFFLFlBQUw7QUFBbUJDLFNBQUMsRUFBRTtBQUF0QixPQWJnQixFQWNoQjtBQUFFRCxTQUFDLEVBQUUsY0FBTDtBQUFxQkMsU0FBQyxFQUFFO0FBQXhCLE9BZGdCLEVBZWhCO0FBQUVELFNBQUMsRUFBRSxTQUFMO0FBQWdCQyxTQUFDLEVBQUU7QUFBbkIsT0FmZ0IsRUFnQmhCO0FBQUVELFNBQUMsRUFBRSxVQUFMO0FBQWlCQyxTQUFDLEVBQUU7QUFBcEIsT0FoQmdCLEVBaUJoQjtBQUFFRCxTQUFDLEVBQUUsUUFBTDtBQUFlQyxTQUFDLEVBQUU7QUFBbEIsT0FqQmdCLEVBa0JoQjtBQUFFRCxTQUFDLEVBQUUsT0FBTDtBQUFjQyxTQUFDLEVBQUU7QUFBakIsT0FsQmdCLEVBbUJoQjtBQUFFRCxTQUFDLEVBQUUsS0FBTDtBQUFZQyxTQUFDLEVBQUU7QUFBZixPQW5CZ0IsRUFvQmhCO0FBQUVELFNBQUMsRUFBRSxVQUFMO0FBQWlCQyxTQUFDLEVBQUU7QUFBcEIsT0FwQmdCLEVBcUJoQjtBQUFFRCxTQUFDLEVBQUUsUUFBTDtBQUFlQyxTQUFDLEVBQUU7QUFBbEIsT0FyQmdCLEVBc0JoQjtBQUFFRCxTQUFDLEVBQUUsS0FBTDtBQUFZQyxTQUFDLEVBQUU7QUFBZixPQXRCZ0IsRUF1QmhCO0FBQUVELFNBQUMsRUFBRSxNQUFMO0FBQWFDLFNBQUMsRUFBRTtBQUFoQixPQXZCZ0IsRUF3QmhCO0FBQUVELFNBQUMsRUFBRSxNQUFMO0FBQWFDLFNBQUMsRUFBRTtBQUFoQixPQXhCZ0IsRUF5QmhCO0FBQUVELFNBQUMsRUFBRSxNQUFMO0FBQWFDLFNBQUMsRUFBRTtBQUFoQixPQXpCZ0IsRUEwQmhCO0FBQUVELFNBQUMsRUFBRSxZQUFMO0FBQW1CQyxTQUFDLEVBQUU7QUFBdEIsT0ExQmdCLENBQXBCOztBQTRCQSxXQUFLLElBQUk1RCxFQUFULElBQWUwRCxhQUFmLEVBQThCO0FBQzFCLFlBQUlHLEVBQUUsR0FBR0gsYUFBYSxDQUFDMUQsRUFBRCxDQUF0Qjs7QUFDQSxZQUFJNkQsRUFBRSxDQUFDRCxDQUFILENBQUtMLElBQUwsQ0FBVWYsSUFBVixDQUFKLEVBQXFCO0FBQ2pCaUIsWUFBRSxHQUFHSSxFQUFFLENBQUNGLENBQVI7QUFDQTtBQUNIO0FBQ0o7O0FBRUQsVUFBSUcsU0FBUyxHQUFHNUIsT0FBaEI7O0FBRUEsVUFBSSxVQUFVcUIsSUFBVixDQUFlRSxFQUFmLENBQUosRUFBd0I7QUFDcEJLLGlCQUFTLEdBQUcsZUFBZUMsSUFBZixDQUFvQk4sRUFBcEIsRUFBd0IsQ0FBeEIsQ0FBWjtBQUNBQSxVQUFFLEdBQUcsU0FBTDtBQUNIOztBQUVELGNBQVFBLEVBQVI7QUFDSSxhQUFLLFVBQUw7QUFDSUssbUJBQVMsR0FBRyx5QkFBeUJDLElBQXpCLENBQThCdkIsSUFBOUIsRUFBb0MsQ0FBcEMsQ0FBWjtBQUNBOztBQUVKLGFBQUssU0FBTDtBQUNJc0IsbUJBQVMsR0FBRyxzQkFBc0JDLElBQXRCLENBQTJCdkIsSUFBM0IsRUFBaUMsQ0FBakMsQ0FBWjtBQUNBOztBQUVKLGFBQUssS0FBTDtBQUNJc0IsbUJBQVMsR0FBRyx5QkFBeUJDLElBQXpCLENBQThCekIsSUFBOUIsQ0FBWjtBQUNBd0IsbUJBQVMsR0FBR0EsU0FBUyxDQUFDLENBQUQsQ0FBVCxHQUFlLEdBQWYsR0FBcUJBLFNBQVMsQ0FBQyxDQUFELENBQTlCLEdBQW9DLEdBQXBDLElBQTJDQSxTQUFTLENBQUMsQ0FBRCxDQUFULEdBQWUsQ0FBMUQsQ0FBWjtBQUNBO0FBWlIsT0E1SXlDLENBMkp6Qzs7QUFDQTs7O0FBQ0EsVUFBSUUsWUFBWSxHQUFHLFVBQW5COztBQUNBLFVBQUksT0FBT0MsU0FBUCxJQUFvQixXQUF4QixFQUFxQztBQUNqQyxZQUFJQyxFQUFFLEdBQUdELFNBQVMsQ0FBQ0UscUJBQVYsRUFBVDs7QUFDQSxZQUFJRCxFQUFFLENBQUNFLEtBQUgsR0FBVyxDQUFmLEVBQWtCO0FBQ2RKLHNCQUFZLEdBQUdFLEVBQUUsQ0FBQ0UsS0FBSCxHQUFXLEdBQVgsR0FBaUJGLEVBQUUsQ0FBQ0csS0FBcEIsR0FBNEIsSUFBNUIsR0FBbUNILEVBQUUsQ0FBQ0ksT0FBckQ7QUFDSCxTQUZELE1BR0s7QUFDRE4sc0JBQVksR0FBRzlCLE9BQWY7QUFDSDtBQUNKOztBQUdEbEQsWUFBTSxDQUFDaUQsVUFBUCxHQUFvQjtBQUNoQkcsY0FBTSxFQUFFRCxVQURRO0FBRWhCTSxlQUFPLEVBQUVBLE9BRk87QUFHaEI4QixzQkFBYyxFQUFFNUIsT0FIQTtBQUloQjZCLDJCQUFtQixFQUFFM0IsWUFKTDtBQUtoQlMsY0FBTSxFQUFFQSxNQUxRO0FBTWhCRyxVQUFFLEVBQUVBLEVBTlk7QUFPaEJLLGlCQUFTLEVBQUVBLFNBUEs7QUFRaEJwSCxlQUFPLEVBQUU4RyxhQVJPO0FBU2hCUSxvQkFBWSxFQUFFQTtBQVRFLE9BQXBCO0FBV0g7O0FBRUQsUUFBSWpDLFFBQVEsSUFBSSxJQUFoQixFQUFzQkEsUUFBUSxDQUFDL0MsTUFBTSxDQUFDaUQsVUFBUixDQUFSO0FBQ3pCLEdBelZtQjtBQTJWcEJ3QyxRQUFNLEVBQUUsUUEzVlk7QUE0VnBCQyxRQUFNLEVBQUUsUUE1Vlk7QUE2VnBCQyxTQUFPLEVBQUUsU0E3Vlc7QUE4VnBCQyxPQUFLLEVBQUUsT0E5VmE7QUErVnBCQyxTQUFPLEVBQUUsU0EvVlc7QUFrV3BCQyxXQUFTLEVBQUUsWUFBWTtBQUNuQkMsV0FBTyxDQUFDQyxHQUFSLENBQVksV0FBWjtBQUNILEdBcFdtQjtBQXNXcEJDLGdCQUFjLEVBQUUsVUFBVUMsTUFBVixFQUFrQjtBQUM5QjtBQUNBLFFBQUlDLE1BQU0sR0FBR3hJLFFBQVEsQ0FBQ3dDLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBYixDQUY4QixDQUc5Qjs7QUFDQWdHLFVBQU0sQ0FBQ2xGLEtBQVAsQ0FBYW1GLFFBQWIsR0FBd0IsT0FBeEI7QUFDQUQsVUFBTSxDQUFDbEYsS0FBUCxDQUFhb0MsTUFBYixHQUFzQjhDLE1BQU0sQ0FBQ2xGLEtBQVAsQ0FBYVEsS0FBYixHQUFxQixLQUEzQztBQUNBMEUsVUFBTSxDQUFDbEYsS0FBUCxDQUFhb0YsR0FBYixHQUFtQkYsTUFBTSxDQUFDbEYsS0FBUCxDQUFhcUYsSUFBYixHQUFvQixNQUF2QztBQUNBSCxVQUFNLENBQUNsRixLQUFQLENBQWFzRixPQUFiLEdBQXVCLE1BQXZCLENBUDhCLENBUTlCOztBQUNBSixVQUFNLENBQUNLLEdBQVAsR0FBYSxjQUFiLENBVDhCLENBVTlCOztBQUNBN0ksWUFBUSxDQUFDcUUsSUFBVCxDQUFjQyxXQUFkLENBQTBCa0UsTUFBMUIsRUFYOEIsQ0FZOUI7O0FBQ0FuRyxVQUFNLENBQUMrRixPQUFQLEdBQWlCLEVBQWpCO0FBQ0EvRixVQUFNLENBQUMrRixPQUFQLEdBQWlCSSxNQUFNLENBQUNNLGFBQVAsQ0FBcUJWLE9BQXRDLENBZDhCLENBZ0I5QjtBQUNILEdBdlhtQjtBQXlYcEJXLGVBQWEsRUFBRSxZQUFZO0FBQ3ZCO0FBQ0EsU0FBSyxJQUFJNUksQ0FBVCxJQUFjaUksT0FBZCxFQUF1QjtBQUNuQkEsYUFBTyxDQUFDakksQ0FBRCxDQUFQLEdBQWEsWUFBWSxDQUFHLENBQTVCO0FBQ0g7QUFDSixHQTlYbUI7QUFnWXBCNkksWUFBVSxFQUFFLFlBQVk7QUFDcEJaLFdBQU8sQ0FBQ0MsR0FBUixDQUFZLG1EQUFaO0FBQ0g7QUFsWW1CLENBQWpCO0FBc1lReEksdUVBQWYsRTs7Ozs7Ozs7Ozs7O0FDdFlBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBRU8sTUFBTUYsVUFBVSxHQUFHO0FBQ3hCQyxlQUFhLEVBQUUsQ0FBQ29KLFVBQVUsR0FBRyxJQUFkLEtBQXVCO0FBQ3BDLFFBQUlDLE1BQU0sRUFBVixFQUFjO0FBQ1piLGFBQU8sQ0FBQ2MsS0FBUjtBQUNBLFVBQUlGLFVBQUosRUFBZ0JuSiwrREFBUSxDQUFDbUosVUFBVDtBQUNoQm5KLHFFQUFRLENBQUNrSixhQUFUO0FBQ0Q7QUFDRjtBQVB1QixDQUFuQjtBQVVBLE1BQU1JLEtBQUssR0FBRyxZQUFZO0FBQy9CLFNBQU9DLGtEQUFNLENBQUNDLFdBQVAsSUFBc0JDLDJEQUFnQixDQUFDQyxXQUE5QztBQUNELENBRk07QUFJQSxNQUFNQyxNQUFNLEdBQUcsWUFBWTtBQUNoQyxTQUFPSixrREFBTSxDQUFDQyxXQUFQLElBQXNCQywyREFBZ0IsQ0FBQ0csT0FBOUM7QUFDRCxDQUZNO0FBSUEsTUFBTVIsTUFBTSxHQUFHLFlBQVk7QUFDaEMsU0FBT0csa0RBQU0sQ0FBQ0MsV0FBUCxJQUFzQkMsMkRBQWdCLENBQUNJLFVBQTlDO0FBQ0QsQ0FGTTtBQUtBLE1BQU1DLFFBQVEsR0FBRyxZQUFZO0FBQ2xDLFNBQU9QLGtEQUFNLENBQUNDLFdBQVAsSUFBc0JDLDJEQUFnQixDQUFDTSxNQUE5QztBQUNELENBRk0sQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN6QlA7QUFBQTtBQUFPLE1BQU1OLGdCQUFnQixHQUFHO0FBQzlCSSxZQUFVLEVBQUUsWUFEa0I7QUFFOUJELFNBQU8sRUFBRSxTQUZxQjtBQUc5QkYsYUFBVyxFQUFFO0FBSGlCLENBQXpCO0FBTVAsTUFBTUgsTUFBTSxHQUFHO0FBQ2JDLGFBQVcsRUFBRVEsT0FBTyxDQUFDQyxHQUFSLENBQVlDLGVBQVosSUFBK0IsYUFEL0I7QUFFYkMsTUFBSSxFQUFFO0FBQ0pDLFNBQUssRUFBRSxrQkFESDtBQUVKQyxlQUFXLEVBQUUsdUJBRlQ7QUFHSkMsUUFBSSxFQUFFO0FBSEYsR0FGTztBQU9iQyxPQUFLLEVBQUU7QUFDTEMsZ0JBQVksRUFBRTtBQURULEdBUE07QUFVYkMsWUFBVSxFQUFFLHFCQVZDO0FBV2I7QUFDQUMsdUJBQXFCLEVBQUVWLE9BQU8sQ0FBQ0MsR0FBUixDQUFZUyxxQkFBWixJQUFxQyxFQVovQztBQVlvRDtBQUNqRUMsd0JBQXNCLEVBQUVYLE9BQU8sQ0FBQ0MsR0FBUixDQUFZVSxzQkFBWixJQUFzQyxrQkFiakQ7QUFhc0U7QUFDbkZDLHVCQUFxQixFQUFFWixPQUFPLENBQUNDLEdBQVIsQ0FBWVcscUJBQVosSUFBcUMsRUFkL0M7QUFlYkMsMkJBQXlCLEVBQUViLE9BQU8sQ0FBQ0MsR0FBUixDQUFZWSx5QkFBWixJQUF5QyxFQWZ2RDtBQWdCYkMsMkJBQXlCLEVBQUVkLE9BQU8sQ0FBQ0MsR0FBUixDQUFZYSx5QkFBWixJQUF5QyxFQWhCdkQ7QUFpQmJDLHdCQUFzQixFQUFFZixPQUFPLENBQUNDLEdBQVIsQ0FBWWMsc0JBQVosSUFBc0MsRUFqQmpEO0FBa0JiQyxzQkFBb0IsRUFBRWhCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZZSxvQkFBWixJQUFvQyxFQWxCN0M7QUFtQmI7QUFDQUMsaUJBQWUsRUFBRWpCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZZ0IsZUFBWixJQUErQixFQXBCbkM7QUFxQmJDLG1CQUFpQixFQUFFLHFCQXJCTjtBQXNCYkMscUJBQW1CLEVBQUVuQixPQUFPLENBQUNDLEdBQVIsQ0FBWWtCLG1CQUFaLElBQW1DLEVBdEIzQzs7QUF1QmIsTUFBSUMsWUFBSixHQUFtQjtBQUNqQixXQUFRLG1CQUFSO0FBQ0QsR0F6Qlk7O0FBMEJiQyxhQUFXLEVBQUUsTUFBTTtBQUNqQixXQUFPOUIsTUFBTSxDQUFDcUIscUJBQVAsR0FBK0IsTUFBTXJCLE1BQU0sQ0FBQ3FCLHFCQUE1QyxHQUFvRSxFQUEzRTtBQUNELEdBNUJZO0FBNkJiVSxZQUFVLEVBQUUsTUFBTTtBQUNoQixXQUFPL0IsTUFBTSxDQUFDeUIsb0JBQVAsR0FBOEJ6QixNQUFNLENBQUN5QixvQkFBckMsR0FBNEQsRUFBbkU7QUFDRCxHQS9CWTtBQWdDYk8sTUFBSSxFQUFHQSxJQUFELElBQVU7QUFDZCxXQUFPaEMsTUFBTSxDQUFDOEIsV0FBUCxLQUF1QkUsSUFBOUI7QUFDRDtBQWxDWSxDQUFmOztBQXFDQSxXQUFrQyxFQUFsQyxNQUdPLENBQ0w7QUFDRDs7QUFFY2hDLHFFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkRBLGtDIiwiZmlsZSI6InBhZ2VzL19hcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHJlcXVpcmUoJy4uL3Nzci1tb2R1bGUtY2FjaGUuanMnKTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0dmFyIHRocmV3ID0gdHJ1ZTtcbiBcdFx0dHJ5IHtcbiBcdFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbiBcdFx0XHR0aHJldyA9IGZhbHNlO1xuIFx0XHR9IGZpbmFsbHkge1xuIFx0XHRcdGlmKHRocmV3KSBkZWxldGUgaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdH1cblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMCk7XG4iLCJpbXBvcnQgXCJhbnRkL2Rpc3QvYW50ZC5taW4uY3NzXCI7XG5pbXBvcnQgXCJzdHlsZXMvZ2xvYmFsLnNjc3NcIjtcbmltcG9ydCBcInNsaWNrLWNhcm91c2VsL3NsaWNrL3NsaWNrLmNzc1wiO1xuaW1wb3J0IFwicXVpbGwvZGlzdC9xdWlsbC5zbm93LmNzc1wiO1xuaW1wb3J0IHsgQ29uZmlnTGl2ZSB9IGZyb20gXCJwbHVnaW5zL3V0aWxzL0NvbmZpZ0xpdmVcIjtcbmltcG9ydCB7IHVzZUVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xuXG5pbXBvcnQgXCJzdHlsZXMvaGVhZGVyLnNjc3NcIjtcbmltcG9ydCBcInN0eWxlcy9oZXJvLnNjc3NcIjtcbmltcG9ydCBcInN0eWxlcy9hcnRpY2xlcy5zY3NzXCI7XG5pbXBvcnQgXCJzdHlsZXMvZm9vdGVyLnNjc3NcIjtcbmltcG9ydCBcInN0eWxlcy9oZXJvLXRleHQuc2Nzc1wiO1xuaW1wb3J0IFwic3R5bGVzL2FydGljbGUtZGV0YWlsLnNjc3NcIjtcblxuZnVuY3Rpb24gTXlBcHAoeyBDb21wb25lbnQsIHBhZ2VQcm9wcyB9KSB7XG4gIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgQ29uZmlnTGl2ZS5jb25zb2xlSGFuZGxlKCk7XG4gICAgcmV0dXJuICgpID0+IHt9O1xuICB9LCBbXSk7XG5cbiAgcmV0dXJuIDxDb21wb25lbnQgey4uLnBhZ2VQcm9wc30gLz47XG59XG5cbmV4cG9ydCBkZWZhdWx0IE15QXBwO1xuIiwiZXhwb3J0IGNvbnN0IFRCcm93c2VyID0ge1xuICAgIGRlbGV0ZUFsbENvb2tpZXM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGNvb2tpZXMgPSBkb2N1bWVudC5jb29raWUuc3BsaXQoXCI7XCIpO1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNvb2tpZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHZhciBjb29raWUgPSBjb29raWVzW2ldO1xuICAgICAgICAgICAgdmFyIGVxUG9zID0gY29va2llLmluZGV4T2YoXCI9XCIpO1xuICAgICAgICAgICAgdmFyIG5hbWUgPSBlcVBvcyA+IC0xID8gY29va2llLnN1YnN0cigwLCBlcVBvcykgOiBjb29raWU7XG4gICAgICAgICAgICBkb2N1bWVudC5jb29raWUgPSBuYW1lICsgXCI9O2V4cGlyZXM9VGh1LCAwMSBKYW4gMTk3MCAwMDowMDowMCBHTVRcIjtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBzZXRDb29raWU6IGZ1bmN0aW9uIChjbmFtZSwgY3ZhbHVlLCBleGRheXMpIHtcbiAgICAgICAgaWYgKGV4ZGF5cyA9PSB1bmRlZmluZWQpIGV4ZGF5cyA9IDk5OTtcbiAgICAgICAgdmFyIGQgPSBuZXcgRGF0ZSgpO1xuICAgICAgICBkLnNldFRpbWUoZC5nZXRUaW1lKCkgKyAoZXhkYXlzICogMjQgKiA2MCAqIDYwICogMTAwMCkpO1xuICAgICAgICB2YXIgZXhwaXJlcyA9IFwiZXhwaXJlcz1cIiArIGQudG9VVENTdHJpbmcoKTtcbiAgICAgICAgZG9jdW1lbnQuY29va2llID0gY25hbWUgKyBcIj1cIiArIGN2YWx1ZSArIFwiO1wiICsgZXhwaXJlcyArIFwiO3BhdGg9L1wiO1xuICAgIH0sXG5cbiAgICBnZXRDb29raWU6IGZ1bmN0aW9uIChjbmFtZSkge1xuICAgICAgICB2YXIgbmFtZSA9IGNuYW1lICsgXCI9XCI7XG4gICAgICAgIHZhciBjYSA9IGRvY3VtZW50LmNvb2tpZS5zcGxpdCgnOycpO1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGNhLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICB2YXIgYyA9IGNhW2ldO1xuICAgICAgICAgICAgd2hpbGUgKGMuY2hhckF0KDApID09ICcgJykge1xuICAgICAgICAgICAgICAgIGMgPSBjLnN1YnN0cmluZygxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmIChjLmluZGV4T2YobmFtZSkgPT0gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBjLnN1YnN0cmluZyhuYW1lLmxlbmd0aCwgYy5sZW5ndGgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBcIlwiO1xuICAgIH0sXG5cbiAgICBoYXNDb29raWU6IGZ1bmN0aW9uIChfbmFtZSkge1xuICAgICAgICB2YXIgX3N0ciA9IHRCcm93c2VyLmdldENvb2tpZShfbmFtZSk7XG4gICAgICAgIGlmIChfc3RyICE9IFwiXCIpIHtcbiAgICAgICAgICAgIHJldHVybiB0cnVlOyDDp1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIGlzTW9iaWxlOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9BbmRyb2lkL2kpXG4gICAgICAgICAgICB8fCBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC93ZWJPUy9pKVxuICAgICAgICAgICAgfHwgbmF2aWdhdG9yLnVzZXJBZ2VudC5tYXRjaCgvaVBob25lL2kpXG4gICAgICAgICAgICB8fCBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9pUGFkL2kpXG4gICAgICAgICAgICB8fCBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9pUG9kL2kpXG4gICAgICAgICAgICB8fCBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9CbGFja0JlcnJ5L2kpXG4gICAgICAgICAgICB8fCBuYXZpZ2F0b3IudXNlckFnZW50Lm1hdGNoKC9XaW5kb3dzIFBob25lL2kpXG4gICAgICAgICkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9LFxuXG4gICAgaXNTdXBwb3J0V2ViR0w6IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICAvLyByZXR1cm4gRGV0ZWN0b3Iud2ViZ2woKTtcbiAgICAgICAgdmFyIERldGVjdG9yID0ge1xuICAgICAgICAgICAgY2FudmFzOiAhIXdpbmRvdy5DYW52YXNSZW5kZXJpbmdDb250ZXh0MkQsXG4gICAgICAgICAgICB3ZWJnbDogKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAvLyBcbiAgICAgICAgICAgICAgICB0cnkge1xuXG4gICAgICAgICAgICAgICAgICAgIHZhciBjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKTsgcmV0dXJuICEhKHdpbmRvdy5XZWJHTFJlbmRlcmluZ0NvbnRleHQgJiYgKGNhbnZhcy5nZXRDb250ZXh0KCd3ZWJnbCcpIHx8IGNhbnZhcy5nZXRDb250ZXh0KCdleHBlcmltZW50YWwtd2ViZ2wnKSkpO1xuXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZSkge1xuXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcblxuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgfSkoKSxcbiAgICAgICAgICAgIHdvcmtlcnM6ICEhd2luZG93LldvcmtlcixcbiAgICAgICAgICAgIGZpbGVhcGk6IHdpbmRvdy5GaWxlICYmIHdpbmRvdy5GaWxlUmVhZGVyICYmIHdpbmRvdy5GaWxlTGlzdCAmJiB3aW5kb3cuQmxvYixcblxuICAgICAgICAgICAgZ2V0V2ViR0xFcnJvck1lc3NhZ2U6IGZ1bmN0aW9uICgpIHtcblxuICAgICAgICAgICAgICAgIHZhciBlbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5pZCA9ICd3ZWJnbC1lcnJvci1tZXNzYWdlJztcbiAgICAgICAgICAgICAgICBlbGVtZW50LnN0eWxlLmZvbnRGYW1pbHkgPSAnbW9ub3NwYWNlJztcbiAgICAgICAgICAgICAgICBlbGVtZW50LnN0eWxlLmZvbnRTaXplID0gJzEzcHgnO1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuc3R5bGUuZm9udFdlaWdodCA9ICdub3JtYWwnO1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuc3R5bGUudGV4dEFsaWduID0gJ2NlbnRlcic7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5zdHlsZS5iYWNrZ3JvdW5kID0gJyNmZmYnO1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuc3R5bGUuY29sb3IgPSAnIzAwMCc7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5zdHlsZS5wYWRkaW5nID0gJzEuNWVtJztcbiAgICAgICAgICAgICAgICBlbGVtZW50LnN0eWxlLndpZHRoID0gJzQwMHB4JztcbiAgICAgICAgICAgICAgICBlbGVtZW50LnN0eWxlLm1hcmdpbiA9ICc1ZW0gYXV0byAwJztcblxuICAgICAgICAgICAgICAgIGlmICghdGhpcy53ZWJnbCkge1xuXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQuaW5uZXJIVE1MID0gd2luZG93LldlYkdMUmVuZGVyaW5nQ29udGV4dCA/IFtcbiAgICAgICAgICAgICAgICAgICAgICAgICdZb3VyIGdyYXBoaWNzIGNhcmQgZG9lcyBub3Qgc2VlbSB0byBzdXBwb3J0IDxhIGhyZWY9XCJodHRwOi8va2hyb25vcy5vcmcvd2ViZ2wvd2lraS9HZXR0aW5nX2FfV2ViR0xfSW1wbGVtZW50YXRpb25cIiBzdHlsZT1cImNvbG9yOiMwMDBcIj5XZWJHTDwvYT4uPGJyIC8+JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdGaW5kIG91dCBob3cgdG8gZ2V0IGl0IDxhIGhyZWY9XCJodHRwOi8vZ2V0LndlYmdsLm9yZy9cIiBzdHlsZT1cImNvbG9yOiMwMDBcIj5oZXJlPC9hPi4nXG4gICAgICAgICAgICAgICAgICAgIF0uam9pbignXFxuJykgOiBbXG4gICAgICAgICAgICAgICAgICAgICAgICAnWW91ciBicm93c2VyIGRvZXMgbm90IHNlZW0gdG8gc3VwcG9ydCA8YSBocmVmPVwiaHR0cDovL2tocm9ub3Mub3JnL3dlYmdsL3dpa2kvR2V0dGluZ19hX1dlYkdMX0ltcGxlbWVudGF0aW9uXCIgc3R5bGU9XCJjb2xvcjojMDAwXCI+V2ViR0w8L2E+Ljxici8+JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICdGaW5kIG91dCBob3cgdG8gZ2V0IGl0IDxhIGhyZWY9XCJodHRwOi8vZ2V0LndlYmdsLm9yZy9cIiBzdHlsZT1cImNvbG9yOiMwMDBcIj5oZXJlPC9hPi4nXG4gICAgICAgICAgICAgICAgICAgIF0uam9pbignXFxuJyk7XG5cbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICByZXR1cm4gZWxlbWVudDtcblxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgYWRkR2V0V2ViR0xNZXNzYWdlOiBmdW5jdGlvbiAocGFyYW1ldGVycykge1xuXG4gICAgICAgICAgICAgICAgdmFyIHBhcmVudCwgaWQsIGVsZW1lbnQ7XG5cbiAgICAgICAgICAgICAgICBwYXJhbWV0ZXJzID0gcGFyYW1ldGVycyB8fCB7fTtcblxuICAgICAgICAgICAgICAgIHBhcmVudCA9IHBhcmFtZXRlcnMucGFyZW50ICE9PSB1bmRlZmluZWQgPyBwYXJhbWV0ZXJzLnBhcmVudCA6IGRvY3VtZW50LmJvZHk7XG4gICAgICAgICAgICAgICAgaWQgPSBwYXJhbWV0ZXJzLmlkICE9PSB1bmRlZmluZWQgPyBwYXJhbWV0ZXJzLmlkIDogJ29sZGllJztcblxuICAgICAgICAgICAgICAgIGVsZW1lbnQgPSBEZXRlY3Rvci5nZXRXZWJHTEVycm9yTWVzc2FnZSgpO1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuaWQgPSBpZDtcblxuICAgICAgICAgICAgICAgIHBhcmVudC5hcHBlbmRDaGlsZChlbGVtZW50KTtcblxuICAgICAgICAgICAgfSxcblxuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBEZXRlY3Rvci53ZWJnbDtcbiAgICB9LFxuXG5cbiAgICBnZXRVcmxQYXJhbWV0ZXJzOiBmdW5jdGlvbiAocGFyYW1ldGVyLCBzdGF0aWNVUkwsIGRlY29kZSkge1xuXG4gICAgICAgIHN0YXRpY1VSTCA9IHN0YXRpY1VSTCA9PSB1bmRlZmluZWQgPyB3aW5kb3cubG9jYXRpb24gOiBzdGF0aWNVUkw7XG4gICAgICAgIHZhciBjdXJyTG9jYXRpb24gPSAoc3RhdGljVVJMLmxlbmd0aCkgPyBzdGF0aWNVUkwgOiB3aW5kb3cubG9jYXRpb24uc2VhcmNoO1xuXG4gICAgICAgIGlmIChjdXJyTG9jYXRpb24uc3BsaXQoXCI/XCIpLmxlbmd0aCA8IDIpXG4gICAgICAgICAgICByZXR1cm4gXCJcIjtcblxuICAgICAgICB2YXIgcGFyQXJyID0gY3VyckxvY2F0aW9uLnNwbGl0KFwiP1wiKVsxXS5zcGxpdChcIiZcIiksXG4gICAgICAgICAgICByZXR1cm5Cb29sID0gdHJ1ZTtcblxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHBhckFyci5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgdmFyIHBhcnIgPSBwYXJBcnJbaV0uc3BsaXQoXCI9XCIpO1xuICAgICAgICAgICAgaWYgKHBhcnJbMF0gPT0gcGFyYW1ldGVyKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIChkZWNvZGUpID8gZGVjb2RlVVJJQ29tcG9uZW50KHBhcnJbMV0pIDogcGFyclsxXTtcbiAgICAgICAgICAgICAgICByZXR1cm5Cb29sID0gdHJ1ZTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuQm9vbCA9IGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKCFyZXR1cm5Cb29sKSByZXR1cm4gZmFsc2U7XG4gICAgfSxcblxuXG4gICAgY2hlY2tPUzogZnVuY3Rpb24gKG9wdGlvbikge1xuICAgICAgICBvcHRpb24gPSBvcHRpb24gIT0gdW5kZWZpbmVkID8gb3B0aW9uIDoge307XG5cbiAgICAgICAgdmFyIGNhbGxiYWNrID0gb3B0aW9uLmhhc093blByb3BlcnR5KFwiY2FsbGJhY2tcIikgPyBvcHRpb24uY2FsbGJhY2sgOiBudWxsO1xuXG4gICAgICAgIGlmICh0eXBlb2Ygd2luZG93LmRldmljZUluZm8gPT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICAgICAgdmFyIHVua25vd24gPSAnLSc7XG5cbiAgICAgICAgICAgIC8vIHNjcmVlblxuICAgICAgICAgICAgdmFyIHNjcmVlblNpemUgPSAnJztcbiAgICAgICAgICAgIGlmIChzY3JlZW4ud2lkdGgpIHtcbiAgICAgICAgICAgICAgICB2YXIgd2lkdGggPSAoc2NyZWVuLndpZHRoKSA/IHNjcmVlbi53aWR0aCA6ICcnO1xuICAgICAgICAgICAgICAgIHZhciBoZWlnaHQgPSAoc2NyZWVuLmhlaWdodCkgPyBzY3JlZW4uaGVpZ2h0IDogJyc7XG4gICAgICAgICAgICAgICAgc2NyZWVuU2l6ZSArPSAnJyArIHdpZHRoICsgXCIgeCBcIiArIGhlaWdodDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gYnJvd3NlclxuICAgICAgICAgICAgdmFyIG5WZXIgPSBuYXZpZ2F0b3IuYXBwVmVyc2lvbjtcbiAgICAgICAgICAgIHZhciBuQWd0ID0gbmF2aWdhdG9yLnVzZXJBZ2VudDtcbiAgICAgICAgICAgIHZhciBicm93c2VyID0gbmF2aWdhdG9yLmFwcE5hbWU7XG4gICAgICAgICAgICB2YXIgdmVyc2lvbiA9ICcnICsgcGFyc2VGbG9hdChuYXZpZ2F0b3IuYXBwVmVyc2lvbik7XG4gICAgICAgICAgICB2YXIgbWFqb3JWZXJzaW9uID0gcGFyc2VJbnQobmF2aWdhdG9yLmFwcFZlcnNpb24sIDEwKTtcbiAgICAgICAgICAgIHZhciBuYW1lT2Zmc2V0LCB2ZXJPZmZzZXQsIGl4O1xuXG4gICAgICAgICAgICAvLyBPcGVyYVxuICAgICAgICAgICAgaWYgKCh2ZXJPZmZzZXQgPSBuQWd0LmluZGV4T2YoJ09wZXJhJykpICE9IC0xKSB7XG4gICAgICAgICAgICAgICAgYnJvd3NlciA9ICdPcGVyYSc7XG4gICAgICAgICAgICAgICAgdmVyc2lvbiA9IG5BZ3Quc3Vic3RyaW5nKHZlck9mZnNldCArIDYpO1xuICAgICAgICAgICAgICAgIGlmICgodmVyT2Zmc2V0ID0gbkFndC5pbmRleE9mKCdWZXJzaW9uJykpICE9IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgIHZlcnNpb24gPSBuQWd0LnN1YnN0cmluZyh2ZXJPZmZzZXQgKyA4KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBPcGVyYSBOZXh0XG4gICAgICAgICAgICBpZiAoKHZlck9mZnNldCA9IG5BZ3QuaW5kZXhPZignT1BSJykpICE9IC0xKSB7XG4gICAgICAgICAgICAgICAgYnJvd3NlciA9ICdPcGVyYSc7XG4gICAgICAgICAgICAgICAgdmVyc2lvbiA9IG5BZ3Quc3Vic3RyaW5nKHZlck9mZnNldCArIDQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gRWRnZVxuICAgICAgICAgICAgZWxzZSBpZiAoKHZlck9mZnNldCA9IG5BZ3QuaW5kZXhPZignRWRnZScpKSAhPSAtMSkge1xuICAgICAgICAgICAgICAgIGJyb3dzZXIgPSAnTWljcm9zb2Z0IEVkZ2UnO1xuICAgICAgICAgICAgICAgIHZlcnNpb24gPSBuQWd0LnN1YnN0cmluZyh2ZXJPZmZzZXQgKyA1KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vIE1TSUVcbiAgICAgICAgICAgIGVsc2UgaWYgKCh2ZXJPZmZzZXQgPSBuQWd0LmluZGV4T2YoJ01TSUUnKSkgIT0gLTEpIHtcbiAgICAgICAgICAgICAgICBicm93c2VyID0gJ01pY3Jvc29mdCBJbnRlcm5ldCBFeHBsb3Jlcic7XG4gICAgICAgICAgICAgICAgdmVyc2lvbiA9IG5BZ3Quc3Vic3RyaW5nKHZlck9mZnNldCArIDUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gQ2hyb21lXG4gICAgICAgICAgICBlbHNlIGlmICgodmVyT2Zmc2V0ID0gbkFndC5pbmRleE9mKCdDaHJvbWUnKSkgIT0gLTEpIHtcbiAgICAgICAgICAgICAgICBicm93c2VyID0gJ0Nocm9tZSc7XG4gICAgICAgICAgICAgICAgdmVyc2lvbiA9IG5BZ3Quc3Vic3RyaW5nKHZlck9mZnNldCArIDcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gU2FmYXJpXG4gICAgICAgICAgICBlbHNlIGlmICgodmVyT2Zmc2V0ID0gbkFndC5pbmRleE9mKCdTYWZhcmknKSkgIT0gLTEpIHtcbiAgICAgICAgICAgICAgICBicm93c2VyID0gJ1NhZmFyaSc7XG4gICAgICAgICAgICAgICAgdmVyc2lvbiA9IG5BZ3Quc3Vic3RyaW5nKHZlck9mZnNldCArIDcpO1xuICAgICAgICAgICAgICAgIGlmICgodmVyT2Zmc2V0ID0gbkFndC5pbmRleE9mKCdWZXJzaW9uJykpICE9IC0xKSB7XG4gICAgICAgICAgICAgICAgICAgIHZlcnNpb24gPSBuQWd0LnN1YnN0cmluZyh2ZXJPZmZzZXQgKyA4KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBGaXJlZm94XG4gICAgICAgICAgICBlbHNlIGlmICgodmVyT2Zmc2V0ID0gbkFndC5pbmRleE9mKCdGaXJlZm94JykpICE9IC0xKSB7XG4gICAgICAgICAgICAgICAgYnJvd3NlciA9ICdGaXJlZm94JztcbiAgICAgICAgICAgICAgICB2ZXJzaW9uID0gbkFndC5zdWJzdHJpbmcodmVyT2Zmc2V0ICsgOCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBNU0lFIDExK1xuICAgICAgICAgICAgZWxzZSBpZiAobkFndC5pbmRleE9mKCdUcmlkZW50LycpICE9IC0xKSB7XG4gICAgICAgICAgICAgICAgYnJvd3NlciA9ICdNaWNyb3NvZnQgSW50ZXJuZXQgRXhwbG9yZXInO1xuICAgICAgICAgICAgICAgIHZlcnNpb24gPSBuQWd0LnN1YnN0cmluZyhuQWd0LmluZGV4T2YoJ3J2OicpICsgMyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBPdGhlciBicm93c2Vyc1xuICAgICAgICAgICAgZWxzZSBpZiAoKG5hbWVPZmZzZXQgPSBuQWd0Lmxhc3RJbmRleE9mKCcgJykgKyAxKSA8ICh2ZXJPZmZzZXQgPSBuQWd0Lmxhc3RJbmRleE9mKCcvJykpKSB7XG4gICAgICAgICAgICAgICAgYnJvd3NlciA9IG5BZ3Quc3Vic3RyaW5nKG5hbWVPZmZzZXQsIHZlck9mZnNldCk7XG4gICAgICAgICAgICAgICAgdmVyc2lvbiA9IG5BZ3Quc3Vic3RyaW5nKHZlck9mZnNldCArIDEpO1xuICAgICAgICAgICAgICAgIGlmIChicm93c2VyLnRvTG93ZXJDYXNlKCkgPT0gYnJvd3Nlci50b1VwcGVyQ2FzZSgpKSB7XG4gICAgICAgICAgICAgICAgICAgIGJyb3dzZXIgPSBuYXZpZ2F0b3IuYXBwTmFtZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyB0cmltIHRoZSB2ZXJzaW9uIHN0cmluZ1xuICAgICAgICAgICAgaWYgKChpeCA9IHZlcnNpb24uaW5kZXhPZignOycpKSAhPSAtMSkgdmVyc2lvbiA9IHZlcnNpb24uc3Vic3RyaW5nKDAsIGl4KTtcbiAgICAgICAgICAgIGlmICgoaXggPSB2ZXJzaW9uLmluZGV4T2YoJyAnKSkgIT0gLTEpIHZlcnNpb24gPSB2ZXJzaW9uLnN1YnN0cmluZygwLCBpeCk7XG4gICAgICAgICAgICBpZiAoKGl4ID0gdmVyc2lvbi5pbmRleE9mKCcpJykpICE9IC0xKSB2ZXJzaW9uID0gdmVyc2lvbi5zdWJzdHJpbmcoMCwgaXgpO1xuXG4gICAgICAgICAgICBtYWpvclZlcnNpb24gPSBwYXJzZUludCgnJyArIHZlcnNpb24sIDEwKTtcbiAgICAgICAgICAgIGlmIChpc05hTihtYWpvclZlcnNpb24pKSB7XG4gICAgICAgICAgICAgICAgdmVyc2lvbiA9ICcnICsgcGFyc2VGbG9hdChuYXZpZ2F0b3IuYXBwVmVyc2lvbik7XG4gICAgICAgICAgICAgICAgbWFqb3JWZXJzaW9uID0gcGFyc2VJbnQobmF2aWdhdG9yLmFwcFZlcnNpb24sIDEwKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgLy8gbW9iaWxlIHZlcnNpb25cbiAgICAgICAgICAgIHZhciBtb2JpbGUgPSAvTW9iaWxlfG1pbml8RmVubmVjfEFuZHJvaWR8aVAoYWR8b2R8aG9uZSkvLnRlc3QoblZlcik7XG5cbiAgICAgICAgICAgIC8vIGNvb2tpZVxuICAgICAgICAgICAgdmFyIGNvb2tpZUVuYWJsZWQgPSAobmF2aWdhdG9yLmNvb2tpZUVuYWJsZWQpID8gdHJ1ZSA6IGZhbHNlO1xuXG4gICAgICAgICAgICBpZiAodHlwZW9mIG5hdmlnYXRvci5jb29raWVFbmFibGVkID09ICd1bmRlZmluZWQnICYmICFjb29raWVFbmFibGVkKSB7XG4gICAgICAgICAgICAgICAgZG9jdW1lbnQuY29va2llID0gJ3Rlc3Rjb29raWUnO1xuICAgICAgICAgICAgICAgIGNvb2tpZUVuYWJsZWQgPSAoZG9jdW1lbnQuY29va2llLmluZGV4T2YoJ3Rlc3Rjb29raWUnKSAhPSAtMSkgPyB0cnVlIDogZmFsc2U7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIHN5c3RlbVxuICAgICAgICAgICAgdmFyIG9zID0gdW5rbm93bjtcbiAgICAgICAgICAgIHZhciBjbGllbnRTdHJpbmdzID0gW1xuICAgICAgICAgICAgICAgIHsgczogJ1dpbmRvd3MgMTAnLCByOiAvKFdpbmRvd3MgMTAuMHxXaW5kb3dzIE5UIDEwLjApLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ1dpbmRvd3MgOC4xJywgcjogLyhXaW5kb3dzIDguMXxXaW5kb3dzIE5UIDYuMykvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnV2luZG93cyA4JywgcjogLyhXaW5kb3dzIDh8V2luZG93cyBOVCA2LjIpLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ1dpbmRvd3MgNycsIHI6IC8oV2luZG93cyA3fFdpbmRvd3MgTlQgNi4xKS8gfSxcbiAgICAgICAgICAgICAgICB7IHM6ICdXaW5kb3dzIFZpc3RhJywgcjogL1dpbmRvd3MgTlQgNi4wLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ1dpbmRvd3MgU2VydmVyIDIwMDMnLCByOiAvV2luZG93cyBOVCA1LjIvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnV2luZG93cyBYUCcsIHI6IC8oV2luZG93cyBOVCA1LjF8V2luZG93cyBYUCkvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnV2luZG93cyAyMDAwJywgcjogLyhXaW5kb3dzIE5UIDUuMHxXaW5kb3dzIDIwMDApLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ1dpbmRvd3MgTUUnLCByOiAvKFdpbiA5eCA0LjkwfFdpbmRvd3MgTUUpLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ1dpbmRvd3MgOTgnLCByOiAvKFdpbmRvd3MgOTh8V2luOTgpLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ1dpbmRvd3MgOTUnLCByOiAvKFdpbmRvd3MgOTV8V2luOTV8V2luZG93c185NSkvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnV2luZG93cyBOVCA0LjAnLCByOiAvKFdpbmRvd3MgTlQgNC4wfFdpbk5UNC4wfFdpbk5UfFdpbmRvd3MgTlQpLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ1dpbmRvd3MgQ0UnLCByOiAvV2luZG93cyBDRS8gfSxcbiAgICAgICAgICAgICAgICB7IHM6ICdXaW5kb3dzIDMuMTEnLCByOiAvV2luMTYvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnQW5kcm9pZCcsIHI6IC9BbmRyb2lkLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ09wZW4gQlNEJywgcjogL09wZW5CU0QvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnU3VuIE9TJywgcjogL1N1bk9TLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ0xpbnV4JywgcjogLyhMaW51eHxYMTEpLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ2lPUycsIHI6IC8oaVBob25lfGlQYWR8aVBvZCkvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnTWFjIE9TIFgnLCByOiAvTWFjIE9TIFgvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnTWFjIE9TJywgcjogLyhNYWNQUEN8TWFjSW50ZWx8TWFjX1Bvd2VyUEN8TWFjaW50b3NoKS8gfSxcbiAgICAgICAgICAgICAgICB7IHM6ICdRTlgnLCByOiAvUU5YLyB9LFxuICAgICAgICAgICAgICAgIHsgczogJ1VOSVgnLCByOiAvVU5JWC8gfSxcbiAgICAgICAgICAgICAgICB7IHM6ICdCZU9TJywgcjogL0JlT1MvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnT1MvMicsIHI6IC9PU1xcLzIvIH0sXG4gICAgICAgICAgICAgICAgeyBzOiAnU2VhcmNoIEJvdCcsIHI6IC8obnVoa3xHb29nbGVib3R8WWFtbXlib3R8T3BlbmJvdHxTbHVycHxNU05Cb3R8QXNrIEplZXZlc1xcL1Rlb21hfGlhX2FyY2hpdmVyKS8gfVxuICAgICAgICAgICAgXTtcbiAgICAgICAgICAgIGZvciAodmFyIGlkIGluIGNsaWVudFN0cmluZ3MpIHtcbiAgICAgICAgICAgICAgICB2YXIgY3MgPSBjbGllbnRTdHJpbmdzW2lkXTtcbiAgICAgICAgICAgICAgICBpZiAoY3Muci50ZXN0KG5BZ3QpKSB7XG4gICAgICAgICAgICAgICAgICAgIG9zID0gY3MucztcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB2YXIgb3NWZXJzaW9uID0gdW5rbm93bjtcblxuICAgICAgICAgICAgaWYgKC9XaW5kb3dzLy50ZXN0KG9zKSkge1xuICAgICAgICAgICAgICAgIG9zVmVyc2lvbiA9IC9XaW5kb3dzICguKikvLmV4ZWMob3MpWzFdO1xuICAgICAgICAgICAgICAgIG9zID0gJ1dpbmRvd3MnO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBzd2l0Y2ggKG9zKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAnTWFjIE9TIFgnOlxuICAgICAgICAgICAgICAgICAgICBvc1ZlcnNpb24gPSAvTWFjIE9TIFggKDEwW1xcLlxcX1xcZF0rKS8uZXhlYyhuQWd0KVsxXTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgICAgICBjYXNlICdBbmRyb2lkJzpcbiAgICAgICAgICAgICAgICAgICAgb3NWZXJzaW9uID0gL0FuZHJvaWQgKFtcXC5cXF9cXGRdKykvLmV4ZWMobkFndClbMV07XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICAgICAgY2FzZSAnaU9TJzpcbiAgICAgICAgICAgICAgICAgICAgb3NWZXJzaW9uID0gL09TIChcXGQrKV8oXFxkKylfPyhcXGQrKT8vLmV4ZWMoblZlcik7XG4gICAgICAgICAgICAgICAgICAgIG9zVmVyc2lvbiA9IG9zVmVyc2lvblsxXSArICcuJyArIG9zVmVyc2lvblsyXSArICcuJyArIChvc1ZlcnNpb25bM10gfCAwKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIGZsYXNoICh5b3UnbGwgbmVlZCB0byBpbmNsdWRlIHN3Zm9iamVjdClcbiAgICAgICAgICAgIC8qIHNjcmlwdCBzcmM9XCIvL2FqYXguZ29vZ2xlYXBpcy5jb20vYWpheC9saWJzL3N3Zm9iamVjdC8yLjIvc3dmb2JqZWN0LmpzXCIgKi9cbiAgICAgICAgICAgIHZhciBmbGFzaFZlcnNpb24gPSAnbm8gY2hlY2snO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBzd2ZvYmplY3QgIT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICB2YXIgZnYgPSBzd2ZvYmplY3QuZ2V0Rmxhc2hQbGF5ZXJWZXJzaW9uKCk7XG4gICAgICAgICAgICAgICAgaWYgKGZ2Lm1ham9yID4gMCkge1xuICAgICAgICAgICAgICAgICAgICBmbGFzaFZlcnNpb24gPSBmdi5tYWpvciArICcuJyArIGZ2Lm1pbm9yICsgJyByJyArIGZ2LnJlbGVhc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBmbGFzaFZlcnNpb24gPSB1bmtub3duO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuXG4gICAgICAgICAgICB3aW5kb3cuZGV2aWNlSW5mbyA9IHtcbiAgICAgICAgICAgICAgICBzY3JlZW46IHNjcmVlblNpemUsXG4gICAgICAgICAgICAgICAgYnJvd3NlcjogYnJvd3NlcixcbiAgICAgICAgICAgICAgICBicm93c2VyVmVyc2lvbjogdmVyc2lvbixcbiAgICAgICAgICAgICAgICBicm93c2VyTWFqb3JWZXJzaW9uOiBtYWpvclZlcnNpb24sXG4gICAgICAgICAgICAgICAgbW9iaWxlOiBtb2JpbGUsXG4gICAgICAgICAgICAgICAgb3M6IG9zLFxuICAgICAgICAgICAgICAgIG9zVmVyc2lvbjogb3NWZXJzaW9uLFxuICAgICAgICAgICAgICAgIGNvb2tpZXM6IGNvb2tpZUVuYWJsZWQsXG4gICAgICAgICAgICAgICAgZmxhc2hWZXJzaW9uOiBmbGFzaFZlcnNpb25cbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoY2FsbGJhY2sgIT0gbnVsbCkgY2FsbGJhY2sod2luZG93LmRldmljZUluZm8pO1xuICAgIH0sXG5cbiAgICBTQUZBUkk6IFwic2FmYXJpXCIsXG4gICAgQ0hST01FOiBcImNocm9tZVwiLFxuICAgIEZJUkVGT1g6IFwiZmlyZWZveFwiLFxuICAgIE9QRVJBOiBcIm9wZXJhXCIsXG4gICAgV0VCVklFVzogXCJ3ZWJ2aWV3XCIsXG5cblxuICAgIGlzUG9ydGFpbjogZnVuY3Rpb24gKCkge1xuICAgICAgICBjb25zb2xlLmxvZygnaXNQb3J0YWluJylcbiAgICB9LFxuXG4gICAgcmVzdG9yZUNvbnNvbGU6IGZ1bmN0aW9uIChwYXJhbXMpIHtcbiAgICAgICAgLy88aWZyYW1lPiBlbGVtZW50XG4gICAgICAgIHZhciBpZnJhbWUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaWZyYW1lXCIpO1xuICAgICAgICAvL0hpZGUgaXQgc29tZXdoZXJlXG4gICAgICAgIGlmcmFtZS5zdHlsZS5wb3NpdGlvbiA9IFwiZml4ZWRcIjtcbiAgICAgICAgaWZyYW1lLnN0eWxlLmhlaWdodCA9IGlmcmFtZS5zdHlsZS53aWR0aCA9IFwiMXB4XCI7XG4gICAgICAgIGlmcmFtZS5zdHlsZS50b3AgPSBpZnJhbWUuc3R5bGUubGVmdCA9IFwiLTVweFwiO1xuICAgICAgICBpZnJhbWUuc3R5bGUuZGlzcGxheSA9IFwibm9uZVwiO1xuICAgICAgICAvL05vIHNyYyB0byBwcmV2ZW50IGxvYWRpbmcgc29tZSBkYXRhXG4gICAgICAgIGlmcmFtZS5zcmMgPSBcImFib3V0OiBibGFua1wiO1xuICAgICAgICAvL05lZWRzIGFwcGVuZCB0byB3b3JrXG4gICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoaWZyYW1lKTtcbiAgICAgICAgLy9HZXQgdGhlIGlubmVyIGNvbnNvbGVcbiAgICAgICAgd2luZG93LmNvbnNvbGUgPSB7fTtcbiAgICAgICAgd2luZG93LmNvbnNvbGUgPSBpZnJhbWUuY29udGVudFdpbmRvdy5jb25zb2xlO1xuXG4gICAgICAgIC8vIGRlbGV0ZSBpZnJhbWU7XG4gICAgfSxcblxuICAgIHJlbW92ZUNvbnNvbGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgLy8gY29uc29sZS5jbGVhcigpXG4gICAgICAgIGZvciAodmFyIGkgaW4gY29uc29sZSkge1xuICAgICAgICAgICAgY29uc29sZVtpXSA9IGZ1bmN0aW9uICgpIHsgfTtcbiAgICAgICAgfVxuICAgIH0sXG5cbiAgICBzaG93Q3JlZGl0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiRGV2ZWxvcGVkIGJ5IERpZ2l0b3AgfCBodHRwczovL3dlYXJldG9wZ3JvdXAuY29tL1wiKTtcbiAgICB9XG5cbn1cblxuZXhwb3J0IGRlZmF1bHQgVEJyb3dzZXI7XG4iLCJpbXBvcnQgVEJyb3dzZXIgZnJvbSBcInBsdWdpbnMvdGVleGlpL1RCcm93c2VyXCI7XG5pbXBvcnQgQ09ORklHLCB7IEVOVklST05NRU5UX0RBVEEgfSBmcm9tIFwid2ViLmNvbmZpZ1wiO1xuXG5leHBvcnQgY29uc3QgQ29uZmlnTGl2ZSA9IHtcbiAgY29uc29sZUhhbmRsZTogKHNob3dDcmVkaXQgPSB0cnVlKSA9PiB7XG4gICAgaWYgKElzUHJvZCgpKSB7XG4gICAgICBjb25zb2xlLmNsZWFyKCk7XG4gICAgICBpZiAoc2hvd0NyZWRpdCkgVEJyb3dzZXIuc2hvd0NyZWRpdCgpO1xuICAgICAgVEJyb3dzZXIucmVtb3ZlQ29uc29sZSgpO1xuICAgIH1cbiAgfSxcbn07XG5cbmV4cG9ydCBjb25zdCBJc0RldiA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIENPTkZJRy5lbnZpcm9ubWVudCA9PSBFTlZJUk9OTUVOVF9EQVRBLkRFVkVMT1BNRU5UO1xufTtcblxuZXhwb3J0IGNvbnN0IElzU3RhZyA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIENPTkZJRy5lbnZpcm9ubWVudCA9PSBFTlZJUk9OTUVOVF9EQVRBLlNUQUdJTkc7XG59O1xuXG5leHBvcnQgY29uc3QgSXNQcm9kID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gQ09ORklHLmVudmlyb25tZW50ID09IEVOVklST05NRU5UX0RBVEEuUFJPRFVDVElPTjtcbn07XG5cblxuZXhwb3J0IGNvbnN0IElzQ2FuYXJ5ID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gQ09ORklHLmVudmlyb25tZW50ID09IEVOVklST05NRU5UX0RBVEEuQ0FOQVJZO1xufTtcblxuIiwiXG5leHBvcnQgY29uc3QgRU5WSVJPTk1FTlRfREFUQSA9IHtcbiAgUFJPRFVDVElPTjogXCJwcm9kdWN0aW9uXCIsXG4gIFNUQUdJTkc6IFwic3RhZ2luZ1wiLFxuICBERVZFTE9QTUVOVDogXCJkZXZlbG9wbWVudFwiLFxufVxuXG5jb25zdCBDT05GSUcgPSB7XG4gIGVudmlyb25tZW50OiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19FTlYgfHwgXCJkZXZlbG9wbWVudFwiLFxuICBzaXRlOiB7XG4gICAgdGl0bGU6IFwiRGlnaW5leHQgV2Vic2l0ZVwiLFxuICAgIGRlc2NyaXB0aW9uOiBcIkRlc2NyaXB0aW9uIGdvZXMgaGVyZVwiLFxuICAgIHR5cGU6IFwiYXJ0aWNsZVwiLFxuICB9LFxuICBsaW5rczoge1xuICAgIGZhY2Vib29rUGFnZTogXCJcIixcbiAgfSxcbiAgZGF0ZUZvcm1hdDogXCJ5eXl5LU1NLWRkIEhIOm1tOnNzXCIsXG4gIC8vIHRoZXNlIHZhcmlhYmxlcyBjYW4gYmUgZXhwb3NlZCB0byBmcm9udC1lbmQ6XG4gIE5FWFRfUFVCTElDX0ZCX0FQUF9JRDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfRkJfQVBQX0lEIHx8IFwiXCIsICAvLyBjdXJyZW50bHkgdXNpbmcgWFhYXG4gIE5FWFRfUFVCTElDX0ZCX1BBR0VfSUQ6IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0ZCX1BBR0VfSUQgfHwgXCIxMTYyMjE0MDQwNTMyOTAyXCIsICAvLyBjdXJyZW50bHkgdXNpbmcgRGlnaXRvcCBkZXZlbG9wZXJzXG4gIE5FWFRfUFVCTElDX0JBU0VfUEFUSDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfQkFTRV9QQVRIIHx8IFwiXCIsXG4gIE5FWFRfUFVCTElDX0FQSV9CQVNFX1BBVEg6IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0FQSV9CQVNFX1BBVEggfHwgXCJcIixcbiAgTkVYVF9QVUJMSUNfQ0ROX0JBU0VfUEFUSDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfQ0ROX0JBU0VfUEFUSCB8fCBcIlwiLFxuICBORVhUX1BVQkxJQ19BUFBfRE9NQUlOOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19BUFBfRE9NQUlOIHx8IFwiXCIsXG4gIE5FWFRfUFVCTElDX0JBU0VfVVJMOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19CQVNFX1VSTCB8fCBcIlwiLFxuICAvLyBzb21lIHNlY3JldCBrZXlzIHdoaWNoIHdvbid0IGJlIGV4cG9zZWQgdG8gZnJvbnQtZW5kOlxuICBTT01FX1NFQ1JFVF9LRVk6IHByb2Nlc3MuZW52LlNPTUVfU0VDUkVUX0tFWSB8fCBcIlwiLFxuICBJUk9OX1NFU1NJT05fTkFNRTogXCJESUdJTkVYVEFETUlOQ09PS0lFXCIsXG4gIElST05fU0VTU0lPTl9TRUNSRVQ6IHByb2Nlc3MuZW52LklST05fU0VTU0lPTl9TRUNSRVQgfHwgXCJcIixcbiAgZ2V0IFNFU1NJT05fTkFNRSgpIHtcbiAgICByZXR1cm4gYERJR0lORVhUQVBQQ09PS0lFYDtcbiAgfSxcbiAgZ2V0QmFzZVBhdGg6ICgpID0+IHtcbiAgICByZXR1cm4gQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfUEFUSCA/IFwiL1wiICsgQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfUEFUSCA6IFwiXCI7XG4gIH0sXG4gIGdldEJhc2VVcmw6ICgpID0+IHtcbiAgICByZXR1cm4gQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfVVJMID8gQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfVVJMIDogXCJcIjtcbiAgfSxcbiAgcGF0aDogKHBhdGgpID0+IHtcbiAgICByZXR1cm4gQ09ORklHLmdldEJhc2VQYXRoKCkgKyBwYXRoO1xuICB9LFxufTtcblxuaWYgKHR5cGVvZiB3aW5kb3cgIT0gXCJ1bmRlZmluZWRcIikge1xuICB3aW5kb3cuX19jb25maWdfXyA9IENPTkZJRztcbiAgLy8gY29uc29sZS5sb2coQ09ORklHKTtcbn0gZWxzZSB7XG4gIC8vIGNvbnNvbGUubG9nKENPTkZJRyk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IENPTkZJRztcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyJdLCJzb3VyY2VSb290IjoiIn0=