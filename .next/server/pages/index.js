module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/website/elements/Footer.js":
/*!***********************************************!*\
  !*** ./components/website/elements/Footer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Footer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! plugins/assets/asset */ "./plugins/assets/asset.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function Footer() {
  return __jsx("footer", {
    className: "site-footer"
  }, __jsx("div", {
    className: "slide"
  }, __jsx("span", null, "Digital product design"), __jsx("span", null, "Remote work"), __jsx("span", null, "UX design"), __jsx("span", null, "Distributed teams"), __jsx("span", null, "Creativity"), __jsx("span", null, "Strategy"), __jsx("span", null, "Suspense"), __jsx("span", null, "Growth")), __jsx("div", {
    className: "container"
  }, __jsx("div", {
    className: "content"
  }, __jsx("a", {
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/logo_light.svg")
  })), __jsx("p", null, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu velit tempus erat egestas efficitur. In hac habitasse platea dictumst. Fusce a nunc eget ligula suscipit finibus."), __jsx("ul", {
    class: "address"
  }, __jsx("li", null, __jsx("a", {
    href: "https://twitter.com/mikamatikainen",
    target: "_blank",
    rel: "noopener"
  }, "Twitter")), __jsx("li", null, __jsx("a", {
    href: "https://www.linkedin.com/in/mikamatikainen",
    target: "_blank",
    rel: "noopener"
  }, "LinkedIn")), __jsx("li", null, __jsx("a", {
    href: "https://www.nordicrose.net/blog/rss/",
    target: "_blank",
    rel: "noopener"
  }, "RSS"))), __jsx("p", {
    class: "site-footer__bottom-text"
  }, "\xA9 2012-2021 Nordic Rose Co.", __jsx("br", null), "All rights reserved."))));
}

/***/ }),

/***/ "./components/website/header/Header.js":
/*!*********************************************!*\
  !*** ./components/website/header/Header.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Header; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! plugins/assets/asset */ "./plugins/assets/asset.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function Header() {
  return __jsx("div", {
    className: "header"
  }, __jsx("div", {
    className: "container-fluid"
  }, __jsx("div", {
    className: "logo"
  }, __jsx("a", {
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/logo_dark.svg")
  }))), __jsx("ul", {
    className: "nav"
  }, __jsx("li", null, __jsx("a", {
    href: "/"
  }, "BLOG")), __jsx("li", null, __jsx("a", {
    href: "/about"
  }, "ABOUT")), __jsx("li", null, __jsx("a", {
    href: "/links"
  }, "LINKS")), __jsx("li", null, __jsx("a", {
    href: "/projects"
  }, "PROJECTS"))), __jsx("div", {
    class: "btnmenu"
  }, __jsx("span", null)), __jsx("nav", {
    className: "menu-mobile"
  }, __jsx("ul", null, __jsx("li", null, __jsx("a", {
    href: "/"
  }, "BLOG")), __jsx("li", null, __jsx("a", {
    href: "/about"
  }, "ABOUT")), __jsx("li", null, __jsx("a", {
    href: "/links"
  }, "LINKS")), __jsx("li", null, __jsx("a", {
    href: "/projects"
  }, "PROJECTS"))))));
}

/***/ }),

/***/ "./components/website/section/articles/Articles.js":
/*!*********************************************************!*\
  !*** ./components/website/section/articles/Articles.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Articles; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! plugins/assets/asset */ "./plugins/assets/asset.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function Articles() {
  return __jsx("div", {
    className: "articles"
  }, __jsx("div", {
    className: "container"
  }, __jsx("div", {
    className: "articles__title"
  }, __jsx("h3", null, "All articles")), __jsx("div", {
    className: "articles__wrap"
  }, __jsx("a", {
    className: "item",
    href: "/articles-detail"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 12-1.png")
  }), __jsx("p", null, "Here are some things you should know regarding how we work ")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 13-1.png")
  }), __jsx("p", null, "Granny gives everyone the finger, and other tips from OFFF Barcelona")), __jsx("a", {
    className: "item",
    href: "/articles-detail.js"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 13.png")
  }), __jsx("p", null, "Hello world, or, in other words, why this blog exists ")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 12.png")
  }), __jsx("p", null, "Here are some things you should know regarding how we work ")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 14.png")
  }), __jsx("p", null, "Connecting artificial intelligence with digital product design")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 15.png")
  }), __jsx("p", null, "It\u2019s all about finding the perfect balance")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 12-2.png")
  }), __jsx("p", null, "I believe learning is the most important skill ")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 13-2.png")
  }), __jsx("p", null, "Clients are part of the team")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 19.png")
  }), __jsx("p", null, "Clients are part of the team")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 17.png")
  }), __jsx("p", null, "Here are some things you should know regarding how we work ")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 16.png")
  }), __jsx("p", null, "Connecting artificial intelligence with digital product design", " ")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 18.png")
  }), __jsx("p", null, "How modern remote working tools get along with Old School Cowboy's methods", " ")))));
}

/***/ }),

/***/ "./components/website/section/hero/Hero.js":
/*!*************************************************!*\
  !*** ./components/website/section/hero/Hero.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Hero; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! plugins/assets/asset */ "./plugins/assets/asset.js");
/* harmony import */ var _Text__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Text */ "./components/website/section/hero/Text.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


function Hero() {
  return __jsx("div", {
    className: "hero"
  }, __jsx("div", {
    className: "container"
  }, __jsx("div", {
    className: "img"
  }, __jsx("a", {
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/image-section1.jpg")
  }))), __jsx(_Text__WEBPACK_IMPORTED_MODULE_2__["default"], null), __jsx("hr", null)));
}

/***/ }),

/***/ "./components/website/section/hero/Text.js":
/*!*************************************************!*\
  !*** ./components/website/section/hero/Text.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Text; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
function Text() {
  return __jsx("div", {
    className: "text"
  }, __jsx("h1", null, "A few words about this blog platform, Ghost, and how this site was made"), __jsx("p", null, "Why Ghost (& Figma) instead of Medium, WordPress or other options?"));
}

/***/ }),

/***/ "./diginext.json":
/*!***********************!*\
  !*** ./diginext.json ***!
  \***********************/
/*! exports provided: git, projectSlug, projectName, username, version, diginext-cli, domain, cdn-host, cdn, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"git\":{\"creator\":\"digitop-duynguyen\",\"repoURL\":\"https://bitbucket.org/digitopvn/diginext/src\"},\"projectSlug\":\"diginext\",\"projectName\":\"Diginext Framework\",\"username\":\"digitop-duynguyen\",\"version\":\"v1.3.0\",\"diginext-cli\":\"0.4.1\",\"domain\":{\"staging\":[],\"prod\":[\"diginext.com.vn\"]},\"cdn-host\":{\"dev\":\"https://storage.googleapis.com/digitop-cdn-sg/diginext/dev\",\"dev-nocache\":\"https://storage.googleapis.com/digitop-cdn-sg/diginext/dev\",\"prod\":\"https://storage.googleapis.com/digitop-cdn-sg/diginext/prod\",\"prod-nocache\":\"https://storage.googleapis.com/digitop-cdn-sg/diginext/prod\"},\"cdn\":{\"dev\":false,\"staging\":false,\"prod\":false}}");

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Home; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_website_header_Header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! components/website/header/Header */ "./components/website/header/Header.js");
/* harmony import */ var components_website_section_hero_Hero__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! components/website/section/hero/Hero */ "./components/website/section/hero/Hero.js");
/* harmony import */ var components_website_section_articles_Articles__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! components/website/section/articles/Articles */ "./components/website/section/articles/Articles.js");
/* harmony import */ var components_website_elements_Footer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! components/website/elements/Footer */ "./components/website/elements/Footer.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
// import CONFIG from "web.config";
//import MasterPageExample from "components/website/master/MasterPageExample";
// import BasicLayout from "components/diginext/layout/BasicLayout";
// import { useRouter } from "next/router";
// import Header from "components/website/elements/Header";
//import DashkitButton from "components/dashkit/Buttons";
//import { BS } from "components/diginext/elements/Splitters";




function Home(props) {
  return __jsx(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, __jsx(components_website_header_Header__WEBPACK_IMPORTED_MODULE_1__["default"], null), __jsx(components_website_section_hero_Hero__WEBPACK_IMPORTED_MODULE_2__["default"], null), __jsx(components_website_section_articles_Articles__WEBPACK_IMPORTED_MODULE_3__["default"], null), __jsx(components_website_elements_Footer__WEBPACK_IMPORTED_MODULE_4__["default"], null));
}

/***/ }),

/***/ "./plugins/assets/asset.js":
/*!*********************************!*\
  !*** ./plugins/assets/asset.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var web_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! web.config */ "./web.config.js");
/* harmony import */ var diginext_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! diginext.json */ "./diginext.json");
var diginext_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! diginext.json */ "./diginext.json", 1);



const asset = src => {
  // console.log(CONFIG.NEXT_PUBLIC_CDN_BASE_PATH);
  // console.log(framework);
  let isEnabledCDN = false;

  if (web_config__WEBPACK_IMPORTED_MODULE_0__["default"].environment == "production") {
    isEnabledCDN = diginext_json__WEBPACK_IMPORTED_MODULE_1__.cdn.prod;
  } else if (web_config__WEBPACK_IMPORTED_MODULE_0__["default"].environment == "staging") {
    isEnabledCDN = diginext_json__WEBPACK_IMPORTED_MODULE_1__.cdn.staging;
  } else if (web_config__WEBPACK_IMPORTED_MODULE_0__["default"].environment == "development") {
    isEnabledCDN = diginext_json__WEBPACK_IMPORTED_MODULE_1__.cdn.dev;
  } else {
    isEnabledCDN = false;
  }

  let isEnableBasePath = false;

  if (isEnabledCDN == false && web_config__WEBPACK_IMPORTED_MODULE_0__["default"].NEXT_PUBLIC_BASE_PATH) {
    isEnableBasePath = true;
  }

  if (isEnabledCDN) {
    return web_config__WEBPACK_IMPORTED_MODULE_0__["default"].NEXT_PUBLIC_CDN_BASE_PATH + "/public" + src;
  } else {
    if (isEnableBasePath) {
      return "/" + web_config__WEBPACK_IMPORTED_MODULE_0__["default"].NEXT_PUBLIC_BASE_PATH + src;
    } else {
      return src;
    }
  }
};

/* harmony default export */ __webpack_exports__["default"] = (asset);

/***/ }),

/***/ "./web.config.js":
/*!***********************!*\
  !*** ./web.config.js ***!
  \***********************/
/*! exports provided: ENVIRONMENT_DATA, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENVIRONMENT_DATA", function() { return ENVIRONMENT_DATA; });
const ENVIRONMENT_DATA = {
  PRODUCTION: "production",
  STAGING: "staging",
  DEVELOPMENT: "development"
};
const CONFIG = {
  environment: process.env.NEXT_PUBLIC_ENV || "development",
  site: {
    title: "Diginext Website",
    description: "Description goes here",
    type: "article"
  },
  links: {
    facebookPage: ""
  },
  dateFormat: "yyyy-MM-dd HH:mm:ss",
  // these variables can be exposed to front-end:
  NEXT_PUBLIC_FB_APP_ID: process.env.NEXT_PUBLIC_FB_APP_ID || "",
  // currently using XXX
  NEXT_PUBLIC_FB_PAGE_ID: process.env.NEXT_PUBLIC_FB_PAGE_ID || "1162214040532902",
  // currently using Digitop developers
  NEXT_PUBLIC_BASE_PATH: process.env.NEXT_PUBLIC_BASE_PATH || "",
  NEXT_PUBLIC_API_BASE_PATH: process.env.NEXT_PUBLIC_API_BASE_PATH || "",
  NEXT_PUBLIC_CDN_BASE_PATH: process.env.NEXT_PUBLIC_CDN_BASE_PATH || "",
  NEXT_PUBLIC_APP_DOMAIN: process.env.NEXT_PUBLIC_APP_DOMAIN || "",
  NEXT_PUBLIC_BASE_URL: process.env.NEXT_PUBLIC_BASE_URL || "",
  // some secret keys which won't be exposed to front-end:
  SOME_SECRET_KEY: process.env.SOME_SECRET_KEY || "",
  IRON_SESSION_NAME: "DIGINEXTADMINCOOKIE",
  IRON_SESSION_SECRET: process.env.IRON_SESSION_SECRET || "",

  get SESSION_NAME() {
    return `DIGINEXTAPPCOOKIE`;
  },

  getBasePath: () => {
    return CONFIG.NEXT_PUBLIC_BASE_PATH ? "/" + CONFIG.NEXT_PUBLIC_BASE_PATH : "";
  },
  getBaseUrl: () => {
    return CONFIG.NEXT_PUBLIC_BASE_URL ? CONFIG.NEXT_PUBLIC_BASE_URL : "";
  },
  path: path => {
    return CONFIG.getBasePath() + path;
  }
};

if (false) {} else {// console.log(CONFIG);
  }

/* harmony default export */ __webpack_exports__["default"] = (CONFIG);

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy93ZWJzaXRlL2VsZW1lbnRzL0Zvb3Rlci5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL3dlYnNpdGUvaGVhZGVyL0hlYWRlci5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL3dlYnNpdGUvc2VjdGlvbi9hcnRpY2xlcy9BcnRpY2xlcy5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL3dlYnNpdGUvc2VjdGlvbi9oZXJvL0hlcm8uanMiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy93ZWJzaXRlL3NlY3Rpb24vaGVyby9UZXh0LmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2luZGV4LmpzIiwid2VicGFjazovLy8uL3BsdWdpbnMvYXNzZXRzL2Fzc2V0LmpzIiwid2VicGFjazovLy8uL3dlYi5jb25maWcuanMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwicmVhY3RcIiJdLCJuYW1lcyI6WyJGb290ZXIiLCJhc3NldCIsIkhlYWRlciIsIkFydGljbGVzIiwiSGVybyIsIlRleHQiLCJIb21lIiwicHJvcHMiLCJzcmMiLCJpc0VuYWJsZWRDRE4iLCJDT05GSUciLCJlbnZpcm9ubWVudCIsImZyYW1ld29yayIsImNkbiIsInByb2QiLCJzdGFnaW5nIiwiZGV2IiwiaXNFbmFibGVCYXNlUGF0aCIsIk5FWFRfUFVCTElDX0JBU0VfUEFUSCIsIk5FWFRfUFVCTElDX0NETl9CQVNFX1BBVEgiLCJFTlZJUk9OTUVOVF9EQVRBIiwiUFJPRFVDVElPTiIsIlNUQUdJTkciLCJERVZFTE9QTUVOVCIsInByb2Nlc3MiLCJlbnYiLCJORVhUX1BVQkxJQ19FTlYiLCJzaXRlIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsInR5cGUiLCJsaW5rcyIsImZhY2Vib29rUGFnZSIsImRhdGVGb3JtYXQiLCJORVhUX1BVQkxJQ19GQl9BUFBfSUQiLCJORVhUX1BVQkxJQ19GQl9QQUdFX0lEIiwiTkVYVF9QVUJMSUNfQVBJX0JBU0VfUEFUSCIsIk5FWFRfUFVCTElDX0FQUF9ET01BSU4iLCJORVhUX1BVQkxJQ19CQVNFX1VSTCIsIlNPTUVfU0VDUkVUX0tFWSIsIklST05fU0VTU0lPTl9OQU1FIiwiSVJPTl9TRVNTSU9OX1NFQ1JFVCIsIlNFU1NJT05fTkFNRSIsImdldEJhc2VQYXRoIiwiZ2V0QmFzZVVybCIsInBhdGgiXSwibWFwcGluZ3MiOiI7O1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxJQUFJO1FBQ0o7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEZBO0FBRWUsU0FBU0EsTUFBVCxHQUFrQjtBQUMvQixTQUNFO0FBQVEsYUFBUyxFQUFDO0FBQWxCLEtBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFLDZDQURGLEVBRUUsa0NBRkYsRUFHRSxnQ0FIRixFQUlFLHdDQUpGLEVBS0UsaUNBTEYsRUFNRSwrQkFORixFQU9FLCtCQVBGLEVBUUUsNkJBUkYsQ0FERixFQVdFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBRyxRQUFJLEVBQUM7QUFBUixLQUNFO0FBQUssT0FBRyxFQUFFQyxvRUFBSyxDQUFDLDZCQUFEO0FBQWYsSUFERixDQURGLEVBSUUsc01BSkYsRUFTRTtBQUFJLFNBQUssRUFBQztBQUFWLEtBQ0Usa0JBQ0U7QUFDRSxRQUFJLEVBQUMsb0NBRFA7QUFFRSxVQUFNLEVBQUMsUUFGVDtBQUdFLE9BQUcsRUFBQztBQUhOLGVBREYsQ0FERixFQVVFLGtCQUNFO0FBQ0UsUUFBSSxFQUFDLDRDQURQO0FBRUUsVUFBTSxFQUFDLFFBRlQ7QUFHRSxPQUFHLEVBQUM7QUFITixnQkFERixDQVZGLEVBbUJFLGtCQUNFO0FBQ0UsUUFBSSxFQUFDLHNDQURQO0FBRUUsVUFBTSxFQUFDLFFBRlQ7QUFHRSxPQUFHLEVBQUM7QUFITixXQURGLENBbkJGLENBVEYsRUFzQ0U7QUFBRyxTQUFLLEVBQUM7QUFBVCx1Q0FFRSxpQkFGRix5QkF0Q0YsQ0FERixDQVhGLENBREY7QUE0REQsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9ERDtBQUNlLFNBQVNDLE1BQVQsR0FBa0I7QUFDL0IsU0FDRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRTtBQUFHLFFBQUksRUFBQztBQUFSLEtBQ0U7QUFBSyxPQUFHLEVBQUVELG9FQUFLLENBQUMsNEJBQUQ7QUFBZixJQURGLENBREYsQ0FERixFQU1FO0FBQUksYUFBUyxFQUFDO0FBQWQsS0FDRSxrQkFDRTtBQUFHLFFBQUksRUFBQztBQUFSLFlBREYsQ0FERixFQUlFLGtCQUNFO0FBQUcsUUFBSSxFQUFDO0FBQVIsYUFERixDQUpGLEVBT0Usa0JBQ0U7QUFBRyxRQUFJLEVBQUM7QUFBUixhQURGLENBUEYsRUFVRSxrQkFDRTtBQUFHLFFBQUksRUFBQztBQUFSLGdCQURGLENBVkYsQ0FORixFQW9CRTtBQUFLLFNBQUssRUFBQztBQUFYLEtBQ0UsbUJBREYsQ0FwQkYsRUF1QkU7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFLGtCQUNFLGtCQUNFO0FBQUcsUUFBSSxFQUFDO0FBQVIsWUFERixDQURGLEVBSUUsa0JBQ0U7QUFBRyxRQUFJLEVBQUM7QUFBUixhQURGLENBSkYsRUFPRSxrQkFDRTtBQUFHLFFBQUksRUFBQztBQUFSLGFBREYsQ0FQRixFQVVFLGtCQUNDO0FBQUcsUUFBSSxFQUFDO0FBQVIsZ0JBREQsQ0FWRixDQURGLENBdkJGLENBREYsQ0FERjtBQTRDRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOUNEO0FBRWUsU0FBU0UsUUFBVCxHQUFvQjtBQUNqQyxTQUNJO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFLGlDQURGLENBREYsRUFJRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBRyxhQUFTLEVBQUMsTUFBYjtBQUFvQixRQUFJLEVBQUM7QUFBekIsS0FDRTtBQUFLLE9BQUcsRUFBRUYsb0VBQUssQ0FBQyxpQ0FBRDtBQUFmLElBREYsRUFFRSwrRUFGRixDQURGLEVBS0U7QUFBRyxhQUFTLEVBQUMsTUFBYjtBQUFvQixRQUFJLEVBQUM7QUFBekIsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQyxpQ0FBRDtBQUFmLElBREYsRUFFRSx3RkFGRixDQUxGLEVBWUU7QUFBRyxhQUFTLEVBQUMsTUFBYjtBQUFvQixRQUFJLEVBQUM7QUFBekIsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQywrQkFBRDtBQUFmLElBREYsRUFFRSwwRUFGRixDQVpGLEVBZ0JFO0FBQUcsYUFBUyxFQUFDLE1BQWI7QUFBb0IsUUFBSSxFQUFDO0FBQXpCLEtBQ0U7QUFBSyxPQUFHLEVBQUVBLG9FQUFLLENBQUMsK0JBQUQ7QUFBZixJQURGLEVBRUUsK0VBRkYsQ0FoQkYsRUFvQkU7QUFBRyxhQUFTLEVBQUMsTUFBYjtBQUFvQixRQUFJLEVBQUM7QUFBekIsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQywrQkFBRDtBQUFmLElBREYsRUFFRSxrRkFGRixDQXBCRixFQTBCRTtBQUFHLGFBQVMsRUFBQyxNQUFiO0FBQW9CLFFBQUksRUFBQztBQUF6QixLQUNFO0FBQUssT0FBRyxFQUFFQSxvRUFBSyxDQUFDLCtCQUFEO0FBQWYsSUFERixFQUVFLG1FQUZGLENBMUJGLEVBOEJFO0FBQUcsYUFBUyxFQUFDLE1BQWI7QUFBb0IsUUFBSSxFQUFDO0FBQXpCLEtBQ0U7QUFBSyxPQUFHLEVBQUVBLG9FQUFLLENBQUMsaUNBQUQ7QUFBZixJQURGLEVBRUUsbUVBRkYsQ0E5QkYsRUFrQ0U7QUFBRyxhQUFTLEVBQUMsTUFBYjtBQUFvQixRQUFJLEVBQUM7QUFBekIsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQyxpQ0FBRDtBQUFmLElBREYsRUFFRSxnREFGRixDQWxDRixFQXNDRTtBQUFHLGFBQVMsRUFBQyxNQUFiO0FBQW9CLFFBQUksRUFBQztBQUF6QixLQUNFO0FBQUssT0FBRyxFQUFFQSxvRUFBSyxDQUFDLCtCQUFEO0FBQWYsSUFERixFQUVFLGdEQUZGLENBdENGLEVBMENFO0FBQUcsYUFBUyxFQUFDLE1BQWI7QUFBb0IsUUFBSSxFQUFDO0FBQXpCLEtBQ0U7QUFBSyxPQUFHLEVBQUVBLG9FQUFLLENBQUMsK0JBQUQ7QUFBZixJQURGLEVBRUUsK0VBRkYsQ0ExQ0YsRUE4Q0U7QUFBRyxhQUFTLEVBQUMsTUFBYjtBQUFvQixRQUFJLEVBQUM7QUFBekIsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQywrQkFBRDtBQUFmLElBREYsRUFFRSxtRkFDaUUsR0FEakUsQ0FGRixDQTlDRixFQW9ERTtBQUFHLGFBQVMsRUFBQyxNQUFiO0FBQW9CLFFBQUksRUFBQztBQUF6QixLQUNFO0FBQUssT0FBRyxFQUFFQSxvRUFBSyxDQUFDLCtCQUFEO0FBQWYsSUFERixFQUVFLCtGQUVtQixHQUZuQixDQUZGLENBcERGLENBSkYsQ0FERixDQURKO0FBcUVELEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEVEO0FBQ0E7QUFFZSxTQUFTRyxJQUFULEdBQWdCO0FBQzdCLFNBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBRyxRQUFJLEVBQUM7QUFBUixLQUNFO0FBQUssT0FBRyxFQUFFSCxvRUFBSyxDQUFDLGlDQUFEO0FBQWYsSUFERixDQURGLENBREYsRUFNRSxNQUFDLDZDQUFELE9BTkYsRUFPRSxpQkFQRixDQURGLENBREY7QUFhRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQmMsU0FBU0ksSUFBVCxHQUFnQjtBQUM3QixTQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRSw0RkFERixFQUlFLHNGQUpGLENBREY7QUFTRCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNWRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRWUsU0FBU0MsSUFBVCxDQUFjQyxLQUFkLEVBQXFCO0FBQ2xDLFNBQ0UsbUVBQ0UsTUFBQyx3RUFBRCxPQURGLEVBRUUsTUFBQyw0RUFBRCxPQUZGLEVBR0UsTUFBQyxvRkFBRCxPQUhGLEVBSUUsTUFBQywwRUFBRCxPQUpGLENBREY7QUFRRCxDOzs7Ozs7Ozs7Ozs7QUNyQkQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOztBQUVBLE1BQU1OLEtBQUssR0FBSU8sR0FBRCxJQUFTO0FBQ3JCO0FBQ0E7QUFFQSxNQUFJQyxZQUFZLEdBQUcsS0FBbkI7O0FBQ0EsTUFBSUMsa0RBQU0sQ0FBQ0MsV0FBUCxJQUFzQixZQUExQixFQUF3QztBQUN0Q0YsZ0JBQVksR0FBR0csMENBQVMsQ0FBQ0MsR0FBVixDQUFjQyxJQUE3QjtBQUNELEdBRkQsTUFFTyxJQUFJSixrREFBTSxDQUFDQyxXQUFQLElBQXNCLFNBQTFCLEVBQXFDO0FBQzFDRixnQkFBWSxHQUFHRywwQ0FBUyxDQUFDQyxHQUFWLENBQWNFLE9BQTdCO0FBQ0QsR0FGTSxNQUVBLElBQUlMLGtEQUFNLENBQUNDLFdBQVAsSUFBc0IsYUFBMUIsRUFBeUM7QUFDOUNGLGdCQUFZLEdBQUdHLDBDQUFTLENBQUNDLEdBQVYsQ0FBY0csR0FBN0I7QUFDRCxHQUZNLE1BRUE7QUFDTFAsZ0JBQVksR0FBRyxLQUFmO0FBQ0Q7O0FBRUQsTUFBSVEsZ0JBQWdCLEdBQUcsS0FBdkI7O0FBQ0EsTUFBSVIsWUFBWSxJQUFJLEtBQWhCLElBQXlCQyxrREFBTSxDQUFDUSxxQkFBcEMsRUFBMkQ7QUFDekRELG9CQUFnQixHQUFHLElBQW5CO0FBQ0Q7O0FBRUQsTUFBSVIsWUFBSixFQUFrQjtBQUNoQixXQUFPQyxrREFBTSxDQUFDUyx5QkFBUCxHQUFtQyxTQUFuQyxHQUErQ1gsR0FBdEQ7QUFDRCxHQUZELE1BRU87QUFDTCxRQUFJUyxnQkFBSixFQUFzQjtBQUNwQixhQUFPLE1BQU1QLGtEQUFNLENBQUNRLHFCQUFiLEdBQXFDVixHQUE1QztBQUNELEtBRkQsTUFFTztBQUNMLGFBQU9BLEdBQVA7QUFDRDtBQUNGO0FBQ0YsQ0E3QkQ7O0FBK0JlUCxvRUFBZixFOzs7Ozs7Ozs7Ozs7QUNqQ0E7QUFBQTtBQUFPLE1BQU1tQixnQkFBZ0IsR0FBRztBQUM5QkMsWUFBVSxFQUFFLFlBRGtCO0FBRTlCQyxTQUFPLEVBQUUsU0FGcUI7QUFHOUJDLGFBQVcsRUFBRTtBQUhpQixDQUF6QjtBQU1QLE1BQU1iLE1BQU0sR0FBRztBQUNiQyxhQUFXLEVBQUVhLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxlQUFaLElBQStCLGFBRC9CO0FBRWJDLE1BQUksRUFBRTtBQUNKQyxTQUFLLEVBQUUsa0JBREg7QUFFSkMsZUFBVyxFQUFFLHVCQUZUO0FBR0pDLFFBQUksRUFBRTtBQUhGLEdBRk87QUFPYkMsT0FBSyxFQUFFO0FBQ0xDLGdCQUFZLEVBQUU7QUFEVCxHQVBNO0FBVWJDLFlBQVUsRUFBRSxxQkFWQztBQVdiO0FBQ0FDLHVCQUFxQixFQUFFVixPQUFPLENBQUNDLEdBQVIsQ0FBWVMscUJBQVosSUFBcUMsRUFaL0M7QUFZb0Q7QUFDakVDLHdCQUFzQixFQUFFWCxPQUFPLENBQUNDLEdBQVIsQ0FBWVUsc0JBQVosSUFBc0Msa0JBYmpEO0FBYXNFO0FBQ25GakIsdUJBQXFCLEVBQUVNLE9BQU8sQ0FBQ0MsR0FBUixDQUFZUCxxQkFBWixJQUFxQyxFQWQvQztBQWVia0IsMkJBQXlCLEVBQUVaLE9BQU8sQ0FBQ0MsR0FBUixDQUFZVyx5QkFBWixJQUF5QyxFQWZ2RDtBQWdCYmpCLDJCQUF5QixFQUFFSyxPQUFPLENBQUNDLEdBQVIsQ0FBWU4seUJBQVosSUFBeUMsRUFoQnZEO0FBaUJia0Isd0JBQXNCLEVBQUViLE9BQU8sQ0FBQ0MsR0FBUixDQUFZWSxzQkFBWixJQUFzQyxFQWpCakQ7QUFrQmJDLHNCQUFvQixFQUFFZCxPQUFPLENBQUNDLEdBQVIsQ0FBWWEsb0JBQVosSUFBb0MsRUFsQjdDO0FBbUJiO0FBQ0FDLGlCQUFlLEVBQUVmLE9BQU8sQ0FBQ0MsR0FBUixDQUFZYyxlQUFaLElBQStCLEVBcEJuQztBQXFCYkMsbUJBQWlCLEVBQUUscUJBckJOO0FBc0JiQyxxQkFBbUIsRUFBRWpCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZZ0IsbUJBQVosSUFBbUMsRUF0QjNDOztBQXVCYixNQUFJQyxZQUFKLEdBQW1CO0FBQ2pCLFdBQVEsbUJBQVI7QUFDRCxHQXpCWTs7QUEwQmJDLGFBQVcsRUFBRSxNQUFNO0FBQ2pCLFdBQU9qQyxNQUFNLENBQUNRLHFCQUFQLEdBQStCLE1BQU1SLE1BQU0sQ0FBQ1EscUJBQTVDLEdBQW9FLEVBQTNFO0FBQ0QsR0E1Qlk7QUE2QmIwQixZQUFVLEVBQUUsTUFBTTtBQUNoQixXQUFPbEMsTUFBTSxDQUFDNEIsb0JBQVAsR0FBOEI1QixNQUFNLENBQUM0QixvQkFBckMsR0FBNEQsRUFBbkU7QUFDRCxHQS9CWTtBQWdDYk8sTUFBSSxFQUFHQSxJQUFELElBQVU7QUFDZCxXQUFPbkMsTUFBTSxDQUFDaUMsV0FBUCxLQUF1QkUsSUFBOUI7QUFDRDtBQWxDWSxDQUFmOztBQXFDQSxXQUFrQyxFQUFsQyxNQUdPLENBQ0w7QUFDRDs7QUFFY25DLHFFQUFmLEU7Ozs7Ozs7Ozs7O0FDbkRBLGtDIiwiZmlsZSI6InBhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9wYWdlcy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCBhc3NldCBmcm9tIFwicGx1Z2lucy9hc3NldHMvYXNzZXRcIjtcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gRm9vdGVyKCkge1xuICByZXR1cm4gKFxuICAgIDxmb290ZXIgY2xhc3NOYW1lPVwic2l0ZS1mb290ZXJcIj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2xpZGVcIj5cbiAgICAgICAgPHNwYW4+RGlnaXRhbCBwcm9kdWN0IGRlc2lnbjwvc3Bhbj5cbiAgICAgICAgPHNwYW4+UmVtb3RlIHdvcms8L3NwYW4+XG4gICAgICAgIDxzcGFuPlVYIGRlc2lnbjwvc3Bhbj5cbiAgICAgICAgPHNwYW4+RGlzdHJpYnV0ZWQgdGVhbXM8L3NwYW4+XG4gICAgICAgIDxzcGFuPkNyZWF0aXZpdHk8L3NwYW4+XG4gICAgICAgIDxzcGFuPlN0cmF0ZWd5PC9zcGFuPlxuICAgICAgICA8c3Bhbj5TdXNwZW5zZTwvc3Bhbj5cbiAgICAgICAgPHNwYW4+R3Jvd3RoPC9zcGFuPlxuICAgICAgPC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRlbnRcIj5cbiAgICAgICAgICA8YSBocmVmPVwiI1wiPlxuICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL2xvZ29fbGlnaHQuc3ZnXCIpfSAvPlxuICAgICAgICAgIDwvYT5cbiAgICAgICAgICA8cD5cbiAgICAgICAgICAgIExvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQuIER1aXMgZXVcbiAgICAgICAgICAgIHZlbGl0IHRlbXB1cyBlcmF0IGVnZXN0YXMgZWZmaWNpdHVyLiBJbiBoYWMgaGFiaXRhc3NlIHBsYXRlYVxuICAgICAgICAgICAgZGljdHVtc3QuIEZ1c2NlIGEgbnVuYyBlZ2V0IGxpZ3VsYSBzdXNjaXBpdCBmaW5pYnVzLlxuICAgICAgICAgIDwvcD5cbiAgICAgICAgICA8dWwgY2xhc3M9XCJhZGRyZXNzXCI+XG4gICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vdHdpdHRlci5jb20vbWlrYW1hdGlrYWluZW5cIlxuICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgcmVsPVwibm9vcGVuZXJcIlxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgVHdpdHRlclxuICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgIGhyZWY9XCJodHRwczovL3d3dy5saW5rZWRpbi5jb20vaW4vbWlrYW1hdGlrYWluZW5cIlxuICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgcmVsPVwibm9vcGVuZXJcIlxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgTGlua2VkSW5cbiAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgICBocmVmPVwiaHR0cHM6Ly93d3cubm9yZGljcm9zZS5uZXQvYmxvZy9yc3MvXCJcbiAgICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgICAgIHJlbD1cIm5vb3BlbmVyXCJcbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIFJTU1xuICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICA8L2xpPlxuICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgPHAgY2xhc3M9XCJzaXRlLWZvb3Rlcl9fYm90dG9tLXRleHRcIj5cbiAgICAgICAgICAgIMKpIDIwMTItMjAyMSBOb3JkaWMgUm9zZSBDby5cbiAgICAgICAgICAgIDxiciAvPlxuICAgICAgICAgICAgQWxsIHJpZ2h0cyByZXNlcnZlZC5cbiAgICAgICAgICA8L3A+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9mb290ZXI+XG4gICk7XG59XG4iLCJpbXBvcnQgYXNzZXQgZnJvbSBcInBsdWdpbnMvYXNzZXRzL2Fzc2V0XCI7XHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEhlYWRlcigpIHtcclxuICByZXR1cm4gKFxyXG4gICAgPGRpdiBjbGFzc05hbWU9XCJoZWFkZXJcIj5cclxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXItZmx1aWRcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImxvZ29cIj5cclxuICAgICAgICAgIDxhIGhyZWY9XCIjXCI+XHJcbiAgICAgICAgICAgIDxpbWcgc3JjPXthc3NldChcIi9pbWFnZXMvaG9tZS9sb2dvX2Rhcmsuc3ZnXCIpfSAvPlxyXG4gICAgICAgICAgPC9hPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDx1bCBjbGFzc05hbWU9XCJuYXZcIj5cclxuICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIi9cIj5CTE9HPC9hPlxyXG4gICAgICAgICAgPC9saT5cclxuICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIi9hYm91dFwiPkFCT1VUPC9hPlxyXG4gICAgICAgICAgPC9saT5cclxuICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIi9saW5rc1wiPkxJTktTPC9hPlxyXG4gICAgICAgICAgPC9saT5cclxuICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIi9wcm9qZWN0c1wiPlBST0pFQ1RTPC9hPlxyXG4gICAgICAgICAgPC9saT5cclxuICAgICAgICA8L3VsPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJidG5tZW51XCI+XHJcbiAgICAgICAgICA8c3Bhbj48L3NwYW4+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPG5hdiBjbGFzc05hbWU9XCJtZW51LW1vYmlsZVwiPlxyXG4gICAgICAgICAgPHVsPlxyXG4gICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgPGEgaHJlZj1cIi9cIj5CTE9HPC9hPlxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgPGEgaHJlZj1cIi9hYm91dFwiPkFCT1VUPC9hPlxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgPGEgaHJlZj1cIi9saW5rc1wiPkxJTktTPC9hPlxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICA8YSBocmVmPVwiL3Byb2plY3RzXCI+UFJPSkVDVFM8L2E+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICA8L3VsPlxyXG4gICAgICAgIDwvbmF2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvZGl2PlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IGFzc2V0IGZyb20gXCJwbHVnaW5zL2Fzc2V0cy9hc3NldFwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQXJ0aWNsZXMoKSB7XHJcbiAgcmV0dXJuICggICAgXHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJ0aWNsZXNcIj5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhcnRpY2xlc19fdGl0bGVcIj5cclxuICAgICAgICAgICAgPGgzPkFsbCBhcnRpY2xlczwvaDM+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXJ0aWNsZXNfX3dyYXBcIj5cclxuICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiaXRlbVwiIGhyZWY9XCIvYXJ0aWNsZXMtZGV0YWlsXCI+XHJcbiAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL1JlY3RhbmdsZSAxMi0xLnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICA8cD5IZXJlIGFyZSBzb21lIHRoaW5ncyB5b3Ugc2hvdWxkIGtub3cgcmVnYXJkaW5nIGhvdyB3ZSB3b3JrIDwvcD5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJpdGVtXCIgaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvUmVjdGFuZ2xlIDEzLTEucG5nXCIpfSAvPlxyXG4gICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgR3Jhbm55IGdpdmVzIGV2ZXJ5b25lIHRoZSBmaW5nZXIsIGFuZCBvdGhlciB0aXBzIGZyb20gT0ZGRlxyXG4gICAgICAgICAgICAgICAgQmFyY2Vsb25hXHJcbiAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIml0ZW1cIiBocmVmPVwiL2FydGljbGVzLWRldGFpbC5qc1wiPlxyXG4gICAgICAgICAgICAgIDxpbWcgc3JjPXthc3NldChcIi9pbWFnZXMvaG9tZS9SZWN0YW5nbGUgMTMucG5nXCIpfSAvPlxyXG4gICAgICAgICAgICAgIDxwPkhlbGxvIHdvcmxkLCBvciwgaW4gb3RoZXIgd29yZHMsIHdoeSB0aGlzIGJsb2cgZXhpc3RzIDwvcD5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJpdGVtXCIgaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvUmVjdGFuZ2xlIDEyLnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICA8cD5IZXJlIGFyZSBzb21lIHRoaW5ncyB5b3Ugc2hvdWxkIGtub3cgcmVnYXJkaW5nIGhvdyB3ZSB3b3JrIDwvcD5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJpdGVtXCIgaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvUmVjdGFuZ2xlIDE0LnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICA8cD5cclxuICAgICAgICAgICAgICAgIENvbm5lY3RpbmcgYXJ0aWZpY2lhbCBpbnRlbGxpZ2VuY2Ugd2l0aCBkaWdpdGFsIHByb2R1Y3QgZGVzaWduXHJcbiAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIml0ZW1cIiBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICAgIDxpbWcgc3JjPXthc3NldChcIi9pbWFnZXMvaG9tZS9SZWN0YW5nbGUgMTUucG5nXCIpfSAvPlxyXG4gICAgICAgICAgICAgIDxwPkl04oCZcyBhbGwgYWJvdXQgZmluZGluZyB0aGUgcGVyZmVjdCBiYWxhbmNlPC9wPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIml0ZW1cIiBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICAgIDxpbWcgc3JjPXthc3NldChcIi9pbWFnZXMvaG9tZS9SZWN0YW5nbGUgMTItMi5wbmdcIil9IC8+XHJcbiAgICAgICAgICAgICAgPHA+SSBiZWxpZXZlIGxlYXJuaW5nIGlzIHRoZSBtb3N0IGltcG9ydGFudCBza2lsbCA8L3A+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiaXRlbVwiIGhyZWY9XCIjXCI+XHJcbiAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL1JlY3RhbmdsZSAxMy0yLnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICA8cD5DbGllbnRzIGFyZSBwYXJ0IG9mIHRoZSB0ZWFtPC9wPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIml0ZW1cIiBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICAgIDxpbWcgc3JjPXthc3NldChcIi9pbWFnZXMvaG9tZS9SZWN0YW5nbGUgMTkucG5nXCIpfSAvPlxyXG4gICAgICAgICAgICAgIDxwPkNsaWVudHMgYXJlIHBhcnQgb2YgdGhlIHRlYW08L3A+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiaXRlbVwiIGhyZWY9XCIjXCI+XHJcbiAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL1JlY3RhbmdsZSAxNy5wbmdcIil9IC8+XHJcbiAgICAgICAgICAgICAgPHA+SGVyZSBhcmUgc29tZSB0aGluZ3MgeW91IHNob3VsZCBrbm93IHJlZ2FyZGluZyBob3cgd2Ugd29yayA8L3A+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiaXRlbVwiIGhyZWY9XCIjXCI+XHJcbiAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL1JlY3RhbmdsZSAxNi5wbmdcIil9IC8+XHJcbiAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICBDb25uZWN0aW5nIGFydGlmaWNpYWwgaW50ZWxsaWdlbmNlIHdpdGggZGlnaXRhbCBwcm9kdWN0IGRlc2lnbntcIiBcIn1cclxuICAgICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiaXRlbVwiIGhyZWY9XCIjXCI+XHJcbiAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL1JlY3RhbmdsZSAxOC5wbmdcIil9IC8+XHJcbiAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICBIb3cgbW9kZXJuIHJlbW90ZSB3b3JraW5nIHRvb2xzIGdldCBhbG9uZyB3aXRoIE9sZCBTY2hvb2xcclxuICAgICAgICAgICAgICAgIENvd2JveSdzIG1ldGhvZHN7XCIgXCJ9XHJcbiAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgKTtcclxufVxyXG4iLCJpbXBvcnQgYXNzZXQgZnJvbSBcInBsdWdpbnMvYXNzZXRzL2Fzc2V0XCI7XHJcbmltcG9ydCBUZXh0IGZyb20gXCIuL1RleHRcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEhlcm8oKSB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVyb1wiPlxyXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaW1nXCI+XHJcbiAgICAgICAgICA8YSBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvaW1hZ2Utc2VjdGlvbjEuanBnXCIpfSAvPlxyXG4gICAgICAgICAgPC9hPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxUZXh0PjwvVGV4dD5cclxuICAgICAgICA8aHI+PC9ocj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICApO1xyXG59XHJcbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFRleHQoKSB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxkaXYgY2xhc3NOYW1lPVwidGV4dFwiPlxyXG4gICAgICA8aDE+XHJcbiAgICAgICAgQSBmZXcgd29yZHMgYWJvdXQgdGhpcyBibG9nIHBsYXRmb3JtLCBHaG9zdCwgYW5kIGhvdyB0aGlzIHNpdGUgd2FzIG1hZGVcclxuICAgICAgPC9oMT5cclxuICAgICAgPHA+V2h5IEdob3N0ICgmIEZpZ21hKSBpbnN0ZWFkIG9mIE1lZGl1bSwgV29yZFByZXNzIG9yIG90aGVyIG9wdGlvbnM/PC9wPlxyXG4gICAgICBcclxuICAgIDwvZGl2PlxyXG4gICk7XHJcbn1cclxuIiwiLy8gaW1wb3J0IENPTkZJRyBmcm9tIFwid2ViLmNvbmZpZ1wiO1xuLy9pbXBvcnQgTWFzdGVyUGFnZUV4YW1wbGUgZnJvbSBcImNvbXBvbmVudHMvd2Vic2l0ZS9tYXN0ZXIvTWFzdGVyUGFnZUV4YW1wbGVcIjtcbi8vIGltcG9ydCBCYXNpY0xheW91dCBmcm9tIFwiY29tcG9uZW50cy9kaWdpbmV4dC9sYXlvdXQvQmFzaWNMYXlvdXRcIjtcbi8vIGltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xuLy8gaW1wb3J0IEhlYWRlciBmcm9tIFwiY29tcG9uZW50cy93ZWJzaXRlL2VsZW1lbnRzL0hlYWRlclwiO1xuLy9pbXBvcnQgRGFzaGtpdEJ1dHRvbiBmcm9tIFwiY29tcG9uZW50cy9kYXNoa2l0L0J1dHRvbnNcIjtcbi8vaW1wb3J0IHsgQlMgfSBmcm9tIFwiY29tcG9uZW50cy9kaWdpbmV4dC9lbGVtZW50cy9TcGxpdHRlcnNcIjtcbmltcG9ydCBIZWFkZXIgZnJvbSBcImNvbXBvbmVudHMvd2Vic2l0ZS9oZWFkZXIvSGVhZGVyXCI7XG5pbXBvcnQgSGVybyBmcm9tIFwiY29tcG9uZW50cy93ZWJzaXRlL3NlY3Rpb24vaGVyby9IZXJvXCI7XG5pbXBvcnQgQXJ0aWNsZXMgZnJvbSBcImNvbXBvbmVudHMvd2Vic2l0ZS9zZWN0aW9uL2FydGljbGVzL0FydGljbGVzXCI7XG5pbXBvcnQgRm9vdGVyIGZyb20gXCJjb21wb25lbnRzL3dlYnNpdGUvZWxlbWVudHMvRm9vdGVyXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEhvbWUocHJvcHMpIHtcbiAgcmV0dXJuIChcbiAgICA8PlxuICAgICAgPEhlYWRlcj48L0hlYWRlcj5cbiAgICAgIDxIZXJvPjwvSGVybz5cbiAgICAgIDxBcnRpY2xlcz48L0FydGljbGVzPlxuICAgICAgPEZvb3Rlcj48L0Zvb3Rlcj5cbiAgICA8Lz5cbiAgKTtcbn1cbiIsImltcG9ydCBDT05GSUcgZnJvbSBcIndlYi5jb25maWdcIjtcbmltcG9ydCBmcmFtZXdvcmsgZnJvbSBcImRpZ2luZXh0Lmpzb25cIjtcblxuY29uc3QgYXNzZXQgPSAoc3JjKSA9PiB7XG4gIC8vIGNvbnNvbGUubG9nKENPTkZJRy5ORVhUX1BVQkxJQ19DRE5fQkFTRV9QQVRIKTtcbiAgLy8gY29uc29sZS5sb2coZnJhbWV3b3JrKTtcblxuICBsZXQgaXNFbmFibGVkQ0ROID0gZmFsc2U7XG4gIGlmIChDT05GSUcuZW52aXJvbm1lbnQgPT0gXCJwcm9kdWN0aW9uXCIpIHtcbiAgICBpc0VuYWJsZWRDRE4gPSBmcmFtZXdvcmsuY2RuLnByb2Q7XG4gIH0gZWxzZSBpZiAoQ09ORklHLmVudmlyb25tZW50ID09IFwic3RhZ2luZ1wiKSB7XG4gICAgaXNFbmFibGVkQ0ROID0gZnJhbWV3b3JrLmNkbi5zdGFnaW5nO1xuICB9IGVsc2UgaWYgKENPTkZJRy5lbnZpcm9ubWVudCA9PSBcImRldmVsb3BtZW50XCIpIHtcbiAgICBpc0VuYWJsZWRDRE4gPSBmcmFtZXdvcmsuY2RuLmRldjtcbiAgfSBlbHNlIHtcbiAgICBpc0VuYWJsZWRDRE4gPSBmYWxzZTtcbiAgfVxuXG4gIGxldCBpc0VuYWJsZUJhc2VQYXRoID0gZmFsc2U7XG4gIGlmIChpc0VuYWJsZWRDRE4gPT0gZmFsc2UgJiYgQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfUEFUSCkge1xuICAgIGlzRW5hYmxlQmFzZVBhdGggPSB0cnVlO1xuICB9XG5cbiAgaWYgKGlzRW5hYmxlZENETikge1xuICAgIHJldHVybiBDT05GSUcuTkVYVF9QVUJMSUNfQ0ROX0JBU0VfUEFUSCArIFwiL3B1YmxpY1wiICsgc3JjO1xuICB9IGVsc2Uge1xuICAgIGlmIChpc0VuYWJsZUJhc2VQYXRoKSB7XG4gICAgICByZXR1cm4gXCIvXCIgKyBDT05GSUcuTkVYVF9QVUJMSUNfQkFTRV9QQVRIICsgc3JjO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gc3JjO1xuICAgIH1cbiAgfVxufTtcblxuZXhwb3J0IGRlZmF1bHQgYXNzZXQ7XG4iLCJcbmV4cG9ydCBjb25zdCBFTlZJUk9OTUVOVF9EQVRBID0ge1xuICBQUk9EVUNUSU9OOiBcInByb2R1Y3Rpb25cIixcbiAgU1RBR0lORzogXCJzdGFnaW5nXCIsXG4gIERFVkVMT1BNRU5UOiBcImRldmVsb3BtZW50XCIsXG59XG5cbmNvbnN0IENPTkZJRyA9IHtcbiAgZW52aXJvbm1lbnQ6IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0VOViB8fCBcImRldmVsb3BtZW50XCIsXG4gIHNpdGU6IHtcbiAgICB0aXRsZTogXCJEaWdpbmV4dCBXZWJzaXRlXCIsXG4gICAgZGVzY3JpcHRpb246IFwiRGVzY3JpcHRpb24gZ29lcyBoZXJlXCIsXG4gICAgdHlwZTogXCJhcnRpY2xlXCIsXG4gIH0sXG4gIGxpbmtzOiB7XG4gICAgZmFjZWJvb2tQYWdlOiBcIlwiLFxuICB9LFxuICBkYXRlRm9ybWF0OiBcInl5eXktTU0tZGQgSEg6bW06c3NcIixcbiAgLy8gdGhlc2UgdmFyaWFibGVzIGNhbiBiZSBleHBvc2VkIHRvIGZyb250LWVuZDpcbiAgTkVYVF9QVUJMSUNfRkJfQVBQX0lEOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19GQl9BUFBfSUQgfHwgXCJcIiwgIC8vIGN1cnJlbnRseSB1c2luZyBYWFhcbiAgTkVYVF9QVUJMSUNfRkJfUEFHRV9JRDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfRkJfUEFHRV9JRCB8fCBcIjExNjIyMTQwNDA1MzI5MDJcIiwgIC8vIGN1cnJlbnRseSB1c2luZyBEaWdpdG9wIGRldmVsb3BlcnNcbiAgTkVYVF9QVUJMSUNfQkFTRV9QQVRIOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19CQVNFX1BBVEggfHwgXCJcIixcbiAgTkVYVF9QVUJMSUNfQVBJX0JBU0VfUEFUSDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfQVBJX0JBU0VfUEFUSCB8fCBcIlwiLFxuICBORVhUX1BVQkxJQ19DRE5fQkFTRV9QQVRIOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19DRE5fQkFTRV9QQVRIIHx8IFwiXCIsXG4gIE5FWFRfUFVCTElDX0FQUF9ET01BSU46IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0FQUF9ET01BSU4gfHwgXCJcIixcbiAgTkVYVF9QVUJMSUNfQkFTRV9VUkw6IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0JBU0VfVVJMIHx8IFwiXCIsXG4gIC8vIHNvbWUgc2VjcmV0IGtleXMgd2hpY2ggd29uJ3QgYmUgZXhwb3NlZCB0byBmcm9udC1lbmQ6XG4gIFNPTUVfU0VDUkVUX0tFWTogcHJvY2Vzcy5lbnYuU09NRV9TRUNSRVRfS0VZIHx8IFwiXCIsXG4gIElST05fU0VTU0lPTl9OQU1FOiBcIkRJR0lORVhUQURNSU5DT09LSUVcIixcbiAgSVJPTl9TRVNTSU9OX1NFQ1JFVDogcHJvY2Vzcy5lbnYuSVJPTl9TRVNTSU9OX1NFQ1JFVCB8fCBcIlwiLFxuICBnZXQgU0VTU0lPTl9OQU1FKCkge1xuICAgIHJldHVybiBgRElHSU5FWFRBUFBDT09LSUVgO1xuICB9LFxuICBnZXRCYXNlUGF0aDogKCkgPT4ge1xuICAgIHJldHVybiBDT05GSUcuTkVYVF9QVUJMSUNfQkFTRV9QQVRIID8gXCIvXCIgKyBDT05GSUcuTkVYVF9QVUJMSUNfQkFTRV9QQVRIIDogXCJcIjtcbiAgfSxcbiAgZ2V0QmFzZVVybDogKCkgPT4ge1xuICAgIHJldHVybiBDT05GSUcuTkVYVF9QVUJMSUNfQkFTRV9VUkwgPyBDT05GSUcuTkVYVF9QVUJMSUNfQkFTRV9VUkwgOiBcIlwiO1xuICB9LFxuICBwYXRoOiAocGF0aCkgPT4ge1xuICAgIHJldHVybiBDT05GSUcuZ2V0QmFzZVBhdGgoKSArIHBhdGg7XG4gIH0sXG59O1xuXG5pZiAodHlwZW9mIHdpbmRvdyAhPSBcInVuZGVmaW5lZFwiKSB7XG4gIHdpbmRvdy5fX2NvbmZpZ19fID0gQ09ORklHO1xuICAvLyBjb25zb2xlLmxvZyhDT05GSUcpO1xufSBlbHNlIHtcbiAgLy8gY29uc29sZS5sb2coQ09ORklHKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgQ09ORklHO1xuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3RcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==