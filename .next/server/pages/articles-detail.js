module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/articles-detail.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/website/elements/Footer.js":
/*!***********************************************!*\
  !*** ./components/website/elements/Footer.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Footer; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! plugins/assets/asset */ "./plugins/assets/asset.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function Footer() {
  return __jsx("footer", {
    className: "site-footer"
  }, __jsx("div", {
    className: "slide"
  }, __jsx("span", null, "Digital product design"), __jsx("span", null, "Remote work"), __jsx("span", null, "UX design"), __jsx("span", null, "Distributed teams"), __jsx("span", null, "Creativity"), __jsx("span", null, "Strategy"), __jsx("span", null, "Suspense"), __jsx("span", null, "Growth")), __jsx("div", {
    className: "container"
  }, __jsx("div", {
    className: "content"
  }, __jsx("a", {
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/logo_light.svg")
  })), __jsx("p", null, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu velit tempus erat egestas efficitur. In hac habitasse platea dictumst. Fusce a nunc eget ligula suscipit finibus."), __jsx("ul", {
    class: "address"
  }, __jsx("li", null, __jsx("a", {
    href: "https://twitter.com/mikamatikainen",
    target: "_blank",
    rel: "noopener"
  }, "Twitter")), __jsx("li", null, __jsx("a", {
    href: "https://www.linkedin.com/in/mikamatikainen",
    target: "_blank",
    rel: "noopener"
  }, "LinkedIn")), __jsx("li", null, __jsx("a", {
    href: "https://www.nordicrose.net/blog/rss/",
    target: "_blank",
    rel: "noopener"
  }, "RSS"))), __jsx("p", {
    class: "site-footer__bottom-text"
  }, "\xA9 2012-2021 Nordic Rose Co.", __jsx("br", null), "All rights reserved."))));
}

/***/ }),

/***/ "./components/website/header/Header.js":
/*!*********************************************!*\
  !*** ./components/website/header/Header.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Header; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! plugins/assets/asset */ "./plugins/assets/asset.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function Header() {
  return __jsx("div", {
    className: "header"
  }, __jsx("div", {
    className: "container-fluid"
  }, __jsx("div", {
    className: "logo"
  }, __jsx("a", {
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/logo_dark.svg")
  }))), __jsx("ul", {
    className: "nav"
  }, __jsx("li", null, __jsx("a", {
    href: "/"
  }, "BLOG")), __jsx("li", null, __jsx("a", {
    href: "/about"
  }, "ABOUT")), __jsx("li", null, __jsx("a", {
    href: "/links"
  }, "LINKS")), __jsx("li", null, __jsx("a", {
    href: "/projects"
  }, "PROJECTS"))), __jsx("div", {
    class: "btnmenu"
  }, __jsx("span", null)), __jsx("nav", {
    className: "menu-mobile"
  }, __jsx("ul", null, __jsx("li", null, __jsx("a", {
    href: "/"
  }, "BLOG")), __jsx("li", null, __jsx("a", {
    href: "/about"
  }, "ABOUT")), __jsx("li", null, __jsx("a", {
    href: "/links"
  }, "LINKS")), __jsx("li", null, __jsx("a", {
    href: "/projects"
  }, "PROJECTS"))))));
}

/***/ }),

/***/ "./components/website/section/bottom-article-detail/Bottom.js":
/*!********************************************************************!*\
  !*** ./components/website/section/bottom-article-detail/Bottom.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Bottom; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! plugins/assets/asset */ "./plugins/assets/asset.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function Bottom() {
  return __jsx("div", {
    className: "read"
  }, __jsx("div", {
    className: "read__eye"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/eye.png")
  })), __jsx("div", {
    className: "container"
  }, __jsx("div", {
    className: "read-next"
  }, __jsx("h2", null, "What to read next"), __jsx("div", {
    className: "read-wrap"
  }, __jsx("a", {
    className: "item",
    href: "/articles-detail"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 12-1.png")
  }), __jsx("p", null, "Here are some things you should know regarding how we work ")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 13-1.png")
  }), __jsx("p", null, "Granny gives everyone the finger, and other tips from OFFF Barcelona")), __jsx("a", {
    className: "item",
    href: "/articles-detail.js"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 13.png")
  }), __jsx("p", null, "Hello world, or, in other words, why this blog exists ")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 12.png")
  }), __jsx("p", null, "Here are some things you should know regarding how we work ")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 14.png")
  }), __jsx("p", null, "Connecting artificial intelligence with digital product design")), __jsx("a", {
    className: "item",
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_1__["default"])("/images/home/Rectangle 15.png")
  }), __jsx("p", null, "It\u2019s all about finding the perfect balance"))))));
}

/***/ }),

/***/ "./components/website/section/bottom-article-detail/Form.js":
/*!******************************************************************!*\
  !*** ./components/website/section/bottom-article-detail/Form.js ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Form; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

function Form() {
  return __jsx("section", {
    class: "form"
  }, __jsx("div", {
    class: "container"
  }, __jsx("div", {
    class: "form__wrap"
  }, __jsx("div", {
    class: "form-text"
  }, __jsx("h2", null, "Sign up for the newsletter"), __jsx("p", null, "If you want relevant updates occasionally, sign up for the private newsletter. Your email is never shared.")), __jsx("form", {
    "data-members-form": "subscribe"
  }, __jsx("div", {
    className: "form-input"
  }, __jsx("input", {
    "data-members-email": "",
    type: "email",
    placeholder: "Enter your email...",
    autocomplete: "false"
  }), __jsx("button", {
    type: "submit"
  }, __jsx("span", null, "Sign up")))))));
}

/***/ }),

/***/ "./components/website/section/hero/Text.js":
/*!*************************************************!*\
  !*** ./components/website/section/hero/Text.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Text; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;
function Text() {
  return __jsx("div", {
    className: "text"
  }, __jsx("h1", null, "A few words about this blog platform, Ghost, and how this site was made"), __jsx("p", null, "Why Ghost (& Figma) instead of Medium, WordPress or other options?"));
}

/***/ }),

/***/ "./diginext.json":
/*!***********************!*\
  !*** ./diginext.json ***!
  \***********************/
/*! exports provided: git, projectSlug, projectName, username, version, diginext-cli, domain, cdn-host, cdn, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"git\":{\"creator\":\"digitop-duynguyen\",\"repoURL\":\"https://bitbucket.org/digitopvn/diginext/src\"},\"projectSlug\":\"diginext\",\"projectName\":\"Diginext Framework\",\"username\":\"digitop-duynguyen\",\"version\":\"v1.3.0\",\"diginext-cli\":\"0.4.1\",\"domain\":{\"staging\":[],\"prod\":[\"diginext.com.vn\"]},\"cdn-host\":{\"dev\":\"https://storage.googleapis.com/digitop-cdn-sg/diginext/dev\",\"dev-nocache\":\"https://storage.googleapis.com/digitop-cdn-sg/diginext/dev\",\"prod\":\"https://storage.googleapis.com/digitop-cdn-sg/diginext/prod\",\"prod-nocache\":\"https://storage.googleapis.com/digitop-cdn-sg/diginext/prod\"},\"cdn\":{\"dev\":false,\"staging\":false,\"prod\":false}}");

/***/ }),

/***/ "./pages/articles-detail.js":
/*!**********************************!*\
  !*** ./pages/articles-detail.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return articlesdetail; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_website_section_hero_Text__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/website/section/hero/Text */ "./components/website/section/hero/Text.js");
/* harmony import */ var components_website_header_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! components/website/header/Header */ "./components/website/header/Header.js");
/* harmony import */ var plugins_assets_asset__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! plugins/assets/asset */ "./plugins/assets/asset.js");
/* harmony import */ var _components_website_section_bottom_article_detail_Bottom__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/components/website/section/bottom-article-detail/Bottom */ "./components/website/section/bottom-article-detail/Bottom.js");
/* harmony import */ var _components_website_section_bottom_article_detail_Form__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/components/website/section/bottom-article-detail/Form */ "./components/website/section/bottom-article-detail/Form.js");
/* harmony import */ var _components_website_elements_Footer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/components/website/elements/Footer */ "./components/website/elements/Footer.js");

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






function articlesdetail(props) {
  return __jsx("div", null, __jsx(components_website_header_Header__WEBPACK_IMPORTED_MODULE_2__["default"], null), __jsx("div", {
    className: "container"
  }, __jsx(_components_website_section_hero_Text__WEBPACK_IMPORTED_MODULE_1__["default"], null), __jsx("div", {
    className: "figure"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_3__["default"])("/images/home/image 2.1.png")
  })), __jsx("hr", null), __jsx("div", {
    className: "blog"
  }, __jsx("div", {
    className: "blog__meta"
  }, __jsx("div", {
    className: "avatar"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_3__["default"])("/images/home/avtart.png")
  })), __jsx("div", {
    className: "info"
  }, __jsx("p", null, __jsx("b", null, "Mika MATIKAINEN"), __jsx("br", null), __jsx("span", null, "Apr 15, 2020 \xB7 4 min read"))), __jsx("div", {
    className: "btn-share"
  }, __jsx("a", {
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_3__["default"])("/images/home/facebook.png")
  })), __jsx("a", {
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_3__["default"])("/images/home/twiter.png")
  })))), __jsx("div", {
    className: "blog__description"
  }, __jsx("p", null, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu velit tempus erat egestas efficitur. In hac habitasse platea dictumst. Fusce a nunc eget ligula suscipit finibus. Aenean pharetra quis lacus at viverra."), __jsx("p", null, "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam quis posuere ligula. In eu dui molestie, molestie lectus eu, semper lectus."), __jsx("h3", null, "Next on the pipeline"), __jsx("p", null, "Duis eu velit tempus erat egestas efficitur. In hac habitasse platea dictumst. Fusce a nunc eget ligula suscipit finibus. Aenean pharetra quis lacus at viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos."), __jsx("p", null, "Morbi efficitur auctor metus, id mollis lorem pellentesque id. Nullam posuere maximus dui et fringilla."), __jsx("div", {
    className: "img"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_3__["default"])("/images/home/Rectangle 8.png")
  }), __jsx("figcaption", null, "Image caption centered this way and I\u2019ll make this a bit longer to indicate the amount of line-height.")), __jsx("p", null, "Aenean pharetra quis lacus at viverra. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam quis posuere ligula."), __jsx("p", null, "In eu dui molestie, molestie lectus eu, semper lectus. Proin at justo lacinia, auctor nisl et, consequat ante. Donec sit amet nisi arcu. Morbi efficitur auctor metus, id mollis lorem pellentesque id. Nullam posuere maximus dui et fringilla. Nulla non volutpat leo."), __jsx("p", null, "A list looks like this:"), __jsx("ul", null, __jsx("li", null, "First item in the list "), __jsx("li", null, "Second item in the list lorem ipsum dolor sit amet nunc felis dolor lorem ipsum sit amet"), __jsx("li", null, "Third item in the list")), __jsx("p", null, "Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam quis posuere ligula."), __jsx("p", null, "Thanks for reading,", __jsx("br", null), " Mika")), __jsx("div", {
    className: "blog__bottom"
  }, __jsx("div", {
    className: "bottom-share"
  }, __jsx("a", {
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_3__["default"])("/images/home/facebook.png")
  }), __jsx("span", null, "Share on Facebook")), __jsx("a", {
    href: "#"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_3__["default"])("/images/home/twiter.png")
  }), __jsx("span", null, "Share on Twitter"))), __jsx("ul", {
    className: "tags"
  }, "Tags:", __jsx("li", null, __jsx("a", {
    href: "/tag/distributed-teams/",
    title: "distributed teams"
  }, "product design, culture"))), __jsx("div", {
    className: "author"
  }, __jsx("div", {
    className: "author__img"
  }, __jsx("img", {
    src: Object(plugins_assets_asset__WEBPACK_IMPORTED_MODULE_3__["default"])("/images/home/avtart.png")
  })), __jsx("div", {
    className: "author__info"
  }, __jsx("p", null, __jsx("span", null, "Mika Matikainen "), "is a Design Founder & Advisor, Berlin School of Creative Leadership Executive MBA participant, Zippie advisor, Wolt co-founder, and Nordic Rose stakeholder.")))))), __jsx(_components_website_section_bottom_article_detail_Bottom__WEBPACK_IMPORTED_MODULE_4__["default"], null), __jsx(_components_website_section_bottom_article_detail_Form__WEBPACK_IMPORTED_MODULE_5__["default"], null), __jsx(_components_website_elements_Footer__WEBPACK_IMPORTED_MODULE_6__["default"], null));
}

/***/ }),

/***/ "./plugins/assets/asset.js":
/*!*********************************!*\
  !*** ./plugins/assets/asset.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var web_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! web.config */ "./web.config.js");
/* harmony import */ var diginext_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! diginext.json */ "./diginext.json");
var diginext_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! diginext.json */ "./diginext.json", 1);



const asset = src => {
  // console.log(CONFIG.NEXT_PUBLIC_CDN_BASE_PATH);
  // console.log(framework);
  let isEnabledCDN = false;

  if (web_config__WEBPACK_IMPORTED_MODULE_0__["default"].environment == "production") {
    isEnabledCDN = diginext_json__WEBPACK_IMPORTED_MODULE_1__.cdn.prod;
  } else if (web_config__WEBPACK_IMPORTED_MODULE_0__["default"].environment == "staging") {
    isEnabledCDN = diginext_json__WEBPACK_IMPORTED_MODULE_1__.cdn.staging;
  } else if (web_config__WEBPACK_IMPORTED_MODULE_0__["default"].environment == "development") {
    isEnabledCDN = diginext_json__WEBPACK_IMPORTED_MODULE_1__.cdn.dev;
  } else {
    isEnabledCDN = false;
  }

  let isEnableBasePath = false;

  if (isEnabledCDN == false && web_config__WEBPACK_IMPORTED_MODULE_0__["default"].NEXT_PUBLIC_BASE_PATH) {
    isEnableBasePath = true;
  }

  if (isEnabledCDN) {
    return web_config__WEBPACK_IMPORTED_MODULE_0__["default"].NEXT_PUBLIC_CDN_BASE_PATH + "/public" + src;
  } else {
    if (isEnableBasePath) {
      return "/" + web_config__WEBPACK_IMPORTED_MODULE_0__["default"].NEXT_PUBLIC_BASE_PATH + src;
    } else {
      return src;
    }
  }
};

/* harmony default export */ __webpack_exports__["default"] = (asset);

/***/ }),

/***/ "./web.config.js":
/*!***********************!*\
  !*** ./web.config.js ***!
  \***********************/
/*! exports provided: ENVIRONMENT_DATA, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENVIRONMENT_DATA", function() { return ENVIRONMENT_DATA; });
const ENVIRONMENT_DATA = {
  PRODUCTION: "production",
  STAGING: "staging",
  DEVELOPMENT: "development"
};
const CONFIG = {
  environment: process.env.NEXT_PUBLIC_ENV || "development",
  site: {
    title: "Diginext Website",
    description: "Description goes here",
    type: "article"
  },
  links: {
    facebookPage: ""
  },
  dateFormat: "yyyy-MM-dd HH:mm:ss",
  // these variables can be exposed to front-end:
  NEXT_PUBLIC_FB_APP_ID: process.env.NEXT_PUBLIC_FB_APP_ID || "",
  // currently using XXX
  NEXT_PUBLIC_FB_PAGE_ID: process.env.NEXT_PUBLIC_FB_PAGE_ID || "1162214040532902",
  // currently using Digitop developers
  NEXT_PUBLIC_BASE_PATH: process.env.NEXT_PUBLIC_BASE_PATH || "",
  NEXT_PUBLIC_API_BASE_PATH: process.env.NEXT_PUBLIC_API_BASE_PATH || "",
  NEXT_PUBLIC_CDN_BASE_PATH: process.env.NEXT_PUBLIC_CDN_BASE_PATH || "",
  NEXT_PUBLIC_APP_DOMAIN: process.env.NEXT_PUBLIC_APP_DOMAIN || "",
  NEXT_PUBLIC_BASE_URL: process.env.NEXT_PUBLIC_BASE_URL || "",
  // some secret keys which won't be exposed to front-end:
  SOME_SECRET_KEY: process.env.SOME_SECRET_KEY || "",
  IRON_SESSION_NAME: "DIGINEXTADMINCOOKIE",
  IRON_SESSION_SECRET: process.env.IRON_SESSION_SECRET || "",

  get SESSION_NAME() {
    return `DIGINEXTAPPCOOKIE`;
  },

  getBasePath: () => {
    return CONFIG.NEXT_PUBLIC_BASE_PATH ? "/" + CONFIG.NEXT_PUBLIC_BASE_PATH : "";
  },
  getBaseUrl: () => {
    return CONFIG.NEXT_PUBLIC_BASE_URL ? CONFIG.NEXT_PUBLIC_BASE_URL : "";
  },
  path: path => {
    return CONFIG.getBasePath() + path;
  }
};

if (false) {} else {// console.log(CONFIG);
  }

/* harmony default export */ __webpack_exports__["default"] = (CONFIG);

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy93ZWJzaXRlL2VsZW1lbnRzL0Zvb3Rlci5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL3dlYnNpdGUvaGVhZGVyL0hlYWRlci5qcyIsIndlYnBhY2s6Ly8vLi9jb21wb25lbnRzL3dlYnNpdGUvc2VjdGlvbi9ib3R0b20tYXJ0aWNsZS1kZXRhaWwvQm90dG9tLmpzIiwid2VicGFjazovLy8uL2NvbXBvbmVudHMvd2Vic2l0ZS9zZWN0aW9uL2JvdHRvbS1hcnRpY2xlLWRldGFpbC9Gb3JtLmpzIiwid2VicGFjazovLy8uL2NvbXBvbmVudHMvd2Vic2l0ZS9zZWN0aW9uL2hlcm8vVGV4dC5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9hcnRpY2xlcy1kZXRhaWwuanMiLCJ3ZWJwYWNrOi8vLy4vcGx1Z2lucy9hc3NldHMvYXNzZXQuanMiLCJ3ZWJwYWNrOi8vLy4vd2ViLmNvbmZpZy5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIl0sIm5hbWVzIjpbIkZvb3RlciIsImFzc2V0IiwiSGVhZGVyIiwiQm90dG9tIiwiRm9ybSIsIlRleHQiLCJhcnRpY2xlc2RldGFpbCIsInByb3BzIiwic3JjIiwiaXNFbmFibGVkQ0ROIiwiQ09ORklHIiwiZW52aXJvbm1lbnQiLCJmcmFtZXdvcmsiLCJjZG4iLCJwcm9kIiwic3RhZ2luZyIsImRldiIsImlzRW5hYmxlQmFzZVBhdGgiLCJORVhUX1BVQkxJQ19CQVNFX1BBVEgiLCJORVhUX1BVQkxJQ19DRE5fQkFTRV9QQVRIIiwiRU5WSVJPTk1FTlRfREFUQSIsIlBST0RVQ1RJT04iLCJTVEFHSU5HIiwiREVWRUxPUE1FTlQiLCJwcm9jZXNzIiwiZW52IiwiTkVYVF9QVUJMSUNfRU5WIiwic2l0ZSIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJ0eXBlIiwibGlua3MiLCJmYWNlYm9va1BhZ2UiLCJkYXRlRm9ybWF0IiwiTkVYVF9QVUJMSUNfRkJfQVBQX0lEIiwiTkVYVF9QVUJMSUNfRkJfUEFHRV9JRCIsIk5FWFRfUFVCTElDX0FQSV9CQVNFX1BBVEgiLCJORVhUX1BVQkxJQ19BUFBfRE9NQUlOIiwiTkVYVF9QVUJMSUNfQkFTRV9VUkwiLCJTT01FX1NFQ1JFVF9LRVkiLCJJUk9OX1NFU1NJT05fTkFNRSIsIklST05fU0VTU0lPTl9TRUNSRVQiLCJTRVNTSU9OX05BTUUiLCJnZXRCYXNlUGF0aCIsImdldEJhc2VVcmwiLCJwYXRoIl0sIm1hcHBpbmdzIjoiOztRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsSUFBSTtRQUNKO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUVlLFNBQVNBLE1BQVQsR0FBa0I7QUFDL0IsU0FDRTtBQUFRLGFBQVMsRUFBQztBQUFsQixLQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRSw2Q0FERixFQUVFLGtDQUZGLEVBR0UsZ0NBSEYsRUFJRSx3Q0FKRixFQUtFLGlDQUxGLEVBTUUsK0JBTkYsRUFPRSwrQkFQRixFQVFFLDZCQVJGLENBREYsRUFXRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUcsUUFBSSxFQUFDO0FBQVIsS0FDRTtBQUFLLE9BQUcsRUFBRUMsb0VBQUssQ0FBQyw2QkFBRDtBQUFmLElBREYsQ0FERixFQUlFLHNNQUpGLEVBU0U7QUFBSSxTQUFLLEVBQUM7QUFBVixLQUNFLGtCQUNFO0FBQ0UsUUFBSSxFQUFDLG9DQURQO0FBRUUsVUFBTSxFQUFDLFFBRlQ7QUFHRSxPQUFHLEVBQUM7QUFITixlQURGLENBREYsRUFVRSxrQkFDRTtBQUNFLFFBQUksRUFBQyw0Q0FEUDtBQUVFLFVBQU0sRUFBQyxRQUZUO0FBR0UsT0FBRyxFQUFDO0FBSE4sZ0JBREYsQ0FWRixFQW1CRSxrQkFDRTtBQUNFLFFBQUksRUFBQyxzQ0FEUDtBQUVFLFVBQU0sRUFBQyxRQUZUO0FBR0UsT0FBRyxFQUFDO0FBSE4sV0FERixDQW5CRixDQVRGLEVBc0NFO0FBQUcsU0FBSyxFQUFDO0FBQVQsdUNBRUUsaUJBRkYseUJBdENGLENBREYsQ0FYRixDQURGO0FBNERELEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvREQ7QUFDZSxTQUFTQyxNQUFULEdBQWtCO0FBQy9CLFNBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBRyxRQUFJLEVBQUM7QUFBUixLQUNFO0FBQUssT0FBRyxFQUFFRCxvRUFBSyxDQUFDLDRCQUFEO0FBQWYsSUFERixDQURGLENBREYsRUFNRTtBQUFJLGFBQVMsRUFBQztBQUFkLEtBQ0Usa0JBQ0U7QUFBRyxRQUFJLEVBQUM7QUFBUixZQURGLENBREYsRUFJRSxrQkFDRTtBQUFHLFFBQUksRUFBQztBQUFSLGFBREYsQ0FKRixFQU9FLGtCQUNFO0FBQUcsUUFBSSxFQUFDO0FBQVIsYUFERixDQVBGLEVBVUUsa0JBQ0U7QUFBRyxRQUFJLEVBQUM7QUFBUixnQkFERixDQVZGLENBTkYsRUFvQkU7QUFBSyxTQUFLLEVBQUM7QUFBWCxLQUNFLG1CQURGLENBcEJGLEVBdUJFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRSxrQkFDRSxrQkFDRTtBQUFHLFFBQUksRUFBQztBQUFSLFlBREYsQ0FERixFQUlFLGtCQUNFO0FBQUcsUUFBSSxFQUFDO0FBQVIsYUFERixDQUpGLEVBT0Usa0JBQ0U7QUFBRyxRQUFJLEVBQUM7QUFBUixhQURGLENBUEYsRUFVRSxrQkFDQztBQUFHLFFBQUksRUFBQztBQUFSLGdCQURELENBVkYsQ0FERixDQXZCRixDQURGLENBREY7QUE0Q0QsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlDRDtBQUNlLFNBQVNFLE1BQVQsR0FBa0I7QUFDL0IsU0FDRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUssT0FBRyxFQUFFRixvRUFBSyxDQUFDLHNCQUFEO0FBQWYsSUFERixDQURGLEVBSUU7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRSxzQ0FERixFQUVFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRTtBQUFHLGFBQVMsRUFBQyxNQUFiO0FBQW9CLFFBQUksRUFBQztBQUF6QixLQUNFO0FBQUssT0FBRyxFQUFFQSxvRUFBSyxDQUFDLGlDQUFEO0FBQWYsSUFERixFQUVFLCtFQUZGLENBREYsRUFLRTtBQUFHLGFBQVMsRUFBQyxNQUFiO0FBQW9CLFFBQUksRUFBQztBQUF6QixLQUNFO0FBQUssT0FBRyxFQUFFQSxvRUFBSyxDQUFDLGlDQUFEO0FBQWYsSUFERixFQUVFLHdGQUZGLENBTEYsRUFZRTtBQUFHLGFBQVMsRUFBQyxNQUFiO0FBQW9CLFFBQUksRUFBQztBQUF6QixLQUNFO0FBQUssT0FBRyxFQUFFQSxvRUFBSyxDQUFDLCtCQUFEO0FBQWYsSUFERixFQUVFLDBFQUZGLENBWkYsRUFnQkU7QUFBRyxhQUFTLEVBQUMsTUFBYjtBQUFvQixRQUFJLEVBQUM7QUFBekIsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQywrQkFBRDtBQUFmLElBREYsRUFFRSwrRUFGRixDQWhCRixFQW9CRTtBQUFHLGFBQVMsRUFBQyxNQUFiO0FBQW9CLFFBQUksRUFBQztBQUF6QixLQUNFO0FBQUssT0FBRyxFQUFFQSxvRUFBSyxDQUFDLCtCQUFEO0FBQWYsSUFERixFQUVFLGtGQUZGLENBcEJGLEVBMEJFO0FBQUcsYUFBUyxFQUFDLE1BQWI7QUFBb0IsUUFBSSxFQUFDO0FBQXpCLEtBQ0U7QUFBSyxPQUFHLEVBQUVBLG9FQUFLLENBQUMsK0JBQUQ7QUFBZixJQURGLEVBRUUsbUVBRkYsQ0ExQkYsQ0FGRixDQURGLENBSkYsQ0FERjtBQTJDRCxDOzs7Ozs7Ozs7Ozs7Ozs7OztBQzdDRDtBQUVlLFNBQVNHLElBQVQsR0FBZ0I7QUFDN0IsU0FDRTtBQUFTLFNBQUssRUFBQztBQUFmLEtBQ0U7QUFBSyxTQUFLLEVBQUM7QUFBWCxLQUNFO0FBQUssU0FBSyxFQUFDO0FBQVgsS0FDRTtBQUFLLFNBQUssRUFBQztBQUFYLEtBQ0UsK0NBREYsRUFFRSw4SEFGRixDQURGLEVBUUU7QUFBTSx5QkFBa0I7QUFBeEIsS0FDRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFDRSwwQkFBbUIsRUFEckI7QUFFRSxRQUFJLEVBQUMsT0FGUDtBQUdFLGVBQVcsRUFBQyxxQkFIZDtBQUlFLGdCQUFZLEVBQUM7QUFKZixJQURGLEVBT0U7QUFBUSxRQUFJLEVBQUM7QUFBYixLQUNFLDhCQURGLENBUEYsQ0FERixDQVJGLENBREYsQ0FERixDQURGO0FBNEJELEM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9CYyxTQUFTQyxJQUFULEdBQWdCO0FBQzdCLFNBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFLDRGQURGLEVBSUUsc0ZBSkYsQ0FERjtBQVNELEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRWUsU0FBU0MsY0FBVCxDQUF3QkMsS0FBeEIsRUFBK0I7QUFDNUMsU0FDRSxtQkFDRSxNQUFDLHdFQUFELE9BREYsRUFFRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0UsTUFBQyw2RUFBRCxPQURGLEVBRUU7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUssT0FBRyxFQUFFTixvRUFBSyxDQUFDLDRCQUFEO0FBQWYsSUFERixDQUZGLEVBS0UsaUJBTEYsRUFNRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQyx5QkFBRDtBQUFmLElBREYsQ0FERixFQUlFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRSxpQkFDRSxtQ0FERixFQUVFLGlCQUZGLEVBR0UsbURBSEYsQ0FERixDQUpGLEVBV0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUcsUUFBSSxFQUFDO0FBQVIsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQywyQkFBRDtBQUFmLElBREYsQ0FERixFQUlFO0FBQUcsUUFBSSxFQUFDO0FBQVIsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQyx5QkFBRDtBQUFmLElBREYsQ0FKRixDQVhGLENBREYsRUFxQkU7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFLDZPQURGLEVBT0UsbU1BUEYsRUFZRSx5Q0FaRixFQWFFLGdSQWJGLEVBbUJFLDJIQW5CRixFQXVCRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBSyxPQUFHLEVBQUVBLG9FQUFLLENBQUMsOEJBQUQ7QUFBZixJQURGLEVBRUUsd0lBRkYsQ0F2QkYsRUE4QkUsbUxBOUJGLEVBbUNFLDRSQW5DRixFQTBDRSwyQ0ExQ0YsRUEyQ0Usa0JBQ0UsNENBREYsRUFFRSw2R0FGRixFQU1FLDJDQU5GLENBM0NGLEVBbURFLDRJQW5ERixFQXVERSx3Q0FFRSxpQkFGRixVQXZERixDQXJCRixFQWlGRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBSyxhQUFTLEVBQUM7QUFBZixLQUNFO0FBQUcsUUFBSSxFQUFDO0FBQVIsS0FDRTtBQUFLLE9BQUcsRUFBRUEsb0VBQUssQ0FBQywyQkFBRDtBQUFmLElBREYsRUFFRSx3Q0FGRixDQURGLEVBS0U7QUFBRyxRQUFJLEVBQUM7QUFBUixLQUNFO0FBQUssT0FBRyxFQUFFQSxvRUFBSyxDQUFDLHlCQUFEO0FBQWYsSUFERixFQUVFLHVDQUZGLENBTEYsQ0FERixFQVdFO0FBQUksYUFBUyxFQUFDO0FBQWQsY0FFRSxrQkFDRTtBQUFHLFFBQUksRUFBQyx5QkFBUjtBQUFrQyxTQUFLLEVBQUM7QUFBeEMsK0JBREYsQ0FGRixDQVhGLEVBbUJFO0FBQUssYUFBUyxFQUFDO0FBQWYsS0FDRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0U7QUFBSyxPQUFHLEVBQUVBLG9FQUFLLENBQUMseUJBQUQ7QUFBZixJQURGLENBREYsRUFJRTtBQUFLLGFBQVMsRUFBQztBQUFmLEtBQ0UsaUJBQ0UsdUNBREYsaUtBREYsQ0FKRixDQW5CRixDQWpGRixDQU5GLENBRkYsRUE0SEUsTUFBQyxnR0FBRCxPQTVIRixFQTZIRSxNQUFDLDhGQUFELE9BN0hGLEVBOEhFLE1BQUMsMkVBQUQsT0E5SEYsQ0FERjtBQWtJRCxDOzs7Ozs7Ozs7Ozs7QUMxSUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOztBQUVBLE1BQU1BLEtBQUssR0FBSU8sR0FBRCxJQUFTO0FBQ3JCO0FBQ0E7QUFFQSxNQUFJQyxZQUFZLEdBQUcsS0FBbkI7O0FBQ0EsTUFBSUMsa0RBQU0sQ0FBQ0MsV0FBUCxJQUFzQixZQUExQixFQUF3QztBQUN0Q0YsZ0JBQVksR0FBR0csMENBQVMsQ0FBQ0MsR0FBVixDQUFjQyxJQUE3QjtBQUNELEdBRkQsTUFFTyxJQUFJSixrREFBTSxDQUFDQyxXQUFQLElBQXNCLFNBQTFCLEVBQXFDO0FBQzFDRixnQkFBWSxHQUFHRywwQ0FBUyxDQUFDQyxHQUFWLENBQWNFLE9BQTdCO0FBQ0QsR0FGTSxNQUVBLElBQUlMLGtEQUFNLENBQUNDLFdBQVAsSUFBc0IsYUFBMUIsRUFBeUM7QUFDOUNGLGdCQUFZLEdBQUdHLDBDQUFTLENBQUNDLEdBQVYsQ0FBY0csR0FBN0I7QUFDRCxHQUZNLE1BRUE7QUFDTFAsZ0JBQVksR0FBRyxLQUFmO0FBQ0Q7O0FBRUQsTUFBSVEsZ0JBQWdCLEdBQUcsS0FBdkI7O0FBQ0EsTUFBSVIsWUFBWSxJQUFJLEtBQWhCLElBQXlCQyxrREFBTSxDQUFDUSxxQkFBcEMsRUFBMkQ7QUFDekRELG9CQUFnQixHQUFHLElBQW5CO0FBQ0Q7O0FBRUQsTUFBSVIsWUFBSixFQUFrQjtBQUNoQixXQUFPQyxrREFBTSxDQUFDUyx5QkFBUCxHQUFtQyxTQUFuQyxHQUErQ1gsR0FBdEQ7QUFDRCxHQUZELE1BRU87QUFDTCxRQUFJUyxnQkFBSixFQUFzQjtBQUNwQixhQUFPLE1BQU1QLGtEQUFNLENBQUNRLHFCQUFiLEdBQXFDVixHQUE1QztBQUNELEtBRkQsTUFFTztBQUNMLGFBQU9BLEdBQVA7QUFDRDtBQUNGO0FBQ0YsQ0E3QkQ7O0FBK0JlUCxvRUFBZixFOzs7Ozs7Ozs7Ozs7QUNqQ0E7QUFBQTtBQUFPLE1BQU1tQixnQkFBZ0IsR0FBRztBQUM5QkMsWUFBVSxFQUFFLFlBRGtCO0FBRTlCQyxTQUFPLEVBQUUsU0FGcUI7QUFHOUJDLGFBQVcsRUFBRTtBQUhpQixDQUF6QjtBQU1QLE1BQU1iLE1BQU0sR0FBRztBQUNiQyxhQUFXLEVBQUVhLE9BQU8sQ0FBQ0MsR0FBUixDQUFZQyxlQUFaLElBQStCLGFBRC9CO0FBRWJDLE1BQUksRUFBRTtBQUNKQyxTQUFLLEVBQUUsa0JBREg7QUFFSkMsZUFBVyxFQUFFLHVCQUZUO0FBR0pDLFFBQUksRUFBRTtBQUhGLEdBRk87QUFPYkMsT0FBSyxFQUFFO0FBQ0xDLGdCQUFZLEVBQUU7QUFEVCxHQVBNO0FBVWJDLFlBQVUsRUFBRSxxQkFWQztBQVdiO0FBQ0FDLHVCQUFxQixFQUFFVixPQUFPLENBQUNDLEdBQVIsQ0FBWVMscUJBQVosSUFBcUMsRUFaL0M7QUFZb0Q7QUFDakVDLHdCQUFzQixFQUFFWCxPQUFPLENBQUNDLEdBQVIsQ0FBWVUsc0JBQVosSUFBc0Msa0JBYmpEO0FBYXNFO0FBQ25GakIsdUJBQXFCLEVBQUVNLE9BQU8sQ0FBQ0MsR0FBUixDQUFZUCxxQkFBWixJQUFxQyxFQWQvQztBQWVia0IsMkJBQXlCLEVBQUVaLE9BQU8sQ0FBQ0MsR0FBUixDQUFZVyx5QkFBWixJQUF5QyxFQWZ2RDtBQWdCYmpCLDJCQUF5QixFQUFFSyxPQUFPLENBQUNDLEdBQVIsQ0FBWU4seUJBQVosSUFBeUMsRUFoQnZEO0FBaUJia0Isd0JBQXNCLEVBQUViLE9BQU8sQ0FBQ0MsR0FBUixDQUFZWSxzQkFBWixJQUFzQyxFQWpCakQ7QUFrQmJDLHNCQUFvQixFQUFFZCxPQUFPLENBQUNDLEdBQVIsQ0FBWWEsb0JBQVosSUFBb0MsRUFsQjdDO0FBbUJiO0FBQ0FDLGlCQUFlLEVBQUVmLE9BQU8sQ0FBQ0MsR0FBUixDQUFZYyxlQUFaLElBQStCLEVBcEJuQztBQXFCYkMsbUJBQWlCLEVBQUUscUJBckJOO0FBc0JiQyxxQkFBbUIsRUFBRWpCLE9BQU8sQ0FBQ0MsR0FBUixDQUFZZ0IsbUJBQVosSUFBbUMsRUF0QjNDOztBQXVCYixNQUFJQyxZQUFKLEdBQW1CO0FBQ2pCLFdBQVEsbUJBQVI7QUFDRCxHQXpCWTs7QUEwQmJDLGFBQVcsRUFBRSxNQUFNO0FBQ2pCLFdBQU9qQyxNQUFNLENBQUNRLHFCQUFQLEdBQStCLE1BQU1SLE1BQU0sQ0FBQ1EscUJBQTVDLEdBQW9FLEVBQTNFO0FBQ0QsR0E1Qlk7QUE2QmIwQixZQUFVLEVBQUUsTUFBTTtBQUNoQixXQUFPbEMsTUFBTSxDQUFDNEIsb0JBQVAsR0FBOEI1QixNQUFNLENBQUM0QixvQkFBckMsR0FBNEQsRUFBbkU7QUFDRCxHQS9CWTtBQWdDYk8sTUFBSSxFQUFHQSxJQUFELElBQVU7QUFDZCxXQUFPbkMsTUFBTSxDQUFDaUMsV0FBUCxLQUF1QkUsSUFBOUI7QUFDRDtBQWxDWSxDQUFmOztBQXFDQSxXQUFrQyxFQUFsQyxNQUdPLENBQ0w7QUFDRDs7QUFFY25DLHFFQUFmLEU7Ozs7Ozs7Ozs7O0FDbkRBLGtDIiwiZmlsZSI6InBhZ2VzL2FydGljbGVzLWRldGFpbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0gcmVxdWlyZSgnLi4vc3NyLW1vZHVsZS1jYWNoZS5qcycpO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHR2YXIgdGhyZXcgPSB0cnVlO1xuIFx0XHR0cnkge1xuIFx0XHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuIFx0XHRcdHRocmV3ID0gZmFsc2U7XG4gXHRcdH0gZmluYWxseSB7XG4gXHRcdFx0aWYodGhyZXcpIGRlbGV0ZSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0fVxuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vcGFnZXMvYXJ0aWNsZXMtZGV0YWlsLmpzXCIpO1xuIiwiaW1wb3J0IGFzc2V0IGZyb20gXCJwbHVnaW5zL2Fzc2V0cy9hc3NldFwiO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBGb290ZXIoKSB7XG4gIHJldHVybiAoXG4gICAgPGZvb3RlciBjbGFzc05hbWU9XCJzaXRlLWZvb3RlclwiPlxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJzbGlkZVwiPlxuICAgICAgICA8c3Bhbj5EaWdpdGFsIHByb2R1Y3QgZGVzaWduPC9zcGFuPlxuICAgICAgICA8c3Bhbj5SZW1vdGUgd29yazwvc3Bhbj5cbiAgICAgICAgPHNwYW4+VVggZGVzaWduPC9zcGFuPlxuICAgICAgICA8c3Bhbj5EaXN0cmlidXRlZCB0ZWFtczwvc3Bhbj5cbiAgICAgICAgPHNwYW4+Q3JlYXRpdml0eTwvc3Bhbj5cbiAgICAgICAgPHNwYW4+U3RyYXRlZ3k8L3NwYW4+XG4gICAgICAgIDxzcGFuPlN1c3BlbnNlPC9zcGFuPlxuICAgICAgICA8c3Bhbj5Hcm93dGg8L3NwYW4+XG4gICAgICA8L2Rpdj5cbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGVudFwiPlxuICAgICAgICAgIDxhIGhyZWY9XCIjXCI+XG4gICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvbG9nb19saWdodC5zdmdcIil9IC8+XG4gICAgICAgICAgPC9hPlxuICAgICAgICAgIDxwPlxuICAgICAgICAgICAgTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gRHVpcyBldVxuICAgICAgICAgICAgdmVsaXQgdGVtcHVzIGVyYXQgZWdlc3RhcyBlZmZpY2l0dXIuIEluIGhhYyBoYWJpdGFzc2UgcGxhdGVhXG4gICAgICAgICAgICBkaWN0dW1zdC4gRnVzY2UgYSBudW5jIGVnZXQgbGlndWxhIHN1c2NpcGl0IGZpbmlidXMuXG4gICAgICAgICAgPC9wPlxuICAgICAgICAgIDx1bCBjbGFzcz1cImFkZHJlc3NcIj5cbiAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgICBocmVmPVwiaHR0cHM6Ly90d2l0dGVyLmNvbS9taWthbWF0aWthaW5lblwiXG4gICAgICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcbiAgICAgICAgICAgICAgICByZWw9XCJub29wZW5lclwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICBUd2l0dGVyXG4gICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgIDxhXG4gICAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vd3d3LmxpbmtlZGluLmNvbS9pbi9taWthbWF0aWthaW5lblwiXG4gICAgICAgICAgICAgICAgdGFyZ2V0PVwiX2JsYW5rXCJcbiAgICAgICAgICAgICAgICByZWw9XCJub29wZW5lclwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICBMaW5rZWRJblxuICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgIGhyZWY9XCJodHRwczovL3d3dy5ub3JkaWNyb3NlLm5ldC9ibG9nL3Jzcy9cIlxuICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgcmVsPVwibm9vcGVuZXJcIlxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgUlNTXG4gICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgPC91bD5cbiAgICAgICAgICA8cCBjbGFzcz1cInNpdGUtZm9vdGVyX19ib3R0b20tdGV4dFwiPlxuICAgICAgICAgICAgwqkgMjAxMi0yMDIxIE5vcmRpYyBSb3NlIENvLlxuICAgICAgICAgICAgPGJyIC8+XG4gICAgICAgICAgICBBbGwgcmlnaHRzIHJlc2VydmVkLlxuICAgICAgICAgIDwvcD5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICA8L2Zvb3Rlcj5cbiAgKTtcbn1cbiIsImltcG9ydCBhc3NldCBmcm9tIFwicGx1Z2lucy9hc3NldHMvYXNzZXRcIjtcclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSGVhZGVyKCkge1xyXG4gIHJldHVybiAoXHJcbiAgICA8ZGl2IGNsYXNzTmFtZT1cImhlYWRlclwiPlxyXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lci1mbHVpZFwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibG9nb1wiPlxyXG4gICAgICAgICAgPGEgaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL2xvZ29fZGFyay5zdmdcIil9IC8+XHJcbiAgICAgICAgICA8L2E+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPHVsIGNsYXNzTmFtZT1cIm5hdlwiPlxyXG4gICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL1wiPkJMT0c8L2E+XHJcbiAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL2Fib3V0XCI+QUJPVVQ8L2E+XHJcbiAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL2xpbmtzXCI+TElOS1M8L2E+XHJcbiAgICAgICAgICA8L2xpPlxyXG4gICAgICAgICAgPGxpPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL3Byb2plY3RzXCI+UFJPSkVDVFM8L2E+XHJcbiAgICAgICAgICA8L2xpPlxyXG4gICAgICAgIDwvdWw+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImJ0bm1lbnVcIj5cclxuICAgICAgICAgIDxzcGFuPjwvc3Bhbj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8bmF2IGNsYXNzTmFtZT1cIm1lbnUtbW9iaWxlXCI+XHJcbiAgICAgICAgICA8dWw+XHJcbiAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICA8YSBocmVmPVwiL1wiPkJMT0c8L2E+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICA8YSBocmVmPVwiL2Fib3V0XCI+QUJPVVQ8L2E+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICA8YSBocmVmPVwiL2xpbmtzXCI+TElOS1M8L2E+XHJcbiAgICAgICAgICAgIDwvbGk+XHJcbiAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgIDxhIGhyZWY9XCIvcHJvamVjdHNcIj5QUk9KRUNUUzwvYT5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgPC9uYXY+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgKTtcclxufVxyXG4iLCJpbXBvcnQgYXNzZXQgZnJvbSBcInBsdWdpbnMvYXNzZXRzL2Fzc2V0XCI7XHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEJvdHRvbSgpIHtcclxuICByZXR1cm4gKFxyXG4gICAgPGRpdiBjbGFzc05hbWU9XCJyZWFkXCI+XHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmVhZF9fZXllXCI+XHJcbiAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL2V5ZS5wbmdcIil9IC8+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicmVhZC1uZXh0XCI+XHJcbiAgICAgICAgICA8aDI+V2hhdCB0byByZWFkIG5leHQ8L2gyPlxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJyZWFkLXdyYXBcIj5cclxuICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiaXRlbVwiIGhyZWY9XCIvYXJ0aWNsZXMtZGV0YWlsXCI+XHJcbiAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL1JlY3RhbmdsZSAxMi0xLnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICA8cD5IZXJlIGFyZSBzb21lIHRoaW5ncyB5b3Ugc2hvdWxkIGtub3cgcmVnYXJkaW5nIGhvdyB3ZSB3b3JrIDwvcD5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJpdGVtXCIgaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvUmVjdGFuZ2xlIDEzLTEucG5nXCIpfSAvPlxyXG4gICAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgICAgR3Jhbm55IGdpdmVzIGV2ZXJ5b25lIHRoZSBmaW5nZXIsIGFuZCBvdGhlciB0aXBzIGZyb20gT0ZGRlxyXG4gICAgICAgICAgICAgICAgQmFyY2Vsb25hXHJcbiAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIml0ZW1cIiBocmVmPVwiL2FydGljbGVzLWRldGFpbC5qc1wiPlxyXG4gICAgICAgICAgICAgIDxpbWcgc3JjPXthc3NldChcIi9pbWFnZXMvaG9tZS9SZWN0YW5nbGUgMTMucG5nXCIpfSAvPlxyXG4gICAgICAgICAgICAgIDxwPkhlbGxvIHdvcmxkLCBvciwgaW4gb3RoZXIgd29yZHMsIHdoeSB0aGlzIGJsb2cgZXhpc3RzIDwvcD5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJpdGVtXCIgaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvUmVjdGFuZ2xlIDEyLnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICA8cD5IZXJlIGFyZSBzb21lIHRoaW5ncyB5b3Ugc2hvdWxkIGtub3cgcmVnYXJkaW5nIGhvdyB3ZSB3b3JrIDwvcD5cclxuICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJpdGVtXCIgaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvUmVjdGFuZ2xlIDE0LnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICA8cD5cclxuICAgICAgICAgICAgICAgIENvbm5lY3RpbmcgYXJ0aWZpY2lhbCBpbnRlbGxpZ2VuY2Ugd2l0aCBkaWdpdGFsIHByb2R1Y3QgZGVzaWduXHJcbiAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIml0ZW1cIiBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICAgIDxpbWcgc3JjPXthc3NldChcIi9pbWFnZXMvaG9tZS9SZWN0YW5nbGUgMTUucG5nXCIpfSAvPlxyXG4gICAgICAgICAgICAgIDxwPkl04oCZcyBhbGwgYWJvdXQgZmluZGluZyB0aGUgcGVyZmVjdCBiYWxhbmNlPC9wPlxyXG4gICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9kaXY+XHJcbiAgICA8L2Rpdj5cclxuICApO1xyXG59XHJcbiIsImltcG9ydCBSZWFjdCBmcm9tIFwicmVhY3RcIjtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEZvcm0oKSB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxzZWN0aW9uIGNsYXNzPVwiZm9ybVwiPlxyXG4gICAgICA8ZGl2IGNsYXNzPVwiY29udGFpbmVyXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImZvcm1fX3dyYXBcIj5cclxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJmb3JtLXRleHRcIj5cclxuICAgICAgICAgICAgPGgyPlNpZ24gdXAgZm9yIHRoZSBuZXdzbGV0dGVyPC9oMj5cclxuICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgSWYgeW91IHdhbnQgcmVsZXZhbnQgdXBkYXRlcyBvY2Nhc2lvbmFsbHksIHNpZ24gdXAgZm9yIHRoZSBwcml2YXRlXHJcbiAgICAgICAgICAgICAgbmV3c2xldHRlci4gWW91ciBlbWFpbCBpcyBuZXZlciBzaGFyZWQuXHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGZvcm0gZGF0YS1tZW1iZXJzLWZvcm09XCJzdWJzY3JpYmVcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmb3JtLWlucHV0XCI+XHJcbiAgICAgICAgICAgICAgPGlucHV0XHJcbiAgICAgICAgICAgICAgICBkYXRhLW1lbWJlcnMtZW1haWw9XCJcIlxyXG4gICAgICAgICAgICAgICAgdHlwZT1cImVtYWlsXCJcclxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRW50ZXIgeW91ciBlbWFpbC4uLlwiXHJcbiAgICAgICAgICAgICAgICBhdXRvY29tcGxldGU9XCJmYWxzZVwiXHJcbiAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJzdWJtaXRcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuPlNpZ24gdXA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPC9mb3JtPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvc2VjdGlvbj5cclxuICApO1xyXG59XHJcbiIsImV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIFRleHQoKSB7XHJcbiAgcmV0dXJuIChcclxuICAgIDxkaXYgY2xhc3NOYW1lPVwidGV4dFwiPlxyXG4gICAgICA8aDE+XHJcbiAgICAgICAgQSBmZXcgd29yZHMgYWJvdXQgdGhpcyBibG9nIHBsYXRmb3JtLCBHaG9zdCwgYW5kIGhvdyB0aGlzIHNpdGUgd2FzIG1hZGVcclxuICAgICAgPC9oMT5cclxuICAgICAgPHA+V2h5IEdob3N0ICgmIEZpZ21hKSBpbnN0ZWFkIG9mIE1lZGl1bSwgV29yZFByZXNzIG9yIG90aGVyIG9wdGlvbnM/PC9wPlxyXG4gICAgICBcclxuICAgIDwvZGl2PlxyXG4gICk7XHJcbn1cclxuIiwiaW1wb3J0IFRleHQgZnJvbSBcIkAvY29tcG9uZW50cy93ZWJzaXRlL3NlY3Rpb24vaGVyby9UZXh0XCI7XHJcbmltcG9ydCBIZWFkZXIgZnJvbSBcImNvbXBvbmVudHMvd2Vic2l0ZS9oZWFkZXIvSGVhZGVyXCI7XHJcbmltcG9ydCBhc3NldCBmcm9tIFwicGx1Z2lucy9hc3NldHMvYXNzZXRcIjtcclxuaW1wb3J0IEJvdHRvbSBmcm9tIFwiQC9jb21wb25lbnRzL3dlYnNpdGUvc2VjdGlvbi9ib3R0b20tYXJ0aWNsZS1kZXRhaWwvQm90dG9tXCI7XHJcbmltcG9ydCBGb3JtIGZyb20gXCJAL2NvbXBvbmVudHMvd2Vic2l0ZS9zZWN0aW9uL2JvdHRvbS1hcnRpY2xlLWRldGFpbC9Gb3JtXCI7XHJcbmltcG9ydCBGb290ZXIgZnJvbSBcIkAvY29tcG9uZW50cy93ZWJzaXRlL2VsZW1lbnRzL0Zvb3RlclwiO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gYXJ0aWNsZXNkZXRhaWwocHJvcHMpIHtcclxuICByZXR1cm4gKFxyXG4gICAgPGRpdj5cclxuICAgICAgPEhlYWRlcj48L0hlYWRlcj5cclxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cclxuICAgICAgICA8VGV4dD48L1RleHQ+XHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmaWd1cmVcIj5cclxuICAgICAgICAgIDxpbWcgc3JjPXthc3NldChcIi9pbWFnZXMvaG9tZS9pbWFnZSAyLjEucG5nXCIpfSAvPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxocj48L2hyPlxyXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYmxvZ1wiPlxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJibG9nX19tZXRhXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYXZhdGFyXCI+XHJcbiAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL2F2dGFydC5wbmdcIil9IC8+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImluZm9cIj5cclxuICAgICAgICAgICAgICA8cD5cclxuICAgICAgICAgICAgICAgIDxiPk1pa2EgTUFUSUtBSU5FTjwvYj5cclxuICAgICAgICAgICAgICAgIDxicj48L2JyPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4+QXByIDE1LCAyMDIwIMK3IDQgbWluIHJlYWQ8L3NwYW4+XHJcbiAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidG4tc2hhcmVcIj5cclxuICAgICAgICAgICAgICA8YSBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL2ZhY2Vib29rLnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPGEgaHJlZj1cIiNcIj5cclxuICAgICAgICAgICAgICAgIDxpbWcgc3JjPXthc3NldChcIi9pbWFnZXMvaG9tZS90d2l0ZXIucG5nXCIpfSAvPlxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYmxvZ19fZGVzY3JpcHRpb25cIj5cclxuICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgTG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC4gRHVpcyBldVxyXG4gICAgICAgICAgICAgIHZlbGl0IHRlbXB1cyBlcmF0IGVnZXN0YXMgZWZmaWNpdHVyLiBJbiBoYWMgaGFiaXRhc3NlIHBsYXRlYVxyXG4gICAgICAgICAgICAgIGRpY3R1bXN0LiBGdXNjZSBhIG51bmMgZWdldCBsaWd1bGEgc3VzY2lwaXQgZmluaWJ1cy4gQWVuZWFuXHJcbiAgICAgICAgICAgICAgcGhhcmV0cmEgcXVpcyBsYWN1cyBhdCB2aXZlcnJhLlxyXG4gICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgIENsYXNzIGFwdGVudCB0YWNpdGkgc29jaW9zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhXHJcbiAgICAgICAgICAgICAgbm9zdHJhLCBwZXIgaW5jZXB0b3MgaGltZW5hZW9zLiBBbGlxdWFtIHF1aXMgcG9zdWVyZSBsaWd1bGEuIEluIGV1XHJcbiAgICAgICAgICAgICAgZHVpIG1vbGVzdGllLCBtb2xlc3RpZSBsZWN0dXMgZXUsIHNlbXBlciBsZWN0dXMuXHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgPGgzPk5leHQgb24gdGhlIHBpcGVsaW5lPC9oMz5cclxuICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgRHVpcyBldSB2ZWxpdCB0ZW1wdXMgZXJhdCBlZ2VzdGFzIGVmZmljaXR1ci4gSW4gaGFjIGhhYml0YXNzZVxyXG4gICAgICAgICAgICAgIHBsYXRlYSBkaWN0dW1zdC4gRnVzY2UgYSBudW5jIGVnZXQgbGlndWxhIHN1c2NpcGl0IGZpbmlidXMuIEFlbmVhblxyXG4gICAgICAgICAgICAgIHBoYXJldHJhIHF1aXMgbGFjdXMgYXQgdml2ZXJyYS4gQ2xhc3MgYXB0ZW50IHRhY2l0aSBzb2Npb3NxdSBhZFxyXG4gICAgICAgICAgICAgIGxpdG9yYSB0b3JxdWVudCBwZXIgY29udWJpYSBub3N0cmEsIHBlciBpbmNlcHRvcyBoaW1lbmFlb3MuXHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgTW9yYmkgZWZmaWNpdHVyIGF1Y3RvciBtZXR1cywgaWQgbW9sbGlzIGxvcmVtIHBlbGxlbnRlc3F1ZSBpZC5cclxuICAgICAgICAgICAgICBOdWxsYW0gcG9zdWVyZSBtYXhpbXVzIGR1aSBldCBmcmluZ2lsbGEuXHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJpbWdcIj5cclxuICAgICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvUmVjdGFuZ2xlIDgucG5nXCIpfSAvPlxyXG4gICAgICAgICAgICAgIDxmaWdjYXB0aW9uPlxyXG4gICAgICAgICAgICAgICAgSW1hZ2UgY2FwdGlvbiBjZW50ZXJlZCB0aGlzIHdheSBhbmQgSeKAmWxsIG1ha2UgdGhpcyBhIGJpdCBsb25nZXJcclxuICAgICAgICAgICAgICAgIHRvIGluZGljYXRlIHRoZSBhbW91bnQgb2YgbGluZS1oZWlnaHQuXHJcbiAgICAgICAgICAgICAgPC9maWdjYXB0aW9uPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgQWVuZWFuIHBoYXJldHJhIHF1aXMgbGFjdXMgYXQgdml2ZXJyYS4gQ2xhc3MgYXB0ZW50IHRhY2l0aVxyXG4gICAgICAgICAgICAgIHNvY2lvc3F1IGFkIGxpdG9yYSB0b3JxdWVudCBwZXIgY29udWJpYSBub3N0cmEsIHBlciBpbmNlcHRvc1xyXG4gICAgICAgICAgICAgIGhpbWVuYWVvcy4gQWxpcXVhbSBxdWlzIHBvc3VlcmUgbGlndWxhLlxyXG4gICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgIEluIGV1IGR1aSBtb2xlc3RpZSwgbW9sZXN0aWUgbGVjdHVzIGV1LCBzZW1wZXIgbGVjdHVzLiBQcm9pbiBhdFxyXG4gICAgICAgICAgICAgIGp1c3RvIGxhY2luaWEsIGF1Y3RvciBuaXNsIGV0LCBjb25zZXF1YXQgYW50ZS4gRG9uZWMgc2l0IGFtZXQgbmlzaVxyXG4gICAgICAgICAgICAgIGFyY3UuIE1vcmJpIGVmZmljaXR1ciBhdWN0b3IgbWV0dXMsIGlkIG1vbGxpcyBsb3JlbSBwZWxsZW50ZXNxdWVcclxuICAgICAgICAgICAgICBpZC4gTnVsbGFtIHBvc3VlcmUgbWF4aW11cyBkdWkgZXQgZnJpbmdpbGxhLiBOdWxsYSBub24gdm9sdXRwYXRcclxuICAgICAgICAgICAgICBsZW8uXHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgPHA+QSBsaXN0IGxvb2tzIGxpa2UgdGhpczo8L3A+XHJcbiAgICAgICAgICAgIDx1bD5cclxuICAgICAgICAgICAgICA8bGk+Rmlyc3QgaXRlbSBpbiB0aGUgbGlzdCA8L2xpPlxyXG4gICAgICAgICAgICAgIDxsaT5cclxuICAgICAgICAgICAgICAgIFNlY29uZCBpdGVtIGluIHRoZSBsaXN0IGxvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0IG51bmMgZmVsaXNcclxuICAgICAgICAgICAgICAgIGRvbG9yIGxvcmVtIGlwc3VtIHNpdCBhbWV0XHJcbiAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgICA8bGk+VGhpcmQgaXRlbSBpbiB0aGUgbGlzdDwvbGk+XHJcbiAgICAgICAgICAgIDwvdWw+XHJcbiAgICAgICAgICAgIDxwPlxyXG4gICAgICAgICAgICAgIENsYXNzIGFwdGVudCB0YWNpdGkgc29jaW9zcXUgYWQgbGl0b3JhIHRvcnF1ZW50IHBlciBjb251YmlhXHJcbiAgICAgICAgICAgICAgbm9zdHJhLCBwZXIgaW5jZXB0b3MgaGltZW5hZW9zLiBBbGlxdWFtIHF1aXMgcG9zdWVyZSBsaWd1bGEuXHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgVGhhbmtzIGZvciByZWFkaW5nLFxyXG4gICAgICAgICAgICAgIDxiciAvPiBNaWthXHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJibG9nX19ib3R0b21cIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJib3R0b20tc2hhcmVcIj5cclxuICAgICAgICAgICAgICA8YSBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL2ZhY2Vib29rLnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICAgIDxzcGFuPlNoYXJlIG9uIEZhY2Vib29rPC9zcGFuPlxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICA8YSBocmVmPVwiI1wiPlxyXG4gICAgICAgICAgICAgICAgPGltZyBzcmM9e2Fzc2V0KFwiL2ltYWdlcy9ob21lL3R3aXRlci5wbmdcIil9IC8+XHJcbiAgICAgICAgICAgICAgICA8c3Bhbj5TaGFyZSBvbiBUd2l0dGVyPC9zcGFuPlxyXG4gICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJ0YWdzXCI+XHJcbiAgICAgICAgICAgICAgVGFnczpcclxuICAgICAgICAgICAgICA8bGk+XHJcbiAgICAgICAgICAgICAgICA8YSBocmVmPVwiL3RhZy9kaXN0cmlidXRlZC10ZWFtcy9cIiB0aXRsZT1cImRpc3RyaWJ1dGVkIHRlYW1zXCI+XHJcbiAgICAgICAgICAgICAgICAgIHByb2R1Y3QgZGVzaWduLCBjdWx0dXJlXHJcbiAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgPC9saT5cclxuICAgICAgICAgICAgPC91bD5cclxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJhdXRob3JcIj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImF1dGhvcl9faW1nXCI+XHJcbiAgICAgICAgICAgICAgICA8aW1nIHNyYz17YXNzZXQoXCIvaW1hZ2VzL2hvbWUvYXZ0YXJ0LnBuZ1wiKX0gLz5cclxuICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImF1dGhvcl9faW5mb1wiPlxyXG4gICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICAgIDxzcGFuPk1pa2EgTWF0aWthaW5lbiA8L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgIGlzIGEgRGVzaWduIEZvdW5kZXIgJiBBZHZpc29yLCBCZXJsaW4gU2Nob29sIG9mIENyZWF0aXZlXHJcbiAgICAgICAgICAgICAgICAgIExlYWRlcnNoaXAgRXhlY3V0aXZlIE1CQSBwYXJ0aWNpcGFudCwgWmlwcGllIGFkdmlzb3IsIFdvbHRcclxuICAgICAgICAgICAgICAgICAgY28tZm91bmRlciwgYW5kIE5vcmRpYyBSb3NlIHN0YWtlaG9sZGVyLlxyXG4gICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgPEJvdHRvbT48L0JvdHRvbT5cclxuICAgICAgPEZvcm0+PC9Gb3JtPlxyXG4gICAgICA8Rm9vdGVyPjwvRm9vdGVyPlxyXG4gICAgPC9kaXY+XHJcbiAgKTtcclxufVxyXG4iLCJpbXBvcnQgQ09ORklHIGZyb20gXCJ3ZWIuY29uZmlnXCI7XG5pbXBvcnQgZnJhbWV3b3JrIGZyb20gXCJkaWdpbmV4dC5qc29uXCI7XG5cbmNvbnN0IGFzc2V0ID0gKHNyYykgPT4ge1xuICAvLyBjb25zb2xlLmxvZyhDT05GSUcuTkVYVF9QVUJMSUNfQ0ROX0JBU0VfUEFUSCk7XG4gIC8vIGNvbnNvbGUubG9nKGZyYW1ld29yayk7XG5cbiAgbGV0IGlzRW5hYmxlZENETiA9IGZhbHNlO1xuICBpZiAoQ09ORklHLmVudmlyb25tZW50ID09IFwicHJvZHVjdGlvblwiKSB7XG4gICAgaXNFbmFibGVkQ0ROID0gZnJhbWV3b3JrLmNkbi5wcm9kO1xuICB9IGVsc2UgaWYgKENPTkZJRy5lbnZpcm9ubWVudCA9PSBcInN0YWdpbmdcIikge1xuICAgIGlzRW5hYmxlZENETiA9IGZyYW1ld29yay5jZG4uc3RhZ2luZztcbiAgfSBlbHNlIGlmIChDT05GSUcuZW52aXJvbm1lbnQgPT0gXCJkZXZlbG9wbWVudFwiKSB7XG4gICAgaXNFbmFibGVkQ0ROID0gZnJhbWV3b3JrLmNkbi5kZXY7XG4gIH0gZWxzZSB7XG4gICAgaXNFbmFibGVkQ0ROID0gZmFsc2U7XG4gIH1cblxuICBsZXQgaXNFbmFibGVCYXNlUGF0aCA9IGZhbHNlO1xuICBpZiAoaXNFbmFibGVkQ0ROID09IGZhbHNlICYmIENPTkZJRy5ORVhUX1BVQkxJQ19CQVNFX1BBVEgpIHtcbiAgICBpc0VuYWJsZUJhc2VQYXRoID0gdHJ1ZTtcbiAgfVxuXG4gIGlmIChpc0VuYWJsZWRDRE4pIHtcbiAgICByZXR1cm4gQ09ORklHLk5FWFRfUFVCTElDX0NETl9CQVNFX1BBVEggKyBcIi9wdWJsaWNcIiArIHNyYztcbiAgfSBlbHNlIHtcbiAgICBpZiAoaXNFbmFibGVCYXNlUGF0aCkge1xuICAgICAgcmV0dXJuIFwiL1wiICsgQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfUEFUSCArIHNyYztcbiAgICB9IGVsc2Uge1xuICAgICAgcmV0dXJuIHNyYztcbiAgICB9XG4gIH1cbn07XG5cbmV4cG9ydCBkZWZhdWx0IGFzc2V0O1xuIiwiXG5leHBvcnQgY29uc3QgRU5WSVJPTk1FTlRfREFUQSA9IHtcbiAgUFJPRFVDVElPTjogXCJwcm9kdWN0aW9uXCIsXG4gIFNUQUdJTkc6IFwic3RhZ2luZ1wiLFxuICBERVZFTE9QTUVOVDogXCJkZXZlbG9wbWVudFwiLFxufVxuXG5jb25zdCBDT05GSUcgPSB7XG4gIGVudmlyb25tZW50OiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19FTlYgfHwgXCJkZXZlbG9wbWVudFwiLFxuICBzaXRlOiB7XG4gICAgdGl0bGU6IFwiRGlnaW5leHQgV2Vic2l0ZVwiLFxuICAgIGRlc2NyaXB0aW9uOiBcIkRlc2NyaXB0aW9uIGdvZXMgaGVyZVwiLFxuICAgIHR5cGU6IFwiYXJ0aWNsZVwiLFxuICB9LFxuICBsaW5rczoge1xuICAgIGZhY2Vib29rUGFnZTogXCJcIixcbiAgfSxcbiAgZGF0ZUZvcm1hdDogXCJ5eXl5LU1NLWRkIEhIOm1tOnNzXCIsXG4gIC8vIHRoZXNlIHZhcmlhYmxlcyBjYW4gYmUgZXhwb3NlZCB0byBmcm9udC1lbmQ6XG4gIE5FWFRfUFVCTElDX0ZCX0FQUF9JRDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfRkJfQVBQX0lEIHx8IFwiXCIsICAvLyBjdXJyZW50bHkgdXNpbmcgWFhYXG4gIE5FWFRfUFVCTElDX0ZCX1BBR0VfSUQ6IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0ZCX1BBR0VfSUQgfHwgXCIxMTYyMjE0MDQwNTMyOTAyXCIsICAvLyBjdXJyZW50bHkgdXNpbmcgRGlnaXRvcCBkZXZlbG9wZXJzXG4gIE5FWFRfUFVCTElDX0JBU0VfUEFUSDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfQkFTRV9QQVRIIHx8IFwiXCIsXG4gIE5FWFRfUFVCTElDX0FQSV9CQVNFX1BBVEg6IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0FQSV9CQVNFX1BBVEggfHwgXCJcIixcbiAgTkVYVF9QVUJMSUNfQ0ROX0JBU0VfUEFUSDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfQ0ROX0JBU0VfUEFUSCB8fCBcIlwiLFxuICBORVhUX1BVQkxJQ19BUFBfRE9NQUlOOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19BUFBfRE9NQUlOIHx8IFwiXCIsXG4gIE5FWFRfUFVCTElDX0JBU0VfVVJMOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19CQVNFX1VSTCB8fCBcIlwiLFxuICAvLyBzb21lIHNlY3JldCBrZXlzIHdoaWNoIHdvbid0IGJlIGV4cG9zZWQgdG8gZnJvbnQtZW5kOlxuICBTT01FX1NFQ1JFVF9LRVk6IHByb2Nlc3MuZW52LlNPTUVfU0VDUkVUX0tFWSB8fCBcIlwiLFxuICBJUk9OX1NFU1NJT05fTkFNRTogXCJESUdJTkVYVEFETUlOQ09PS0lFXCIsXG4gIElST05fU0VTU0lPTl9TRUNSRVQ6IHByb2Nlc3MuZW52LklST05fU0VTU0lPTl9TRUNSRVQgfHwgXCJcIixcbiAgZ2V0IFNFU1NJT05fTkFNRSgpIHtcbiAgICByZXR1cm4gYERJR0lORVhUQVBQQ09PS0lFYDtcbiAgfSxcbiAgZ2V0QmFzZVBhdGg6ICgpID0+IHtcbiAgICByZXR1cm4gQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfUEFUSCA/IFwiL1wiICsgQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfUEFUSCA6IFwiXCI7XG4gIH0sXG4gIGdldEJhc2VVcmw6ICgpID0+IHtcbiAgICByZXR1cm4gQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfVVJMID8gQ09ORklHLk5FWFRfUFVCTElDX0JBU0VfVVJMIDogXCJcIjtcbiAgfSxcbiAgcGF0aDogKHBhdGgpID0+IHtcbiAgICByZXR1cm4gQ09ORklHLmdldEJhc2VQYXRoKCkgKyBwYXRoO1xuICB9LFxufTtcblxuaWYgKHR5cGVvZiB3aW5kb3cgIT0gXCJ1bmRlZmluZWRcIikge1xuICB3aW5kb3cuX19jb25maWdfXyA9IENPTkZJRztcbiAgLy8gY29uc29sZS5sb2coQ09ORklHKTtcbn0gZWxzZSB7XG4gIC8vIGNvbnNvbGUubG9nKENPTkZJRyk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IENPTkZJRztcbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0XCIpOyJdLCJzb3VyY2VSb290IjoiIn0=