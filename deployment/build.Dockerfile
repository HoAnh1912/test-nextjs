# Base on offical Node.js Alpine image
# FROM node:alpine
FROM mhart/alpine-node:latest

# RUN apk add --update g++ make python3
RUN apk add --no-cache python2 make g++

# Set working directory
WORKDIR /usr/app

# Copy package.json and package-lock.json before other files
# Utilise Docker cache to save re-installing dependencies if unchanged
COPY ./package*.json ./

# Install dependencies
RUN yarn install --production=true

# # Copy all files
# COPY ./ ./

# # Delete unnecessary files
# RUN rm -rf ./pages/
# RUN rm -rf ./plugins/
# RUN rm -rf ./components/
# RUN rm -rf ./.next/
# RUN rm -rf ./next.config.js
# RUN rm -rf ./diginext.json
# RUN rm -rf ./.env.local
# RUN rm -rf ./.env
# RUN rm -rf ./package.json
# RUN rm -rf ./package-lock.json

# Build app
# RUN npm run build
# RUN yarn build

# Expose the listening port
# EXPOSE 3000

# Run container as non-root (unprivileged) user
# The node user is provided in the Node.js Alpine base image
# USER node

# Run npm start script when container starts
# CMD [ "npm", "start" ]
# CMD [ "yarn", "start" ]