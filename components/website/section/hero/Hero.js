import asset from "plugins/assets/asset";
import Text from "./Text";

export default function Hero() {
  return (
    <div className="hero">
      <div className="container">
        <div className="img">
          <a href="#">
            <img src={asset("/images/home/image-section1.jpg")} />
          </a>
        </div>
        <Text></Text>
        <hr></hr>
      </div>
    </div>
  );
}
