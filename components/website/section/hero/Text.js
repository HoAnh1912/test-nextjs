export default function Text() {
  return (
    <div className="text">
      <h1>
        A few words about this blog platform, Ghost, and how this site was made
      </h1>
      <p>Why Ghost (& Figma) instead of Medium, WordPress or other options?</p>
      
    </div>
  );
}
