import asset from "plugins/assets/asset";
export default function Bottom() {
  return (
    <div className="read">
      <div className="read__eye">
        <img src={asset("/images/home/eye.png")} />
      </div>
      <div className="container">
        <div className="read-next">
          <h2>What to read next</h2>
          <div className="read-wrap">
            <a className="item" href="/articles-detail">
              <img src={asset("/images/home/Rectangle 12-1.png")} />
              <p>Here are some things you should know regarding how we work </p>
            </a>
            <a className="item" href="#">
              <img src={asset("/images/home/Rectangle 13-1.png")} />
              <p>
                Granny gives everyone the finger, and other tips from OFFF
                Barcelona
              </p>
            </a>
            <a className="item" href="/articles-detail.js">
              <img src={asset("/images/home/Rectangle 13.png")} />
              <p>Hello world, or, in other words, why this blog exists </p>
            </a>
            <a className="item" href="#">
              <img src={asset("/images/home/Rectangle 12.png")} />
              <p>Here are some things you should know regarding how we work </p>
            </a>
            <a className="item" href="#">
              <img src={asset("/images/home/Rectangle 14.png")} />
              <p>
                Connecting artificial intelligence with digital product design
              </p>
            </a>
            <a className="item" href="#">
              <img src={asset("/images/home/Rectangle 15.png")} />
              <p>It’s all about finding the perfect balance</p>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
