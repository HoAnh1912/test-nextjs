const FONTS = {
  ComfortaaRegular: "Comfortaa-Regular",
  ComfortaaLight: "Comfortaa-Light",
  AvenirNextLTProBold: "AvenirNextLTPro-Bold",
  AvenirNextLTProRegular: "AvenirNextLTPro-Regular",
  // AvenirNextLTProCn: "AvenirNextLTPro-Cn",
  AvenirNextLTProDemi: "AvenirNextLTPro-Demi",
};
export default FONTS;
