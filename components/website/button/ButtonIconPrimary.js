import asset from "plugins/assets/asset";
import { COLOR, LOGO, FONTS } from "components/website/constants/Constants";

export default function ButtonPrimary({
    text = "Name button",
    handleOutSite,
    children,
    fontSize = "inherit",
    backgroundColor,
    textColor,
    icon = asset("/images/icon-arrow-black.png"),
    width,
    fonFamily = FONTS.AvenirNextLTProDemi,
    borderColor = COLOR.BLACK,
    backgroundAfter = COLOR.BLACK,
}) {

    const color = COLOR;

    const handleClick = () => {
        if (handleOutSite) {
            handleOutSite();
        }
    }

    return <p className="btnPrimary" onClick={handleClick}>
        <p className="text">{ text }</p>
        <i className="btnEffect"></i>
        <style jsx>{`
            .btnPrimary{
                position: relative;
                &::after{
                    content: "";
                    position: absolute;
                    width: 100%;
                    height: 100%;
                    border-radius: 23px;
                    background-color: ${backgroundAfter};
                    left: 3.5px;
                    top: 3.5px;
                    z-index: 1;
                }
            }
            .text{
                position: relative;
                background-color: ${backgroundColor ? backgroundColor : color.WHITE};
                border-radius: 23px;
                border: 1px solid ${borderColor};
                color: ${textColor ? textColor : color.BLACK};
                padding: 13px 25px;
                padding-right: 47px;
                cursor: pointer;
                /* overflow: hidden; */
                transition: 0.3s;
                display: inline-block;
                font-size: ${fontSize};
                width: ${width ?  width : "auto"};
                font-family: ${fonFamily};
                z-index: 2;
                display: block;
                font-weight: 500;
            }
            .btnEffect{
                    display: block;
                    position: absolute;
                    background-image: url(${icon});
                    background-position: center;
                    background-size: 100%;
                    background-repeat: no-repeat;
                    transition: 0.3s;
                    width: 16px;
                    height: 14px;
                    right: 20px;
                    top: 50%;
                    transform: translate(0,-50%);
                    z-index: 2;
                }
            .text:hover{
                /* padding: 13px 25px; */
                padding-left: 36px;
                padding-right: 36px;
            }
            .btnPrimary:hover >.btnEffect{
                transform: translate(100%, -50%);
                opacity: 0;
            }
        `}</style>
    </p>
}