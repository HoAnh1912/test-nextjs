import asset from "plugins/assets/asset";
export default function Header() {
  return (
    <div className="header">
      <div className="container-fluid">
        <div className="logo">
          <a href="#">
            <img src={asset("/images/home/logo_dark.svg")} />
          </a>
        </div>
        <ul className="nav">
          <li>
            <a href="/">BLOG</a>
          </li>
          <li>
            <a href="/about">ABOUT</a>
          </li>
          <li>
            <a href="/links">LINKS</a>
          </li>
          <li>
            <a href="/projects">PROJECTS</a>
          </li>
        </ul>
        <div class="btnmenu">
          <span></span>
        </div>
        <nav className="menu-mobile">
          <ul>
            <li>
              <a href="/">BLOG</a>
            </li>
            <li>
              <a href="/about">ABOUT</a>
            </li>
            <li>
              <a href="/links">LINKS</a>
            </li>
            <li>
             <a href="/projects">PROJECTS</a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
}
