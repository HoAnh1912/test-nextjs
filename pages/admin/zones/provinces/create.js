import { useEffect, useRef, useState } from "react";
import { useRouter } from "next/router";
import { Switch, Divider, Select } from "antd";
//
import BackButton from "components/admin/BackButton";
import LayoutPage from "components/admin/LayoutPage";
import Section from "@/diginext/containers/Section";
import PageHeader from "@/dashkit/PageHeader";
import AdminButton, { ButtonSize } from "@/dashkit/Buttons";
import { Input, ValidationType, InputType } from "@/diginext/form/Form";
import { HorizontalList, ListItem, ListItemSize } from "@/diginext/layout/ListLayout";
import { showMessages, showSuccess, showError, checkPermission, preSaveForm, removeSignVietnamese } from "@/helpers/helpers";
import ApiCall from "modules/ApiCall";
import SingleImage from '@/diginext/upload/singleImage'
import { getServerSideProps as TrackingUserSession } from "plugins/next-session/admin";

export const getServerSideProps = TrackingUserSession;

const AdminPlaceCreatePage = ({ user }) => {
    const router = useRouter();
    const formInputRef = useRef({});
    const [formInput, setFormInput] = useState({});
    const [myTimeout, setMyTimeout] = useState();
    const { id } = router.query
    //Const

    //Permissions
    const canCreate   = checkPermission(user, 'zone_province_create');

    //Init load
    useEffect(function() {
        if(!canCreate) {
            router.push('/admin');
        } else {
        }
    }, []);


    // save
    const saveHandler = function() {
        let msgs = [];
        let currentFormInput = {
            active: formInput.active || false,
            name: {
              vi: formInputRef.current.nameVi.value,
              en: removeSignVietnamese(formInputRef.current.nameEn.value),
              viNon: removeSignVietnamese(formInputRef.current.nameViNon.value),
            },
            code: formInputRef.current.code.value,
            sortOrder: formInputRef.current.sortOrder.value,
            lat: formInputRef.current.lat.value,
            lng: formInputRef.current.lng.value,

        };
        if(msgs.length) {
            return showMessages(msgs);
        }
        // 
        clearTimeout(myTimeout);
        let loginTimeout = setTimeout(async function() {
            let params = {
                router,
                path: `/api/v1/admin/zone-provinces`,
                token: user.token,
                method: 'POST',
                data: currentFormInput,
                contentType: 2
            };
            let res = await ApiCall(params);
            if(!res.status) return showError(res);
            showSuccess(res);
            router.push('/admin/zones/provinces');
        }, 1000);
        setMyTimeout(loginTimeout);
    }

    //Single Upload
    const handleChangeSingleUpload = function(type, data) {
        setFormInput({
            ...formInput,
            ...data
        });
    };

    const header = (
        <PageHeader pretitle="admin" title="Province" button={<BackButton />} separator={true}>
            Create
        </PageHeader>
    );

    return (
        <LayoutPage header={header} user={user}>
            <Section borderBottom={true} style={{ padding: "2rem 0" }}>
            <HorizontalList itemSize={ListItemSize.STRETCH}>
                    <ListItem style={{ marginRight: "1rem" }}>
                        <div className="form-group">
                            <label style={{ marginRight: "15px" }}>Status</label>
                            <Switch
                                checked={formInput.active}
                                onChange={() => {
                                    setFormInput({
                                        ...formInput,
                                        active: !formInput.active
                                    })
                                }}
                            />
                        </div>
                    </ListItem>
                </HorizontalList>
                <HorizontalList itemSize={ListItemSize.STRETCH}>
                    <ListItem style={{ marginRight: "1rem" }}>
                        <Input
                            ref={el => formInputRef.current.nameVi = el}
                            defaultValue={formInput.name? formInput.name.vi:""}
                            label="Vi Name"
                            placeholder="tên tiếng việt"
                            maxLength="255"
                            validateConditions={[{ type: ValidationType.NOT_EMPTY, errMessage: "Bắt buộc" }]}
                        />
                    </ListItem>
                    <ListItem style={{ marginRight: "1rem" }}>
                        <Input
                            ref={el => formInputRef.current.nameEn = el}
                            defaultValue={formInput.name ? formInput.name.en : ""}
                            label="En Name"
                            placeholder="tên tiếng anh"
                            maxLength="255"
                            validateConditions={[{ type: ValidationType.NOT_EMPTY, errMessage: "Bắt buộc" }]}
                        />
                    </ListItem>
                    <ListItem style={{ marginRight: "1rem" }}>
                        <Input
                            ref={el => formInputRef.current.nameViNon = el}
                            defaultValue={formInput.name ? formInput.name.viNon:""}
                            label="Vi-Non Name"
                            placeholder="tên tiếng việt không dấu"
                            maxLength="255"
                        />
                    </ListItem>
                </HorizontalList>
                <HorizontalList itemSize={ListItemSize.STRETCH}>
                    <ListItem style={{ marginRight: "1rem" }}>
                        <Input
                            ref={el => formInputRef.current.code = el}
                            defaultValue={formInput.code}
                            label="Code"
                            placeholder="code"
                            maxLength="255"
                        />
                    </ListItem>
                    <ListItem style={{ marginRight: "1rem" }}>
                        <Input
                            ref={el => formInputRef.current.sortOrder = el}
                            defaultValue={formInput.sortOrder}
                            label="Sort Order"
                            placeholder="thứ tự sắp xếp"
                            maxLength="255"
                            validateConditions={[
                            { type: ValidationType.NOT_EMPTY, errMessage: "Bắt buộc" },
                            { type: ValidationType.NUMBERS, errMessage: "Phải là số" }]}
                        />
                    </ListItem>
                </HorizontalList>
                <HorizontalList itemSize={ListItemSize.STRETCH}>
                    <ListItem style={{ marginRight: "1rem" }}>
                        <Input
                            ref={el => formInputRef.current.lat = el}
                            defaultValue={formInput.lat}
                            label="Latitude"
                            placeholder="Vĩ độ trên map"
                            maxLength="255"
                            validateConditions={[
                                { type: ValidationType.NUMBERS, errMessage: "Phải là số" }]}
                        />
                    </ListItem>
                    <ListItem style={{ marginRight: "1rem" }}>
                        <Input
                            ref={el => formInputRef.current.lng = el}
                            defaultValue={formInput.lng}
                            label="Longitude"
                            placeholder="kinh độ trên map"
                            maxLength="255"
                            validateConditions={[
                                { type: ValidationType.NUMBERS, errMessage: "Phải là số" }]}
                        />
                    </ListItem>
                </HorizontalList>
                <AdminButton
                    size={ButtonSize.LARGE}
                    onClick={saveHandler}
                    style={{margin: '20px'}}
                >
                    Save changes
                </AdminButton>
            </Section>
        </LayoutPage>
    )
};

export default AdminPlaceCreatePage;
