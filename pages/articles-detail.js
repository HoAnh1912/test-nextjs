import Text from "@/components/website/section/hero/Text";
import Header from "components/website/header/Header";
import asset from "plugins/assets/asset";
import Bottom from "@/components/website/section/bottom-article-detail/Bottom";
import Form from "@/components/website/section/bottom-article-detail/Form";
import Footer from "@/components/website/elements/Footer";

export default function articlesdetail(props) {
  return (
    <div>
      <Header></Header>
      <div className="container">
        <Text></Text>
        <div className="figure">
          <img src={asset("/images/home/image 2.1.png")} />
        </div>
        <hr></hr>
        <div className="blog">
          <div className="blog__meta">
            <div className="avatar">
              <img src={asset("/images/home/avtart.png")} />
            </div>
            <div className="info">
              <p>
                <b>Mika MATIKAINEN</b>
                <br></br>
                <span>Apr 15, 2020 · 4 min read</span>
              </p>
            </div>
            <div className="btn-share">
              <a href="#">
                <img src={asset("/images/home/facebook.png")} />
              </a>
              <a href="#">
                <img src={asset("/images/home/twiter.png")} />
              </a>
            </div>
          </div>
          <div className="blog__description">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu
              velit tempus erat egestas efficitur. In hac habitasse platea
              dictumst. Fusce a nunc eget ligula suscipit finibus. Aenean
              pharetra quis lacus at viverra.
            </p>
            <p>
              Class aptent taciti sociosqu ad litora torquent per conubia
              nostra, per inceptos himenaeos. Aliquam quis posuere ligula. In eu
              dui molestie, molestie lectus eu, semper lectus.
            </p>
            <h3>Next on the pipeline</h3>
            <p>
              Duis eu velit tempus erat egestas efficitur. In hac habitasse
              platea dictumst. Fusce a nunc eget ligula suscipit finibus. Aenean
              pharetra quis lacus at viverra. Class aptent taciti sociosqu ad
              litora torquent per conubia nostra, per inceptos himenaeos.
            </p>
            <p>
              Morbi efficitur auctor metus, id mollis lorem pellentesque id.
              Nullam posuere maximus dui et fringilla.
            </p>
            <div className="img">
              <img src={asset("/images/home/Rectangle 8.png")} />
              <figcaption>
                Image caption centered this way and I’ll make this a bit longer
                to indicate the amount of line-height.
              </figcaption>
            </div>
            <p>
              Aenean pharetra quis lacus at viverra. Class aptent taciti
              sociosqu ad litora torquent per conubia nostra, per inceptos
              himenaeos. Aliquam quis posuere ligula.
            </p>
            <p>
              In eu dui molestie, molestie lectus eu, semper lectus. Proin at
              justo lacinia, auctor nisl et, consequat ante. Donec sit amet nisi
              arcu. Morbi efficitur auctor metus, id mollis lorem pellentesque
              id. Nullam posuere maximus dui et fringilla. Nulla non volutpat
              leo.
            </p>
            <p>A list looks like this:</p>
            <ul>
              <li>First item in the list </li>
              <li>
                Second item in the list lorem ipsum dolor sit amet nunc felis
                dolor lorem ipsum sit amet
              </li>
              <li>Third item in the list</li>
            </ul>
            <p>
              Class aptent taciti sociosqu ad litora torquent per conubia
              nostra, per inceptos himenaeos. Aliquam quis posuere ligula.
            </p>
            <p>
              Thanks for reading,
              <br /> Mika
            </p>
          </div>
          <div className="blog__bottom">
            <div className="bottom-share">
              <a href="#">
                <img src={asset("/images/home/facebook.png")} />
                <span>Share on Facebook</span>
              </a>
              <a href="#">
                <img src={asset("/images/home/twiter.png")} />
                <span>Share on Twitter</span>
              </a>
            </div>
            <ul className="tags">
              Tags:
              <li>
                <a href="/tag/distributed-teams/" title="distributed teams">
                  product design, culture
                </a>
              </li>
            </ul>
            <div className="author">
              <div className="author__img">
                <img src={asset("/images/home/avtart.png")} />
              </div>
              <div className="author__info">
                <p>
                  <span>Mika Matikainen </span>
                  is a Design Founder & Advisor, Berlin School of Creative
                  Leadership Executive MBA participant, Zippie advisor, Wolt
                  co-founder, and Nordic Rose stakeholder.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Bottom></Bottom>
      <Form></Form>
      <Footer></Footer>
    </div>
  );
}
